-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2018 at 01:18 PM
-- Server version: 5.5.59-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `opendesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

CREATE TABLE IF NOT EXISTS `entities` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) DEFAULT '0',
  `mode` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `registration` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `primarycontact` int(11) DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `groups` varchar(255) NOT NULL,
  `access` int(10) DEFAULT '0',
  `updated` date NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `profile` (`profile`),
  KEY `status` (`status`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `entities`
--

INSERT INTO `entities` (`eid`, `profile`, `mode`, `name`, `registration`, `status`, `type`, `primarycontact`, `createdby`, `groups`, `access`, `updated`) VALUES
(1, 1, 1, 'OpenDesk', '', 3, 1, 1, 1, '', 1, '2017-02-16'),
(2, 1, 1, 'Navig8r', '', 3, 1, 1, 1, '', 0, '2016-11-21'),
(3, 1, 1, 'Demo Company', '2016/201223/07', 3, 5, 2, 1, '', 1, '2017-02-16'),
(4, 3, 1, 'Test Client', '5911115114087', 3, 1, 3, 2, '', 0, '2017-02-16'),
(5, 1, 1, 'GovServSA', '12345', 3, 1, 1, 1, '', 1, '2018-04-08'),
(6, 5, 1, 'Hermann Labuschagne', '7108135234080', 3, 1, 4, 3, '1', 0, '2018-04-09'),
(7, 5, 1, 'Patricia Jansen', '6111190229087', 3, 1, 5, 4, '1', 0, '2018-04-19');

-- --------------------------------------------------------

--
-- Table structure for table `entities_activities`
--

CREATE TABLE IF NOT EXISTS `entities_activities` (
  `eaid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`eaid`),
  KEY `type` (`type`),
  KEY `createdby` (`createdby`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `entities_activities`
--

INSERT INTO `entities_activities` (`eaid`, `eid`, `type`, `description`, `createdby`, `updated`) VALUES
(1, 6, 3, 'Product Updated', 3, '2018-04-09 02:04:49'),
(2, 7, 3, 'Product Updated', 3, '2018-04-19 09:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `entities_communications`
--

CREATE TABLE IF NOT EXISTS `entities_communications` (
  `ecid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `mode` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`ecid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_contacts`
--

CREATE TABLE IF NOT EXISTS `entities_contacts` (
  `ecid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `relationship` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ecid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `entities_contacts`
--

INSERT INTO `entities_contacts` (`ecid`, `eid`, `pid`, `relationship`) VALUES
(1, 2, 1, 1),
(2, 3, 2, 1),
(3, 4, 3, 1),
(4, 6, 4, 1),
(5, 7, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `entities_correspondence`
--

CREATE TABLE IF NOT EXISTS `entities_correspondence` (
  `ecid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `createdby` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`ecid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_fais_assessments`
--

CREATE TABLE IF NOT EXISTS `entities_fais_assessments` (
  `efaid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `type` int(10) NOT NULL,
  `managedby` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`efaid`),
  KEY `efaid` (`efaid`),
  KEY `managedby` (`managedby`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_fais_licenses`
--

CREATE TABLE IF NOT EXISTS `entities_fais_licenses` (
  `eflid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `licensenr` int(100) NOT NULL,
  `licensestatus` int(10) NOT NULL,
  `licensecategories` varchar(250) NOT NULL,
  PRIMARY KEY (`eflid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_fais_representatives`
--

CREATE TABLE IF NOT EXISTS `entities_fais_representatives` (
  `efrid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `identification` varchar(255) DEFAULT NULL,
  `re5` int(11) DEFAULT NULL,
  `qualification` int(11) DEFAULT NULL,
  `honestyintegrity` int(11) DEFAULT NULL,
  `products` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`efrid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `entities_fais_representatives`
--

INSERT INTO `entities_fais_representatives` (`efrid`, `eid`, `firstname`, `lastname`, `identification`, `re5`, `qualification`, `honestyintegrity`, `products`, `status`) VALUES
(2, 1, 'Demo', 'Representative', '7905115053087', 1, 1, 1, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `entities_financials`
--

CREATE TABLE IF NOT EXISTS `entities_financials` (
  `efid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `property1` float DEFAULT NULL,
  `property2` float DEFAULT NULL,
  `property3` float DEFAULT NULL,
  `business1` float DEFAULT NULL,
  `business2` float DEFAULT NULL,
  `business3` float DEFAULT NULL,
  `investment1` float DEFAULT NULL,
  `investment2` float DEFAULT NULL,
  `investment3` float DEFAULT NULL,
  `insurance1` float DEFAULT NULL,
  `insurance2` float DEFAULT NULL,
  `insurance3` float DEFAULT NULL,
  `other1` float DEFAULT NULL,
  `other2` float DEFAULT NULL,
  `other3` float DEFAULT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) DEFAULT NULL,
  `field5` int(11) DEFAULT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`efid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_groups`
--

CREATE TABLE IF NOT EXISTS `entities_groups` (
  `egid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`egid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `entities_groups`
--

INSERT INTO `entities_groups` (`egid`, `eid`, `name`, `subject`, `content`, `status`, `updated`) VALUES
(1, 5, 'Pre-Retirement Clients', 'Clients who have not yet reached retirement stage.', '', 1, '2018-04-09'),
(2, 5, 'Post-retirement Clients', 'Clients who have already reached retirement stage. ', '', 1, '2018-04-19');

-- --------------------------------------------------------

--
-- Table structure for table `entities_medicals`
--

CREATE TABLE IF NOT EXISTS `entities_medicals` (
  `emid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `diet` int(11) DEFAULT NULL,
  `smoker` int(11) DEFAULT NULL,
  `allergy` int(11) DEFAULT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) DEFAULT NULL,
  `field5` int(11) DEFAULT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`emid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_options`
--

CREATE TABLE IF NOT EXISTS `entities_options` (
  `eoid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `productcategories` varchar(255) NOT NULL,
  `productproviders` varchar(255) NOT NULL,
  `producttypes` varchar(255) NOT NULL,
  `servicetypes` varchar(255) NOT NULL,
  `tickettypes` varchar(255) NOT NULL,
  `entitytypes` varchar(255) NOT NULL,
  PRIMARY KEY (`eoid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `entities_options`
--

INSERT INTO `entities_options` (`eoid`, `eid`, `productcategories`, `productproviders`, `producttypes`, `servicetypes`, `tickettypes`, `entitytypes`) VALUES
(1, 1, '1', '1', '1', '1,3,2', '14,12,11', '4,7,1,2,5,6,3'),
(2, 3, '7,4,8,5,3,2,9,6,1', '7,18,6,17,16,4,14,27,29,8,20,21,19,23,5,3,15,1,10,12,13,9,25,22,26,28,2,11', '1,2,3', '16,15', '48,47', '1,2,3,4,5,6,7'),
(3, 5, '4,10,2,9', '30,18,19,3,15,12,13,26,28', '21,23,22', '18,17,19', '48,47', '1,2,3,4,5,6,7\n');

-- --------------------------------------------------------

--
-- Table structure for table `entities_products`
--

CREATE TABLE IF NOT EXISTS `entities_products` (
  `epid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `issuedate` date DEFAULT NULL,
  `field1` int(11) NOT NULL,
  `field2` int(11) NOT NULL,
  `field3` int(11) NOT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`epid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `entities_products`
--

INSERT INTO `entities_products` (`epid`, `eid`, `identifier`, `category`, `type`, `provider`, `status`, `issuedate`, `field1`, `field2`, `field3`, `field4`, `field5`, `updated`) VALUES
(1, 6, '123', 9, 21, 12, 1, '2012-01-01', 0, 0, 0, 0, 0, '2018-04-09'),
(2, 7, 'AL180000001340156', 10, 23, 30, 1, '2018-05-01', 0, 0, 0, 0, 0, '2018-04-19');

-- --------------------------------------------------------

--
-- Table structure for table `entities_services`
--

CREATE TABLE IF NOT EXISTS `entities_services` (
  `esid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `issuedate` date DEFAULT NULL,
  `field1` int(11) NOT NULL,
  `field2` int(11) NOT NULL,
  `field3` int(11) NOT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`esid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `entities_services`
--

INSERT INTO `entities_services` (`esid`, `eid`, `identifier`, `type`, `status`, `issuedate`, `field1`, `field2`, `field3`, `field4`, `field5`, `updated`) VALUES
(1, 4, 'DC001', 15, 1, '2017-02-01', 0, 0, 0, 0, 0, '2017-02-16'),
(2, 4, 'DC002', 16, 1, '2017-02-01', 0, 0, 0, 0, 0, '2017-02-16');

-- --------------------------------------------------------

--
-- Table structure for table `entities_settings`
--

CREATE TABLE IF NOT EXISTS `entities_settings` (
  `esid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `email` int(11) NOT NULL DEFAULT '0',
  `email_from` varchar(255) DEFAULT NULL,
  `email_smtp` varchar(255) DEFAULT NULL,
  `sms` tinyint(1) NOT NULL DEFAULT '0',
  `sms_apiid` varchar(255) DEFAULT NULL,
  `sms_username` varchar(255) DEFAULT NULL,
  `sms_password` varchar(255) DEFAULT NULL,
  `twitter` int(11) DEFAULT '0',
  `twitter_username` varchar(255) DEFAULT NULL,
  `twitter_password` varchar(255) DEFAULT NULL,
  `correspondence` tinyint(1) NOT NULL DEFAULT '0',
  `support` tinyint(1) NOT NULL DEFAULT '0',
  `governance` tinyint(1) NOT NULL DEFAULT '0',
  `fiduciary` tinyint(1) NOT NULL DEFAULT '0',
  `files` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`esid`),
  UNIQUE KEY `esid` (`esid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `entities_settings`
--

INSERT INTO `entities_settings` (`esid`, `eid`, `email`, `email_from`, `email_smtp`, `sms`, `sms_apiid`, `sms_username`, `sms_password`, `twitter`, `twitter_username`, `twitter_password`, `correspondence`, `support`, `governance`, `fiduciary`, `files`) VALUES
(1, 1, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 1, 1, 1, 1, 1),
(2, 3, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, 0, 0, 0),
(3, 5, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `entities_tickets`
--

CREATE TABLE IF NOT EXISTS `entities_tickets` (
  `etid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `epid` int(11) NOT NULL,
  `esid` int(11) NOT NULL,
  `ownedby` int(100) NOT NULL,
  `assignedto` int(100) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `reminderdate` date NOT NULL,
  `duedate` date NOT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`etid`),
  KEY `eid` (`eid`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `entities_tickets`
--

INSERT INTO `entities_tickets` (`etid`, `eid`, `epid`, `esid`, `ownedby`, `assignedto`, `type`, `title`, `description`, `reminderdate`, `duedate`, `field1`, `field2`, `field3`, `field4`, `field5`, `status`) VALUES
(1, 4, 0, 2, 2, 2, 48, 'New Momentum Health Custom Option', 'sdgfsdfgsd', '2017-02-28', '2017-02-28', 1, NULL, NULL, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `entities_tickets_notes`
--

CREATE TABLE IF NOT EXISTS `entities_tickets_notes` (
  `etnid` int(11) NOT NULL AUTO_INCREMENT,
  `etid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`etnid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_trusts_registrations`
--

CREATE TABLE IF NOT EXISTS `entities_trusts_registrations` (
  `ettid` int(100) NOT NULL AUTO_INCREMENT,
  `eid` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`ettid`),
  KEY `efaid` (`ettid`),
  KEY `managedby` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options_commnicationtypes`
--

CREATE TABLE IF NOT EXISTS `options_commnicationtypes` (
  `octid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`octid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_commnicationtypes`
--

INSERT INTO `options_commnicationtypes` (`octid`, `description`) VALUES
(1, 'Email'),
(2, 'SMS');

-- --------------------------------------------------------

--
-- Table structure for table `options_communicationmodes`
--

CREATE TABLE IF NOT EXISTS `options_communicationmodes` (
  `ocmid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ocmid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_communicationmodes`
--

INSERT INTO `options_communicationmodes` (`ocmid`, `description`) VALUES
(1, 'Qeued'),
(2, 'Sent');

-- --------------------------------------------------------

--
-- Table structure for table `options_communicationstatus`
--

CREATE TABLE IF NOT EXISTS `options_communicationstatus` (
  `ocsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ocsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_communicationstatus`
--

INSERT INTO `options_communicationstatus` (`ocsid`, `description`) VALUES
(1, 'Draft'),
(2, 'Finalized');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityactivitytypes`
--

CREATE TABLE IF NOT EXISTS `options_entityactivitytypes` (
  `oeatid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oeatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_entityactivitytypes`
--

INSERT INTO `options_entityactivitytypes` (`oeatid`, `description`) VALUES
(1, 'comment'),
(2, 'phone-square'),
(3, 'briefcase'),
(4, 'male'),
(5, 'ticket'),
(6, 'email'),
(7, 'building-o');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitycontactrelationships`
--

CREATE TABLE IF NOT EXISTS `options_entitycontactrelationships` (
  `oecrid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oecrid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `options_entitycontactrelationships`
--

INSERT INTO `options_entitycontactrelationships` (`oecrid`, `description`) VALUES
(1, 'Director'),
(2, 'Member'),
(3, 'Trustee'),
(4, 'Beneficiary'),
(5, 'Spouse'),
(6, 'Dependant'),
(7, 'Proprietor'),
(8, 'Executor'),
(9, 'Founder'),
(10, 'Financial Officer'),
(11, 'Staff Member'),
(12, 'Donor');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitycorrespondencetypes`
--

CREATE TABLE IF NOT EXISTS `options_entitycorrespondencetypes` (
  `oectid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `fa_icon` varchar(255) NOT NULL,
  PRIMARY KEY (`oectid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `options_entitycorrespondencetypes`
--

INSERT INTO `options_entitycorrespondencetypes` (`oectid`, `description`, `fa_icon`) VALUES
(1, 'Phone', 'fa-phone'),
(2, 'SMS', 'fa-mobile'),
(3, 'Email', 'fa-envelope'),
(4, 'Note', 'fa-comment');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityfilters`
--

CREATE TABLE IF NOT EXISTS `options_entityfilters` (
  `oefid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`oefid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options_entitymedicaldiets`
--

CREATE TABLE IF NOT EXISTS `options_entitymedicaldiets` (
  `oemdid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oemdid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitymedicaldiets`
--

INSERT INTO `options_entitymedicaldiets` (`oemdid`, `description`) VALUES
(1, 'Vegeterian'),
(2, 'Vegan'),
(3, 'Raw Food');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitymodes`
--

CREATE TABLE IF NOT EXISTS `options_entitymodes` (
  `oemid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oemid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitymodes`
--

INSERT INTO `options_entitymodes` (`oemid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityproductcategories`
--

CREATE TABLE IF NOT EXISTS `options_entityproductcategories` (
  `oepcid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oepcid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `options_entityproductcategories`
--

INSERT INTO `options_entityproductcategories` (`oepcid`, `description`) VALUES
(1, 'Short-Term Insurance'),
(2, 'Long-Term Insurance'),
(3, 'Health Service Benefits'),
(4, 'Collective Investment Scheme'),
(5, 'Foreign Exchange'),
(6, 'Securities and Shares'),
(7, 'Bank Deposits'),
(8, 'Debentures and Securitized Debt'),
(9, 'Retail Pension Benefits'),
(10, 'Funeral Policy');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityproductproviders`
--

CREATE TABLE IF NOT EXISTS `options_entityproductproviders` (
  `oeppid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oeppid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `options_entityproductproviders`
--

INSERT INTO `options_entityproductproviders` (`oeppid`, `description`) VALUES
(1, 'Mutual and Federal'),
(2, 'Santam'),
(3, 'Momentum Life'),
(4, 'Discovery Life'),
(5, 'Momentum Health'),
(6, 'Discovery Health'),
(7, 'Allan Gray'),
(8, 'Investec'),
(9, 'ONE Insurance'),
(10, 'New Wheels Insurance'),
(11, 'Zurich Insurance'),
(12, 'Old Mutual Life'),
(13, 'Old Mutual Wealth'),
(14, 'Efficient Wealth'),
(15, 'Momentum Wealth'),
(16, 'Discovery Invest'),
(17, 'Discovery Insure'),
(18, 'Brightrock Life'),
(19, 'Liberty Life'),
(20, 'Liberty Health'),
(21, 'Liberty Invest'),
(22, 'Resolution Health'),
(23, 'Medscheme'),
(24, 'Bonitas Medical Scheme'),
(25, 'Profmed Medical Scheme'),
(26, 'Sanlam Glacier'),
(27, 'Genric Insurance'),
(28, 'Sanlam Life'),
(29, 'Hollard'),
(30, 'Assupol');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityproductstatus`
--

CREATE TABLE IF NOT EXISTS `options_entityproductstatus` (
  `oepsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oepsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entityproductstatus`
--

INSERT INTO `options_entityproductstatus` (`oepsid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityproducttypes`
--

CREATE TABLE IF NOT EXISTS `options_entityproducttypes` (
  `oeptid` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) DEFAULT NULL,
  `field2_description` varchar(255) DEFAULT NULL,
  `field3_description` varchar(255) DEFAULT NULL,
  `field4_description` varchar(255) DEFAULT NULL,
  `field5_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oeptid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `options_entityproducttypes`
--

INSERT INTO `options_entityproducttypes` (`oeptid`, `profile`, `description`, `field1_description`, `field2_description`, `field3_description`, `field4_description`, `field5_description`) VALUES
(2, 105, 'Unit Trust - Tax Free Savings', 'sdfs', 'sdfs', '', '', ''),
(3, 105, 'Unit Trust - Retirement Annuity', '', '', '', '', ''),
(4, 105, 'Unit Trust - Living Annuity', '', '', '', '', ''),
(5, 105, 'Pension Fund', '', '', '', '', ''),
(6, 105, 'Personal Short-Term Insurance', '', '', 'zsxvfasdf', '', ''),
(7, 105, 'Commercial Short-Term Insurance', '', '', '', '', ''),
(8, 105, 'Provident Fund', '', '', '', '', ''),
(9, 0, 'Endowment', '', '', '', '', ''),
(10, 0, 'Keyman Insurance', '', '', '', '', ''),
(11, 0, 'Buy and Sell Insurance', '', '', '', '', ''),
(12, 0, 'Gap Cover', '', '', '', '', ''),
(13, 0, 'Health Insurance', '', '', '', '', ''),
(14, 0, 'Professional Indemnity Insurance', '', '', '', '', ''),
(15, 0, 'Retirement Annuity', 'Risk Analysis', 'Record of Advice', '', '', ''),
(16, 0, 'Living Annuity', '', '', '', '', ''),
(17, 0, 'Whole Life Cover', 'Financial Needs Analysis', 'Record of Advice', '', '', ''),
(20, 105, 'Additional Fin Product Type', NULL, NULL, NULL, NULL, NULL),
(21, 5, 'Long Term Insurance - Retirement Annuity', NULL, NULL, NULL, NULL, NULL),
(22, 5, 'Unit Trust - Retirement Annuity', NULL, NULL, NULL, NULL, NULL),
(23, 5, 'Long-Term Insurance - Funeral Policy', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options_entityservicestatus`
--

CREATE TABLE IF NOT EXISTS `options_entityservicestatus` (
  `oessid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oessid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entityservicestatus`
--

INSERT INTO `options_entityservicestatus` (`oessid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityservicetypes`
--

CREATE TABLE IF NOT EXISTS `options_entityservicetypes` (
  `oestid` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) DEFAULT NULL,
  `field2_description` varchar(255) DEFAULT NULL,
  `field3_description` varchar(255) DEFAULT NULL,
  `field4_description` varchar(255) DEFAULT NULL,
  `field5_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oestid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `options_entityservicetypes`
--

INSERT INTO `options_entityservicetypes` (`oestid`, `profile`, `description`, `field1_description`, `field2_description`, `field3_description`, `field4_description`, `field5_description`) VALUES
(1, 1, 'Compliance Assistance', 'test1', 'test2', 'test3', 'test4', 'test5'),
(2, 1, 'Compliance Monitoring - Category 2', 'field1', 'field2', 'field3', 'field4', 'field5'),
(3, 1, 'Compliance Monitoring - Category 1', 'sdf', 'er', 'fdgh', 'seg', 'fsdgh'),
(4, 0, 'Estate Planning', '', '', '', '', NULL),
(5, 0, 'Executorship', '', '', '', '', NULL),
(6, 0, 'Trust Planning', NULL, NULL, NULL, NULL, NULL),
(7, 105, 'Financial Planning', NULL, NULL, NULL, NULL, NULL),
(8, 0, 'Information Gathering', NULL, NULL, NULL, NULL, NULL),
(9, 0, 'Life Application', NULL, NULL, NULL, NULL, NULL),
(10, 0, 'Life Application', NULL, NULL, NULL, NULL, NULL),
(11, 0, 'Life Application', NULL, NULL, NULL, NULL, NULL),
(12, 0, 'Servicing Request', NULL, NULL, NULL, NULL, NULL),
(13, 105, 'Trust Planning', NULL, NULL, NULL, NULL, NULL),
(14, 105, 'Estate Planning', NULL, NULL, NULL, NULL, NULL),
(15, 3, 'Intermediary Service', NULL, NULL, NULL, NULL, NULL),
(16, 3, 'Advisory Service', NULL, NULL, NULL, NULL, NULL),
(17, 5, 'Discretionary Management', NULL, NULL, NULL, NULL, NULL),
(18, 5, 'Advisory Service', NULL, NULL, NULL, NULL, NULL),
(19, 5, 'Intermediary Service', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options_entitystatus`
--

CREATE TABLE IF NOT EXISTS `options_entitystatus` (
  `oesid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oesid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitystatus`
--

INSERT INTO `options_entitystatus` (`oesid`, `description`) VALUES
(1, 'Lead'),
(2, 'Prospect'),
(3, 'Client');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityticketstatus`
--

CREATE TABLE IF NOT EXISTS `options_entityticketstatus` (
  `oetsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oetsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entityticketstatus`
--

INSERT INTO `options_entityticketstatus` (`oetsid`, `description`) VALUES
(1, 'Pending'),
(2, 'Completed'),
(3, 'Unresolved');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitytickettypes`
--

CREATE TABLE IF NOT EXISTS `options_entitytickettypes` (
  `oettid` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) NOT NULL,
  `field2_description` varchar(255) NOT NULL,
  `field3_description` varchar(255) NOT NULL,
  `field4_description` varchar(255) NOT NULL,
  `field5_description` varchar(255) NOT NULL,
  PRIMARY KEY (`oettid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `options_entitytickettypes`
--

INSERT INTO `options_entitytickettypes` (`oettid`, `profile`, `description`, `field1_description`, `field2_description`, `field3_description`, `field4_description`, `field5_description`) VALUES
(6, 1, 'FAIS - Change of Address', 'FSP1', 'FSP14A', 'Phone Analyst', 'Confirm with client', ''),
(7, 1, 'FAIS - Remove Representative', 'FSP1', 'FSP14A', '', '', ''),
(11, 1, 'FAIS - Add Representative', '', '', '', '', ''),
(12, 1, 'FAIS - Add License Product', '', '', '', '', ''),
(13, 1, 'FAIS - Remove License Product', '', '', '', '', ''),
(14, 1, 'FAIS - Add Key Individual', 'Obtain Certified ID', 'Obtain Certified Proof of Address', 'Obtain Proof of Bank', 'Give kiss', ''),
(15, 1, 'FAIS - Remove Key Individual', '', '', '', '', ''),
(16, 1, 'FAIS - Submit Financial Statement', 'Capture Financial Statements on FSB Online - confirm solvency and signatories', 'Capture Subordinated Loan Agreement', 'Capture FSB Annexure A', '', ''),
(17, 1, 'FAIS - Submit Compliance Report', '', '', '', '', ''),
(18, 1, 'FAIS - Change of Directorship', '', '', '', '', ''),
(19, 1, 'FAIS - Change of Shareholding', '', '', '', '', ''),
(20, 1, 'FAIS - Add Compliance Officer', '', '', '', '', ''),
(21, 1, 'FAIS - Remove Compliance Officer', '', '', '', '', ''),
(22, 1, 'FAIS - Request License Certificate', '', '', '', '', ''),
(23, 1, 'FAIS - Request Extension for Financial Statements', '', '', '', '', ''),
(24, 1, 'FAIS - New license application', '', '', '', '', ''),
(25, 1, 'CMS - Broker ( BR) Application', '', '', '', '', ''),
(26, 1, 'FAIS - DOFA request', '', '', '', '', ''),
(27, 1, 'CMS - Brokerage ( ORG) Application', '', '', '', '', ''),
(28, 1, 'FAIS - Remove representative supervision status', '', '', '', '', ''),
(29, 1, 'FAIS - Update Key Individual Products', '', '', '', '', ''),
(30, 1, 'FAIS - Representative amendment', '', '', '', '', ''),
(31, 1, 'FAIS - Change of auditor', '', '', '', '', ''),
(33, 1, 'CMS Broker renewal', '', '', '', '', ''),
(36, 105, 'Information Gathering', 'Capture Client Contact Information', 'Capture Letters of Introduction & Authority', 'Capture Client Instruction', 'Capture Financials', 'Capture Medicals'),
(37, 1, 'FAIS - Name Change', '', '', '', '', ''),
(38, 1, 'FAIS - Discretionary Mandate Approval', 'Capture Amended Discretionary Mandate', 'Confirm Payment to the FSB', 'Submit Amended Mandate and Proof of Payment', 'Capture Specimen Mandate', ''),
(39, 1, 'FAIS - Update details', '', '', '', '', ''),
(40, 12, 'Life Application ticket', '', '', '', '', ''),
(41, 105, 'Record of Advice & Client Instruction', 'Capture Record of Advice', 'Capture Client Instruction', 'Activate Services and/or Products Required', '', ''),
(42, 105, 'General Query', 'Capture relevant prospect information', 'Capture Nature of Query', 'Assign to correct user profile', '', ''),
(43, 1, 'Submit AUM ( Assets under management)', '', '', '', '', ''),
(44, 1, 'FIC registration', '', '', '', '', ''),
(45, 12, 'test ticket with michelle', 'Need completed form with instruction ', 'Once all documents are received email company', 'Follow up ', 'Check confirmation and email to client', ''),
(46, 1, 'FSB - SUBMIT COMPLIANCE REPORT', '', '', '', '', ''),
(47, 3, 'New Retirement Annuity', 'Financial Needs Analysis', 'Risk Profile', 'Record of Advice', 'Application Form', 'Quotation'),
(48, 3, 'New Health Insurance', 'Medical Questionnaire', 'Financial Position', 'Record of Advice', 'Application Form', 'Quotation');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitytypes`
--

CREATE TABLE IF NOT EXISTS `options_entitytypes` (
  `oetid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oetid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_entitytypes`
--

INSERT INTO `options_entitytypes` (`oetid`, `description`) VALUES
(1, 'Individual'),
(2, 'Partnership'),
(3, 'Trust'),
(4, 'Close Corporation'),
(5, 'Private Company'),
(6, 'Public Company'),
(7, 'Incorporation');

-- --------------------------------------------------------

--
-- Table structure for table `options_faisassessmentquestions`
--

CREATE TABLE IF NOT EXISTS `options_faisassessmentquestions` (
  `ofaq` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  PRIMARY KEY (`ofaq`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_faisassessmentquestions`
--

INSERT INTO `options_faisassessmentquestions` (`ofaq`, `question`) VALUES
(1, ''),
(2, 'Who in the company is responsible to oversee the day to day FSP corporate administration?');

-- --------------------------------------------------------

--
-- Table structure for table `options_useraccesslevels`
--

CREATE TABLE IF NOT EXISTS `options_useraccesslevels` (
  `oualid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oualid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_useraccesslevels`
--

INSERT INTO `options_useraccesslevels` (`oualid`, `description`) VALUES
(1, 'Administrative User'),
(2, 'Privileged User'),
(3, 'Normal User');

-- --------------------------------------------------------

--
-- Table structure for table `options_usermode`
--

CREATE TABLE IF NOT EXISTS `options_usermode` (
  `oumid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oumid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_usermode`
--

INSERT INTO `options_usermode` (`oumid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_userstatus`
--

CREATE TABLE IF NOT EXISTS `options_userstatus` (
  `ousid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ousid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_userstatus`
--

INSERT INTO `options_userstatus` (`ousid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_usertaskstatus`
--

CREATE TABLE IF NOT EXISTS `options_usertaskstatus` (
  `outsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`outsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_usertaskstatus`
--

INSERT INTO `options_usertaskstatus` (`outsid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `preferredname` varchar(255) NOT NULL,
  `idnumber` varchar(255) NOT NULL,
  `dateofbirth` date NOT NULL,
  `physicaladdress` varchar(255) NOT NULL,
  `postaladdress` varchar(255) NOT NULL,
  `homephone` varchar(255) NOT NULL,
  `officephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `field1` varchar(255) NOT NULL,
  `field2` varchar(11255) NOT NULL,
  `field3` varchar(255) NOT NULL,
  `field4` varchar(255) NOT NULL,
  `field5` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`pid`, `profile`, `lastname`, `firstname`, `preferredname`, `idnumber`, `dateofbirth`, `physicaladdress`, `postaladdress`, `homephone`, `officephone`, `email`, `mobile`, `field1`, `field2`, `field3`, `field4`, `field5`, `updated`) VALUES
(1, 1, 'Badenhorst', 'Dick', 'Dick', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '2016-11-21'),
(2, 1, 'User', 'Demo', 'Demo', '7902445215369', '2017-02-01', '', '', '', '', 'demo@democompany.co.za', '', '', '', '', '', '', '2017-02-16'),
(3, 3, 'Client', 'Test', 'Test', '5911115114087', '2017-02-01', '', '', '', '', '', '', '', '', '', '', '', '2017-02-16'),
(4, 5, 'Labuschagne', 'Hermann', '', '7108135234080', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '2018-04-09'),
(5, 5, 'Jansen', 'Patricia', 'Patricia', '6111190229087', '0000-00-00', '13 Fern Straat Broadlands Park Strand 7140', '13 Fern Straat Broadlands Park Strand 7140', '0218533381', '', '', '0719070218', '', '', '', '', '', '2018-04-19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `updated` date NOT NULL,
  `accesslevel` int(100) NOT NULL,
  `profile` int(11) NOT NULL,
  `linkedprofiles` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `fullname`, `email`, `status`, `updated`, `accesslevel`, `profile`, `linkedprofiles`) VALUES
(1, 'admin@opendesk.co.za', '5f4dcc3b5aa765d61d8327deb882cf99', 'Administrator', 'admin@opendesk.co.za', 1, '2018-04-09', 1, 1, '1,2,3,5'),
(2, 'demo@company.co.za', '5f4dcc3b5aa765d61d8327deb882cf99', 'Demonstrator', 'demo@company.co.za', 1, '2017-09-11', 2, 3, ''),
(3, 'hermann@govservsa.co.za', '5f4dcc3b5aa765d61d8327deb882cf99', 'Hermann Labuschagne', 'hermann@govservsa.co.za', 1, '2018-04-19', 1, 5, ''),
(4, 'danie@govservsa.co.za', '5f4dcc3b5aa765d61d8327deb882cf99', 'Danie Groenewald', 'danie@govservsa.co.za', 1, '2018-04-19', 3, 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `users_notifications`
--

CREATE TABLE IF NOT EXISTS `users_notifications` (
  `unid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`unid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_settings`
--

CREATE TABLE IF NOT EXISTS `users_settings` (
  `usid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `imap` tinyint(1) NOT NULL DEFAULT '0',
  `imap_email` varchar(255) DEFAULT NULL,
  `imap_password` varchar(255) DEFAULT NULL,
  `imap_server` varchar(255) DEFAULT NULL,
  `imap_folder` varchar(255) DEFAULT NULL,
  `imap_port` varchar(255) NOT NULL,
  `imap_flags` varchar(255) NOT NULL,
  `seafile` int(11) NOT NULL DEFAULT '0',
  `seafile_username` varchar(255) DEFAULT NULL,
  `seafile_password` varchar(255) DEFAULT NULL,
  `seafile_token` varchar(255) NOT NULL,
  `seafile_leadlibrary` varchar(255) DEFAULT NULL,
  `seafile_prospectlibrary` varchar(255) DEFAULT NULL,
  `seafile_clientlibrary` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`usid`),
  KEY `usid` (`usid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users_settings`
--

INSERT INTO `users_settings` (`usid`, `uid`, `imap`, `imap_email`, `imap_password`, `imap_server`, `imap_folder`, `imap_port`, `imap_flags`, `seafile`, `seafile_username`, `seafile_password`, `seafile_token`, `seafile_leadlibrary`, `seafile_prospectlibrary`, `seafile_clientlibrary`) VALUES
(1, 1, 1, 'admin@opendesk.co.za', 'Admin@opend3sk', 'imap.opendesk.co.za', 'INBOX', '143', 'tls/novalidate-cert', 1, 'admin@opendesk.co.za', 'Admin@opend3sk', '13834863789bf89dff6b7fb06a1beacb8476daea', '5dfc1afa-bcf1-4f1a-88e0-6c045d1e1a1e', '0fffae88-e3cb-4542-a430-873abd1a6d75', '0b09a9c8-e702-4d07-8a6f-56a66642fcc1'),
(2, 2, 0, NULL, NULL, NULL, NULL, '', '', 0, NULL, NULL, '', NULL, NULL, NULL),
(3, 3, 0, NULL, NULL, NULL, NULL, '', '', 0, NULL, NULL, '', NULL, NULL, NULL),
(4, 4, 0, NULL, NULL, NULL, NULL, '', '', 0, NULL, NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_tasks`
--

CREATE TABLE IF NOT EXISTS `users_tasks` (
  `utid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `reminderdate` date NOT NULL,
  `duedate` date NOT NULL,
  `progress` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`utid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_tasks_notes`
--

CREATE TABLE IF NOT EXISTS `users_tasks_notes` (
  `utnid` int(11) NOT NULL AUTO_INCREMENT,
  `utid` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`utnid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
