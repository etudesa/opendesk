OPENDESK CHANGELOG

V1.3.0 ( 08/03/2016):

Whats New:

    Seperated business profile functions from user account functions
    
Bugs Fixed:

    Bread crumb issues fixed
    Some design issues fixed

V1.2.0 ( 01/03/2016):

Whats New:

    Ability to add and update various dropdown options
    Added ability to include custom fields into products, services and tickets
    Added ability to add and update settings for SMS service

Bugs Fixed:

    Search Result link to entity was missing
    Google Fonts link to https fixed
    Various broken links on the dashboard fixed

V1.1.0 ( 07/02/2016):

Whats New:

    User Account and Company Profile Management functionality was added
    Settings and Options  functionality was added
    Top Menu navigation improvements and layout changes
    Various minor changes to selections and dropdown options

Bugs Fixed:

    Top Menu missing links
    Bulk Email sending issues fixed
    Bulk SMS sending issues fixed

V1.0.0 (01/01/2016):

What's New:

    This is the first public stable release of OpenDesk.

Bugs Fixed:

    The project was only released on Github now, no prior issues tracked
