# OpenDesk
An Open Source Client Management System designed for Financial Advisors in South Africa

##Introduction
OpenDesk is customer relationship manager built using PHP, MySQL and Apache. It makes use of the Model View Controller framework provided by CodeIgniter.

The system is specifically designed for advisors within the South African financial services industry and makes specific references to various legislation and industry practices specific to the region.

<img src="https://opendesk.co.za/screenshot.png">

##Installation
OpenDesk can be installed on any LAMP (Linux Apache Mysql PHP) or WAMP (Windows Apache Mysql PHP) stack. It makes use of a single database instance and requires some extensions to be enabled. The following steps should assist you in getting started:
- Enable the following PHP modules: php5_imap, php5_curl
- Enable the following APACHE modules: none
- Ensure that short_open_tags are enabled in your php.ini file
- Ensure that sessions are enabled in your php.ini file
- Copy the entire source folder into the webroot of your webserver.
- Run the database\opendesk.sql query in MySQL. This can be done using the terminal or via PHPMyAdmin.

##Setup
The codeigniter framework requires some customizations in order to connect to your custom web server and mysql server
- Specify the correct base_url in application\config\config.php
- Specify the correct database details in application\config\database.php
- Specify the correct app_absolutepath in application\config\layout.php

##First Time Use
Once the setup is complete you have to specify some additional information in order to activate all of the features.
- A default admin user is created by the opendesk.sql file, make sure that you specify the email details if you want to access your inbox. 
- For gmail access make sure to use your full gmail address as username, imap.gmail.com:993/imap/ssl as the imap server and INBOX/home as the imap inbox