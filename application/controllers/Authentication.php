<?php

class Authentication extends CI_Controller {

	function login(){
		$data['mode'] = "page";

		if ($this->session->userdata('logged_in') == 1) {
			
			redirect('dashboard/index');
			
		}else{
		
			//setup validation rules and fields
			$rules['login_username']	= "required|min_length[4]|max_length[32]";
			$rules['login_password']	= "required|min_length[4]|max_length[32]";		
			$this->validation->set_rules($rules);
			$fields['login_username'] = 'Username';
			$fields['login_password'] = 'Password';
			$this->validation->set_fields($fields);
	
			if ($this->validation->run() == TRUE) {
	
				if($this->simplelogin->login($this->input->post('login_username'), $this->input->post('login_password'))) {
					$this->db->where('uid', $this->session->userdata('uid'));
					$this->db->set('updated', date("Y-m-d"));
					$this->db->update('users');
          
					if ($this->session->userdata('accesslevel') == 1){
						redirect('admin/index');
					}else{						
						$this->db->db_select($this->session->userdata('database'));					
            $this->db->select("organizations.name, organizations.orgid");
            $this->db->from('organizations');
            $this->db->where('organizations.orgid', 1);
            $result = $this->db->get();
            $organization = $result->first_row();
						$this->session->set_userdata('organization_name', $organization->name);
            $this->session->set_userdata('organization', $organization->orgid);
						
            redirect('dashboard/index');
					}
				} else {
					$data['message'] = "Username or password not found.";
					$this->layout->buildPage('authentication/login', $data);
				}
			}else{
				//$data['message'] = "Enter a valid username and password.";
				$this->layout->buildPage('authentication/login', $data);
			}
			
		}
	}

	function logout()
	{
		$data['body'] = "";
		$data['window'] = "mainWindow";
		$data['mode'] = "view";

		//Load
		$this->load->helper('url');

		//Logout
		$this->simplelogin->logout();
		$data['message'] = "You have successfully logged out.";
		$this->layout->buildPage('authentication/login', $data);
	}
}
?>