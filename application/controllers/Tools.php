<?php

class Tools extends CI_Controller {

	function bulkemails(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Bulk Emails";
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'tools');

    $data['query'] = $this->database_model->getOrganizationCommunications($this->session->userdata('organization'), 1 ,NULL, NULL, NULL);

		$this->layout->buildPage('tools/bulkemails', $data);
	}

	function bulkemails_view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['ecid'] = $this->uri->segment(3);
		
		$this->session->set_userdata('menuitem', 'tools');
		
		$data['profile'] = $this->database_model->viewOrganization($this->session->userdata('organization'), NULL ,NULL, NULL, NULL);
		
		$data['communication'] = $this->database_model->viewOrganizationCommunications($data['ecid']);
		$data['title'] = $data['communication']->subject;
		$data['group'] = $data['communication']->group;

		$this->db->select("entities.*");
		$this->db->select("entities_contacts.*");
		$this->db->select("persons.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->like('entities.groups', $data['communication']->group); 
		$data['subscribers'] = $this->db->get();

    $this->layout->buildPage('tools/bulkemails/view', $data);
	}

	function bulkemails_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "New Bulk Email";
		$data['subtitle'] = "";
		
    $data['groups'] = $this->database_model->getOrganizationGroups($this->session->userdata('organization'), NULL ,NULL);
		$data['communicationstatus'] = $this->db->get('options_communicationstatus');
		
    $this->layout->buildPage('tools/bulkemails/add', $data);
	}
	
	function bulkemails_insert(){	
		$this->db->trans_start();
		$this->db->insert('organizations_communications', $_POST);
		$this->db->trans_complete();
		
		redirect('tools/bulkemails');
	}
	
	function bulkemails_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['ecid'] = $this->uri->segment(3);
				
		$data['communication'] = $this->database_model->viewOrganizationCommunications($data['ecid']);
		$data['title'] = $data['communication']->subject;
		
		$data['communicationstatus'] = $this->db->get('options_communicationstatus');
		
    $this->layout->buildPage('tools/bulkemails/edit', $data);
	}
	
	function bulkemails_update(){
	
		$this->db->trans_start();
		$this->db->where('ecid', $_POST['ecid']);
		$this->db->update('organizations_communications', $_POST);
		$this->db->trans_complete();

		redirect('tools/bulkemails');
	}
	
	function bulkemails_send(){
		$data['ecid'] = $this->uri->segment(3);
		
    $communication = $this->database_model->viewOrganizationCommunications($data['ecid']);
		
		$this->db->trans_start();
		$this->db->where('ecid', $data['ecid']);
		$this->db->update('organizations_communications', array('mode' => 2, 'status' => 2));
		$this->db->trans_complete();
		
		//obtain bulk email subscribers
		$this->db->select("entities.*");
		$this->db->select("entities_contacts.*");
		$this->db->select("persons.email");
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
		$this->db->like('entities.groups', $communication->group); 
		$subscribers = $this->db->get();
		
		foreach ($subscribers->result() as $subscriber){
			$this->email->from('admin@opendesk.co.za', 'OpenDesk');
			$this->email->to($subscriber->email);
			$this->email->subject($communication->subject);
			$this->email->message($communication->content);
			$this->email->send();
			//echo $subscriber->email;
			//echo $communication->content;
		}

		redirect('tools/bulkemails');
	}
	
	function bulksms(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Bulk SMS";
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'tools');
		
		$data['query'] = $this->database_model->getOrganizationCommunications($this->session->userdata('organization'), 2 ,NULL, NULL, NULL);
		
    $this->layout->buildPage('tools/bulksms', $data);
	}

	function bulksms_view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['ecid'] = $this->uri->segment(3);
		
		$this->session->set_userdata('menuitem', 'tools');
		
		$data['profile'] = $this->database_model->viewOrganization($this->session->userdata('organization'), NULL ,NULL, NULL, NULL);
		
		$data['communication'] = $this->database_model->viewOrganizationCommunications($data['ecid']);
		$data['title'] = $data['communication']->subject;
		$data['group'] = $data['communication']->group;

		$this->db->select("entities.*");
		$this->db->select("entities_contacts.*");
		$this->db->select("persons.firstname, persons.lastname, persons.mobile");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->where_in('entities.groups', $data['group']);
		$data['subscribers'] = $this->db->get();
		
    $username = $this->session->userdata('sms_username');
		$password = $this->session->userdata('sms_password');
		$api_id = $this->session->userdata('sms_api');			
		$this->curl->open();
		$data['sms'] = $this->curl->http_get("https://api.clickatell.com/http/getbalance?user=".$username ."&password=".$password."&api_id=".$api_id);
		$this->curl->close();
		
        $this->layout->buildPage('tools/bulksms/view', $data);
	}
	
	function bulksms_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "New Bulk SMS";
		$data['subtitle'] = "";
		
		$data['groups'] = $this->database_model->getOrganizationGroups($this->session->userdata('organization'), NULL ,NULL);
		$data['communicationstatus'] = $this->db->get('options_communicationstatus');
		
    $this->layout->buildPage('tools/bulksms/add', $data);
	}
	
	function bulksms_insert(){	
		$this->db->trans_start();
		$this->db->insert('organizations_communications', $_POST);
		$this->db->trans_complete();
		
		redirect('tools/bulksms');
	}
	
	function bulksms_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['ecid'] = $this->uri->segment(3);
		
    $data['communication'] = $this->database_model->viewOrganizationCommunications($data['ecid']);
		$data['title'] = $data['communication']->subject;
		
		$data['communicationstatus'] = $this->db->get('options_communicationstatus');
    $data['groups'] = $this->database_model->getOrganizationGroups($this->session->userdata('organization'), NULL ,NULL);
    $this->layout->buildPage('tools/bulksms/edit', $data);
	}
	
	function bulksms_update(){	
		$this->db->trans_start();
		$this->db->where('ecid', $_POST['ecid']);
		$this->db->update('organizations_communications', $_POST);
		$this->db->trans_complete();

		redirect('tools/bulksms');
	}
	
	function bulksms_send(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['ecid'] = $this->uri->segment(3);
		
		$this->session->set_userdata('menuitem', 'tools');
		
		$data['communication'] = $this->database_model->viewOrganizationCommunications($data['ecid']);
		$data['title'] = $data['communication']->subject;
		$data['group'] = $data['communication']->group;
		
		$this->db->select("entities_communications.*");
		$this->db->from('entities_communications');
		$this->db->where('entities_communications.ecid', $data['ecid']);
		$result = $this->db->get();
		$sms = $result->first_row();
		
		$this->db->select("entities.*");
		$this->db->select("entities_contacts.firstname, entities_contacts.lastname, entities_contacts.mobile");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->where_in('entities.groups', $data['group']);
		$data['subscribers'] = $this->db->get();
		
    $username = $this->session->userdata('sms_username');
		$password = $this->session->userdata('sms_password');
		$api_id = $this->session->userdata('sms_api');	
		
		foreach ($data['subscribers']->result() as $row){
			//$to = '27824155587';
			//$text = $sms->content;			
			//$this->curl->open();
			//$newtext = preg_replace("/ /", "+", $text);
			//$content = $this->curl->http_get("http://api.clickatell.com/http/sendmsg?user=$username&password=$password&api_id=$api_id&to=$to&text=$newtext");
			//$this->curl->close();

			echo $row->firstname." - ".$row->mobile." : ".$sms->content."<br>";
		}
		
		$this->db->where('ecid', $data['ecid']);
		$this->db->update('entities_communications', array('mode' => 2, 'status' => 2));

    //$data['result'] = $content;

    //$this->layout->buildPage('tools/bulksms/result', $data);
	}

}
?>