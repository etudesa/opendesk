<?php

class Dashboard extends CI_Controller {

	function index(){
		$this->session->set_userdata('menuitem', 'dashboard');
    $data['entity'] = $this->uri->segment(3);
		$data['accesslevel'] = $this->session->userdata('accesslevel');

    $data['organization'] = $this->database_model->viewOrganization($this->session->userdata('organization'),NULL,NULL,NULL,NULL);
			
    $query = $this->database_model->getEntities($this->session->userdata('organization'),1 ,1, NULL, $this->session->userdata('uid'), NULL);
		$data['leads'] = $query->num_rows();
		
		$query = $this->database_model->getEntities($this->session->userdata('organization'),1 ,2, NULL, $this->session->userdata('uid'), NULL);		
    $data['prospects'] = $query->num_rows();
		
		$query = $this->database_model->getEntities($this->session->userdata('organization'),1 ,3, NULL, $this->session->userdata('uid'), NULL);
		$data['clients'] = $query->num_rows();
		
    $data['tasks'] = $this->database_model->getUserTasks($this->session->userdata('uid'), $this->session->userdata('organization'),NULL ,NULL);	
		
		$data['reminders'] = 0; //initialize variable
		
		$data['taskstatus'] = $this->db->get('options_usertaskstatus');
		
		$data['entities'] = $this->database_model->getEntities($this->session->userdata('organization'),1 ,NULL, NULL, NULL, NULL);
		
		$data['entitystatus'] = $this->db->get('options_entitystatus');
		
    $data['entitytypes'] = $this->database_model->getEntityTypes(NULL);    
    $data['tickets'] = $this->database_model->getEntityTickets(NULL, NULL , $this->session->userdata('uid'), NULL, NULL, NULL, NULL, NULL, NULL, NULL);    
    $data['entitytickettypes'] = $this->database_model->getTicketTypes(NULL);
		$data['entityticketstatus'] = $this->db->get('options_ticketstatus');
		
    $data['persons'] = $this->database_model->getPersons($this->session->userdata('organization'));
		
    $data['products'] = $this->database_model->getEntityProducts($data['entity'],NULL,NULL,NULL,NULL, NULL);
		
		$data['services'] = $this->database_model->getEntityServices($data['entity'],NULL,NULL,NULL,NULL);
		
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL,NULL,NULL);
		
    $this->layout->buildPage('dashboard/index', $data);
	}
	
	function search(){
		$this->session->set_userdata('menuitem', '');
		
		if (isset($_POST['searchstring'])){
			$data['searchstring'] = $_POST['searchstring'];
		}else{
			$data['searchstring'] = "";
		}
		
    $data['profile'] = $this->database_model->viewOrganization($this->session->userdata('organization'), NULL ,NULL, NULL, NULL);
		$data['title'] = $data['profile']->name;
		
		$this->db->select("entities.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');	
    $this->db->like('name', $data['searchstring']);
    $this->db->or_like('registration', $data['searchstring']);
		$data['entities'] = $this->db->get();
		
		$this->db->select("entities.*");
		$this->db->select("entities_correspondence.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('entities_correspondence', 'entities_correspondence.ecid = entities.eid');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->like('name', $data['searchstring']);
		$this->db->where('entities.organization', $this->session->userdata('organization'));		
		$data['correspondence'] = $this->db->get();
		
		$this->db->select("entities.*");
		$this->db->select("entities_tickets.*");
		$this->db->select("options_ticketstatus.description as entityticketstatus");
		$this->db->select("options_tickettypes.description as entitytickettype");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->join('options_ticketstatus', 'options_ticketstatus.oetsid = entities_tickets.status');
		$this->db->join('options_tickettypes', 'options_tickettypes.oettid = entities_tickets.type');
		$this->db->like('entities_tickets.title', $data['searchstring']);
		$this->db->where('entities.organization', $this->session->userdata('organization'));		
		$data['support'] = $this->db->get();
		
		$this->db->select("options_entitytypes.*");
		$this->db->from('options_entitytypes');
		$data['entitytypes'] = $this->db->get();
		
		$this->db->select("options_entitystatus.*");
		$this->db->from('options_entitystatus');
		$data['entitystatus'] = $this->db->get();
		
		$this->db->select("options_entitymodes.*");
		$this->db->from('options_entitymodes');
		$data['entitymodes'] = $this->db->get();
		
		$this->db->select("organizations_users.*");
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.organization', $this->session->userdata('organization'));
		$data['users'] = $this->db->get();
		
    $this->layout->buildPage('dashboard/search/results', $data);
	}
	
	function advancedsearch(){
		$this->session->set_userdata('menuitem', '');
		
		if (isset($_POST['searchstring'])){
			$data['searchstring'] = $_POST['searchstring'];
		}else{
			$data['searchstring'] = "";
		}
		
	  $data['profile'] = $this->database_model->viewOrganization($this->session->userdata('organization'), NULL ,NULL, NULL, NULL);
		$data['title'] = $data['profile']->name;
		
		$this->db->select("entities.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->where('entities.organization', $this->session->userdata('organization'));	
		//$this->db->where('entities.eid !=', $this->session->userdata('profile'));	
    $this->db->like('name', $data['searchstring']);
		$data['entities'] = $this->db->get();
		
		$this->db->select("entities.*");
		$this->db->select("entities_correspondence.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('entities_correspondence', 'entities_correspondence.ecid = entities.eid');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->like('name', $data['searchstring']);
		$this->db->where('entities.organization', $this->session->userdata('organization'));		
		$data['correspondence'] = $this->db->get();
		
		$this->db->select("entities.*");
		$this->db->select("entities_tickets.*");
		$this->db->select("options_ticketstatus.description as entityticketstatus");
		$this->db->select("options_tickettypes.description as entitytickettype");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->join('options_ticketstatus', 'options_ticketstatus.oetsid = entities_tickets.status');
		$this->db->join('options_tickettypes', 'options_tickettypes.oettid = entities_tickets.type');
		$this->db->like('entities_tickets.title', $data['searchstring']);
		$this->db->where('entities.organization', $this->session->userdata('organization'));		
		$data['support'] = $this->db->get();
		
		$this->db->select("options_entitytypes.*");
		$this->db->from('options_entitytypes');
		$data['entitytypes'] = $this->db->get();
		
		$this->db->select("options_entitystatus.*");
		$this->db->from('options_entitystatus');
		$data['entitystatus'] = $this->db->get();
		
		$this->db->select("options_entitymodes.*");
		$this->db->from('options_entitymodes');
		$data['entitymodes'] = $this->db->get();
		
		$this->db->select("users.*");
		$this->db->from('users');
		$this->db->where('users.organization', $this->session->userdata('organization'));
		$data['users'] = $this->db->get();
		
    $this->layout->buildPage('dashboard/search/results', $data);
	}
	
}
?>