<?php

class Admin extends CI_Controller {

	function index(){
		$this->session->set_userdata('menuitem', 'dashboard');
    $data['entity'] = $this->uri->segment(3);
		$data['accesslevel'] = $this->session->userdata('accesslevel');
		
    $this->db->select("organizations.*");		
    $this->db->from('organizations');
    $data['organizations'] = $this->db->get();
    
    $this->db->select("users.*");		
    $this->db->select("organizations.*");		
		$this->db->from('users');
    $this->db->join('organizations', 'organizations.oid = users.organization');
		$data['query'] = $this->db->get();
    
    $this->layout->buildPage('admin/index', $data);
	}
  
  function user_insert(){
			//insert user data
			$user = array(
			   'fullname' => $_POST['fullname'] ,
			   'accesslevel' => $_POST['accesslevel'] ,
			   'username' => $_POST['username'],
         'organization' => $_POST['organization'],
			);
			$this->db->insert('users', $user);
			$uid = $this->db->insert_id();
    
      redirect('admin/index');
  }
	
  function organization_insert(){

		
			//create the dabatase structure
			$mysqli = new mysqli("localhost", "root", "password",NULL);
			if (mysqli_connect_errno()) { /* check connection */
					printf("Connect failed: %s\n", mysqli_connect_error());
					exit();
			}else{
				$sql = "CREATE DATABASE ".$_POST['database'].";";
				$mysqli->multi_query($sql);
			}
		
			$mysqli = new mysqli("localhost", "root", "password",$_POST['database']);
			if (mysqli_connect_errno()) { /* check connection */
					printf("Connect failed: %s\n", mysqli_connect_error());
					exit();
			}else{
				$sql = file_get_contents('assets/default/database.sql');		
				/* execute multi query */
				if ($mysqli->multi_query($sql)) {
						echo "success";
				} else {
					 echo "error";
				}
			}
      
			//insert user data
			$organization = array(
			   'name' => $_POST['name'] ,
			   'database' => $_POST['database']
			);
			$this->db->insert('organizations', $organization);
			$oid = $this->db->insert_id();
		
      redirect('admin/index');
  }
	
}
?>