<?php

class Account extends CI_Controller {

	function view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', '');

		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
    
    $data['user'] = $this->database_model->viewUser($data['uid']);
    $data['orgid'] = $data['user']->organization;

    $data['title'] = $data['user']->fullname;
    $data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);

		$data['accesslevels'] = $this->db->get('options_useraccesslevels');
		$data['entities'] =  $this->database_model->getEntities($data['orgid'],NULL, NULL, NULL, $data['uid'] , NULL);
		$data['tasks'] = $this->database_model->getUserTasks($data['orgid'],$data['uid'] , NULL);
		$data['tickets'] = $this->database_model->getTickets(NULL,NULL, $data['uid'], NULL, NULL, NULL, NULL);
    $data['folders'] = $this->database_model->getFiles($data['orgid'], NULL);
    
    /* 
    //get imap emails
		if ($data['user']->imap == 1){
			$data['folder'] = 'INBOX';
			$imap_username = $data['user']->imap_email;
			$imap_password = $data['user']->imap_password;
			$imap_server = $data['user']->imap_server;
			$imap_folder = $data['user']->imap_folder;
			$imap_port = $data['user']->imap_port;
			$imap_flags = $data['user']->imap_flags;
			
			//$dsn = sprintf('{%s}%s', IMAP_SERVER, "INBOX");		
			$dsn = "{".$imap_server.":".$imap_port."/".$imap_flags."}".$imap_folder;
			$imap = $this->mail->openMailbox($imap_username,$imap_password,$dsn);
			//$imap = $this->mail->openMailbox("admin@opendesk.co.za","Admin@opend3sk",$dsn);
			$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
			$data['messages'] = $this->mail->getAllMessages($imap,$dsn);
			imap_close($imap);
		}
    */

		$this->db->select("organizations.*");
		$this->db->from('organizations');
    foreach (explode(",",$data['user']->organizations) as $row){
      $this->db->or_where('organizations.orgid', $row[0]);
    }
		$data['organizations'] = $this->db->get();
    
    $this->layout->buildPage('account/view', $data);
	}

	function password(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['uid'] = $this->uri->segment(3);
		
		$this->db->select("users.*");
		$this->db->from('users');
		$this->db->where('users.uid', $data['uid']);
		$users = $this->db->get();
		$data['user'] = $users->first_row();
		$data['title'] = $data['user']->fullname;
		
    $this->layout->buildPage('account/password', $data);
	}
	
	function settings_email(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Email Settings";
		$data['subtitle'] = "";

		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->where('users.uid', $data['uid']);
		$user = $this->db->get();
		$data['user'] = $user->first_row();
		$data['title'] = $data['user']->fullname;
		
		$this->layout->buildPage('account/settings/email', $data);
	}
	
	function settings_email_update(){		
		$this->db->trans_start();
		$this->db->where('uid', $_POST['uid']);
		$this->db->update('users_settings', $_POST);
		$this->db->trans_complete();

		redirect('account/view/' . $_POST['user_id'].'#tab_2');
	}
	
	function settings_seafile(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Seafile Settings";
		$data['subtitle'] = "";

		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->where('users.uid', $data['uid']);
		$user = $this->db->get();
		$data['user'] = $user->first_row();
		$data['title'] = $data['user']->fullname;
		
		$this->layout->buildPage('account/settings/seafile', $data);
	}
	
	function settings_seafile_update(){		
		$this->db->trans_start();
		
		//get user details
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->where('users.uid', $_POST['uid']);
		$result = $this->db->get();
		$user = $result->first_row();
		
		$params = array('user' => $user->seafile_username, 'password' => $user->seafile_password, 'token' => NULL);
		$this->load->library('seafile', $params);
		//$_POST['seafile_token'] = $this->seafile->getToken();
		
		$this->db->where('uid', $_POST['uid']);
		$this->db->update('users_settings', $_POST);
		
		$this->db->trans_complete();

		redirect('account/view/' . $_POST['user_id'].'#tab_2');
	}
	
	function tasks_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		
		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
		$data['user'] = $this->database_model->viewUser($data['uid']);
		$data['title'] = $data['user']->fullname;
		
		$this->db->select("users.*");
		$this->db->from('users');
		$this->db->where('users.organization', $this->session->userdata('organization'));
		$data['users'] = $this->db->get();
		
    $this->layout->buildPage('account/tasks/add', $data);
	}
	
	function tasks_insert(){	
		$this->db->trans_start();
		
		$this->db->insert('users_tasks', $_POST);
		
		//-------compile message content
		$content = $_POST['title']."<br>";
		$content .= "- Title: ".$_POST['title']."<br>";
		$content .= "- Description: ".$_POST['description']."<br>";	
		$content .= "- Due Date: ".$_POST['duedate']."<br>";	
		$content .= "- Reminder Date: ".$_POST['reminderdate']."<br>";	
		
		//-------------send email notice to task owner
		$this->db->from('users');
		$this->db->where('users.uid', $_POST['uid']);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Task Created');
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following task was created and you are the owner: <br><br>";
		$footer = "<br>OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;
		
		$this->db->trans_complete();
		
		redirect('account/view/' . $_POST['uid'].'#tab_1_3');
	}

	function tasks_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'settings');
		$data['utid'] = $this->uri->segment(3);		
    
		$data['task'] = $this->database_model->viewUserTask($data['utid']);
		$data['title'] = $data['task']->fullname;
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL, NULL, NULL, NULL);
		
		$this->db->select("users_tasks_notes.*");
		$this->db->from('users_tasks_notes');
		$this->db->where('users_tasks_notes.utid', $data['utid']);
		$data['usertasknotes'] = $this->db->get();
		
		$data['taskstatus'] = $this->db->get('options_usertaskstatus');
		
    $this->layout->buildPage('account/tasks/edit', $data);
	}
	
	function tasks_update(){		
		
		$this->db->trans_start();
		
		$this->db->where('utid', $_POST['utid']);
		$task = array(
		   'utid' => $_POST['utid'],
		   'uid' => $_POST['uid'],
		   'title' => $_POST['title'],
		   'description' => $_POST['description'],
		   'reminderdate' => $_POST['reminderdate'],
		   'duedate' => $_POST['duedate'],
		   'progress' => $_POST['progress'],
		   'status' => $_POST['status']
		);
		$this->db->update('users_tasks', $task);
		
		
		//-------compile message content
		$content = $_POST['title']."<br>";
		$content .= "- Title: ".$_POST['title']."<br>";
		$content .= "- Description: ".$_POST['description']."<br>";	
		$content .= "- Due Date: ".$_POST['duedate']."<br>";	
		$content .= "- Reminder Date: ".$_POST['reminderdate']."<br>";	
		$content .= "- Progress: ".$_POST['progress']."%<br>";	
		$content .= "- Status: ".$_POST['status']."<br>";	
		
		//-------------send email notice to task owner
		$this->db->from('users');
		$this->db->where('users.uid', $_POST['uid']);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Task Updated');
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following task was updated and you are the owner: <br><br>";
		$footer = "<br>OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;
		
		$this->db->trans_complete();
		

		redirect('account/view/' . $_POST['user_id'].'#tab_1_3');
	}
	
	function tasks_notes_insert(){	
		$this->db->trans_start();
		$this->db->insert('users_tasks_notes', $_POST);
		$this->db->trans_complete();
		
		redirect('account/tasks_edit/' . $_POST['utid']);
	}
	
	function emails_drafts(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Drafts";
		$data['subtitle'] = "";
		$data['folder'] = $this->uri->segment(3);
		$this->session->set_userdata('menuitem', 'emails');
		
		$dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Drafts");		
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$data['messages'] = $this->mail->getAllMessages($imap,$dsn);
		imap_close($imap);
		
		$this->layout->buildPage('account/emails/index', $data);
	}
	
	function emails_sentmail(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Sent Mail";
		$data['subtitle'] = "";
		$data['folder'] = $this->uri->segment(3);
		$this->session->set_userdata('menuitem', 'emails');
		
		$dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Sent Mail");		
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$data['messages'] = $this->mail->getAllMessages($imap,$dsn);
		imap_close($imap);
		
		$this->layout->buildPage('account/emails/index', $data);
	}
	
	function emails_trash(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Trash";
		$data['subtitle'] = "";
		$data['folder'] = $this->uri->segment(3);
		$this->session->set_userdata('menuitem', 'emails');
		
		$dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Trash");		
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$data['messages'] = $this->mail->getAllMessages($imap,$dsn);
		imap_close($imap);
		
		$this->layout->buildPage('account/emails/index', $data);
	}
	
	function emails_compose(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "New Email";
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'emails');

		
		$dsn = sprintf('{%s}%s', IMAP_SERVER, IMAP_FOLDER);
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		imap_close($imap);
		
		$this->layout->buildPage('account/emails/new', $data);
	}
	
	function emails_view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "View Email";
		$data['subtitle'] = "";
		$data['msgno'] = $this->uri->segment(3);
		$data['folder'] = $this->uri->segment(4);

		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->select("entities.name as profilename");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->join('entities', 'entities.eid = users.profile');
		$this->db->where('users.uid', $this->session->userdata('uid'));
		$result = $this->db->get();
		$data['user'] = $result->first_row();
		
		$data['folder'] = 'INBOX';
		$imap_username = $data['user']->imap_email;
		$imap_password = $data['user']->imap_password;
		$imap_server = $data['user']->imap_server;
		$imap_folder = $data['user']->imap_folder;
		$imap_port = $data['user']->imap_port;
		$imap_flags = $data['user']->imap_flags;
		
		$dsn = "{".$imap_server.":".$imap_port."/".$imap_flags."}".$imap_folder;
		$imap = $this->mail->openMailbox($imap_username,$imap_password,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$message = $this->mail->readMessage($imap,$data['msgno']);
		imap_close($imap);
		
		$data['msgoverview'] = $message['overview'];
		$data['msgbody'] = quoted_printable_decode($message['body']);
		$data['msgheader'] = $message['header']->from;
		
		$fromaddress = $data['msgheader'][0]->mailbox."@".$data['msgheader'][0]->host;
		$data['eid'] = $this->database_model->verify_email($fromaddress);
		
		$this->db->select("entities.name");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$result = $this->db->get();
		$data['entity'] = $result->first_row();
		
		$this->layout->buildPage('account/emails/view', $data);
	}
	
	function emails_reply(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Reply Email";
		$data['subtitle'] = "";
		$data['msgno'] = $this->uri->segment(3);
		$data['msgtype'] = $this->uri->segment(4);
		$this->session->set_userdata('menuitem', 'emails');

		
		$dsn = sprintf('{%s}%s', IMAP_SERVER, IMAP_FOLDER);
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$message = $this->mail->readMessage($imap,$data['msgno']);
		imap_close($imap);
		
		$data['msgoverview'] = $message['overview'];
		$data['msgbody'] = quoted_printable_decode($message['body']);
		
		$this->layout->buildPage('account/emails/reply', $data);
	}
	
	function emails_send(){
		$dsn = sprintf('{%s}%s', IMAP_SERVER, IMAP_FOLDER);
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$this->mail->sendMessage($_POST['toaddress'], $_POST['ccaddress'], $_POST['bccaddress'], $_POST['subject'], $_POST['message']);
		imap_close($imap);
		
		redirect('account/view#tab_1_5');
	}
	
	function emails_archive(){

		$data['today'] = date("Y-m-d");
		$data['msgno'] = $this->uri->segment(3);
		
		
		switch ($data['folder']){
			case 1 : $dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Sent Mail");break;
			case 2 : $dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Drafts");break;
			case 3 : $dsn = sprintf('{%s}%s', IMAP_SERVER, "[Gmail]/Trash");break;
			default : $dsn = sprintf('{%s}%s', IMAP_SERVER, "INBOX");break;
		};
		
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->select("entities.name as profilename");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->join('entities', 'entities.eid = users.profile');
		$this->db->where('users.uid', $this->session->userdata('uid'));
		$result = $this->db->get();
		$data['user'] = $result->first_row();
		
		$data['folder'] = 'INBOX';
		$imap_username = $data['user']->imap_email;
		$imap_password = $data['user']->imap_password;
		$imap_server = $data['user']->imap_server;
		$imap_folder = $data['user']->imap_folder;
		$imap_port = $data['user']->imap_port;
		$imap_flags = $data['user']->imap_flags;
		
		$dsn = "{".$imap_server.":".$imap_port."/".$imap_flags."}".$imap_folder;
		$imap = $this->mail->openMailbox($imap_username,$imap_password,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$message = $this->mail->readMessage($imap,$data['msgno']);
		
		$msgoverview = $message['overview'];
		$msgbody = quoted_printable_decode($message['body']);
		$msgheader = $message['header']->from;
		
		$fromaddress = $msgheader[0]->mailbox."@".$msgheader[0]->host;
		$eid = $this->database_model->verify_email($fromaddress);
		
		$this->db->trans_start();
			$correspondence = array(
			   'eid' => $eid,
			   'type' => 3,
			   'description' => $msgoverview[0]->subject,
			   'content' => $msgbody,
			   'createdby' => $this->session->userdata('uid'),
			   'updated' => date('Y-m-d h:m:s')		   
			);
			$this->db->insert('entities_correspondence', $correspondence);
		$this->db->trans_complete();	

		$this->mail->deleteMessage($imap,$data['msgno']);
		imap_close($imap);		
		
		redirect('entities/view/'.$eid.'#tab_2_2');
	}
	
	function emails_link(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Link Email";
		$data['subtitle'] = "";
		$data['msgno'] = $this->uri->segment(3);
		$data['msgtype'] = $this->uri->segment(4);
		$data['folder'] = 4; // hardcoded for now
		
		$data['today'] = date("Y-m-d");
		if ($this->uri->segment(3) != ""){
			$data['msgno'] = $this->uri->segment(3);
		}else{
			$data['msgno'] = $_POST['msgno'];
		}
		
		$imap = $this->mail->openMailbox(IMAP_EMAIL,IMAP_PASSWORD,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$message = $this->mail->readMessage($imap,$data['msgno']);
		imap_close($imap);
		
		$data['msgoverview'] = $message['overview'];
		$msgbody = quoted_printable_decode($message['body']);
		$msgheader = $message['header']->from;
		$fromaddress = $msgheader[0]->mailbox."@".$msgheader[0]->host;
		
		$this->db->select("entities.*");
		$this->db->from('entities');
		//$this->db->where('entities.status', 1);
		$this->db->where('entities.profile', $this->session->userdata('profile'));
		$data['entities'] = $this->db->get();
		
		$this->db->select("entities_tickets.*");
		$this->db->from('entities_tickets');
		//$this->db->where('entities.status', 1);
		//$this->db->where('entities_tickets.profile', $this->session->userdata('profile'));
		$data['tickets'] = $this->db->get();
		
		$this->layout->buildPage('account/emails/link', $data);
	}
	
	function emails_delete(){
		$data['msgno'] = $this->uri->segment(3);
		
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->select("entities.name as profilename");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->join('entities', 'entities.eid = users.profile');
		$this->db->where('users.uid', $this->session->userdata('uid'));
		$result = $this->db->get();
		$data['user'] = $result->first_row();
		
		$data['folder'] = 'INBOX';
		$imap_username = $data['user']->imap_email;
		$imap_password = $data['user']->imap_password;
		$imap_server = $data['user']->imap_server;
		$imap_folder = $data['user']->imap_folder;
		$imap_port = $data['user']->imap_port;
		$imap_flags = $data['user']->imap_flags;
		
		$dsn = "{".$imap_server.":".$imap_port."/".$imap_flags."}".$imap_folder;
		$imap = $this->mail->openMailbox($imap_username,$imap_password,$dsn);
		$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
		$message = $this->mail->deleteMessage($imap,$data['msgno']);
		imap_close($imap);
		
		redirect('account/view#tab_1_5');
	}
}
?>