<?php

class Cron extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		//$this->load->model('Database_model', 'database');
	}	
	
	//--send email birthday reminders to users for today
	function email_birthdays(){				
		$this->load->library('mail');

		$this->db->select("entities.*");
		$this->db->select("entities_contacts.*");
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.eid = entities.eid');
		$this->db->where('entities.mode', 1);
		$this->db->where('entities_contacts.dateofbirth !=', "0000-00-00");
		$this->db->where('SUBSTRING(entities_contacts.dateofbirth,6,6)', date('m-d'));
		$this->db->group_by("entities.profile");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("entities.*");
			$this->db->select("entities_contacts.*");
			$this->db->select("users.fullname, users.imap_email");
			$this->db->from('entities');
			$this->db->join('entities_contacts', 'entities_contacts.eid = entities.eid');
			$this->db->join('users', 'users.uid = entities.createdby');
			$this->db->where('entities.profile', $bigrow->profile);
			$this->db->where('entities.mode', 1);
			$this->db->where('entities_contacts.dateofbirth !=', "0000-00-00");
			$this->db->where('SUBSTRING(entities_contacts.dateofbirth,6,6)', date('m-d'));
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Dear ".$user->fullname.",</p>";
			$message .= "</p>This is a system generated email. Please do not respond to this address.</p>";
			$message .= "<p>The following birthday reminders are set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->firstname."&nbsp;".$row->lastname."</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please login to the system if you wish to send these individuals an SMS.<br>";
			$message .= "If you are receiving incorrect notifications, please contact the system administrator on leroux@etude.co.za<br><br>";
			$message .= "Regards<br>OpenDesk</p>";
			
			$this->email->from('admin@opendesk.co.za', 'OpenDesk');
			$this->email->to($user->imap_email);
			$this->email->subject('OpenDesk - Daily Birthday Reminder');
			$this->email->message($message);
			//$this->email->send();
			//echo $this->email->print_debugger();
			echo $message;
			echo "<hr>";
		}
		
	}
	
	//--view ticket reminders to all users for today
	function email_dailytickets(){
		
		$this->load->library('email');
		$eid = $this->uri->segment(3);
		$action = $this->uri->segment(4);

		//ticket reminders today
		//assigned to user
		$this->db->select("entities.*");
		$this->db->select("entities_tickets.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities_tickets.reminderdate', date("Y-m-d"));
		$this->db->where('entities.profile', $eid);
		$this->db->group_by("entities_tickets.assignedto");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.assignedto');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.assignedto', $bigrow->assignedto);
			$this->db->where('entities_tickets.reminderdate', date("Y-m-d"));
			$this->db->order_by("entities_tickets.reminderdate","ASC");
			$query = $this->db->get();
			$user = $query->first_row();
			
			$message = "<p>Morning ".$user->fullname.",</p>";
			$message .= "<p>The following tickets are assigned to you and have reminders set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please login to the system and update these tickets. You will also receive a final email notice prior to the due date of your tickets.<br></p>";
			$message .= "<p>This is a system generated email, please do not respond to this, however If you are receiving incorrect notifications, please contact the system administrator on leroux@etude.co.za<br><br></p>";
			$message .= "";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Ticket reminders for today');
				$this->email->message($message);
				$this->email->send();
			}
		}
		
		//tickets due today 
		//assigned to user
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities_tickets.duedate', date("Y-m-d"));
		$this->db->where('entities.profile', $eid);
		$this->db->group_by("entities_tickets.assignedto");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.assignedto');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.assignedto', $bigrow->assignedto);
			$this->db->where('entities_tickets.duedate', date("Y-m-d"));
			$this->db->order_by("entities_tickets.duedate","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Morning ".$user->fullname.",</p>";
			$message .= "<p>The following tickets are assigned to you and have due dates set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please <a href='".site_url('authentication/login')."'>click here</a> to login to the system and update these tickets.</p>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Tickets due today');
				$this->email->message($message);
				$this->email->send();
			}
		}
		
		//tickets overdue 
		//assigned to user
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities_tickets.duedate <', date("Y-m-d"));
		$this->db->where('entities.profile', $eid);
		$this->db->group_by("entities_tickets.assignedto");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.assignedto');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.assignedto', $bigrow->assignedto);
			$this->db->where('entities_tickets.duedate <', date("Y-m-d"));
			$this->db->order_by("entities_tickets.duedate","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Morning ".$user->fullname.",</p>";
			$message .= "<p>The following tickets are assigned to you and have passed their due dates:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please <a href='".site_url('authentication/login')."'>click here</a> to login to the system and update these tickets.</p>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Tickets overdue today');
				$this->email->message($message);
				$this->email->send();
				//echo $this->email->print_debugger();
			}
		}
		
		//ticket reminders today
		//owned by user
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities_tickets.reminderdate', date("Y-m-d"));
		$this->db->where('entities.profile', $eid);
		$this->db->group_by("entities_tickets.ownedby");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.ownedby');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.ownedby', $bigrow->ownedby);
			$this->db->where('entities_tickets.reminderdate', date("Y-m-d"));
			$this->db->order_by("entities_tickets.reminderdate","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Dear ".$user->fullname.",</p>";
			$message .= "</p>This is a system generated email. Please do not respond to this address.</p>";
			$message .= "<p>The following tickets are owned by you and have reminders set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please <a href='".site_url('authentication/login')."'>click here</a> to login to the system and update these tickets.</p>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Ticket reminders for today');
				$this->email->message($message);
				$this->email->send();
				//echo $this->email->print_debugger();
			}
		}
		
		//tickets due today
		//owned by user
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities.profile', $eid);
		$this->db->where('entities_tickets.duedate', date("Y-m-d"));
		$this->db->group_by("entities_tickets.ownedby");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.ownedby');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.ownedby', $bigrow->ownedby);
			$this->db->where('entities_tickets.duedate', date("Y-m-d"));
			$this->db->order_by("entities_tickets.duedate","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Morning ".$user->fullname.",</p>";
			$message .= "<p>The following tickets are owned by you and have due dates set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please <a href='".site_url('authentication/login')."'>click here</a> to login to the system and update these tickets.</p>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Tickets due today');
				$this->email->message($message);
				$this->email->send();
				//echo $this->email->print_debugger();
			}
		}
		
		//tickets overdue
		//owned by user
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->where('entities_tickets.status', 1);
		$this->db->where('entities.profile', $eid);
		$this->db->where('entities_tickets.duedate <', date("Y-m-d"));
		$this->db->group_by("entities_tickets.ownedby");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("entities_tickets.*");
			$this->db->select("entities.*");
			$this->db->select("options_entitytickettypes.description as tickettypedescription");
			$this->db->select("users.*");
			$this->db->from('entities');
			$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
			$this->db->join('options_entitytickettypes', 'options_entitytickettypes.oettid = entities_tickets.type');
			$this->db->join('users', 'users.uid = entities_tickets.ownedby');
			$this->db->where('entities_tickets.status', 1);
			$this->db->where('entities_tickets.ownedby', $bigrow->ownedby);
			$this->db->where('entities_tickets.duedate <', date("Y-m-d"));
			$this->db->order_by("entities_tickets.duedate","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Morning ".$user->fullname.",</p>";
			$message .= "<p>The following tickets are owned by you and have passed their due dates:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->tickettypedescription." - ".$row->name." (".$row->title.")</li>";
			}		
			$message .= "</ul>";
						$message .= "<p>Please <a href='".site_url('authentication/login')."'>click here</a> to login to the system and update these tickets.</p>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Tickets overdue today');
				$this->email->message($message);
				$this->email->send();
				//echo $this->email->print_debugger();
			}
		}
	
	}
	
	//--send email task reminders to all users for today
	function email_dailytasks(){
		
		$this->load->library('email');
		$action = $this->uri->segment(3);

		//due today
		$this->db->select("users_tasks.*");
		$this->db->select("users.fullname");
		$this->db->from('users_tasks');
		$this->db->join('users', 'users.uid = users_tasks.uid');
		$this->db->where('users_tasks.status', 1);
		$this->db->where('users_tasks.duedate', date("Y-m-d"));
		$this->db->group_by("users_tasks.uid");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("users_tasks.*");
			$this->db->select("users.fullname");
			$this->db->from('users_tasks');
			$this->db->join('users', 'users.uid = users_tasks.uid');
			$this->db->where('users_tasks.status', 1);
			$this->db->where('users_tasks.uid', $bigrow->uid);
			$this->db->where('users_tasks.duedate', date("Y-m-d"));
			$this->db->order_by("users_tasks.uid","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Dear ".$user->fullname.",</p>";
			$message .= "</p>This is a system generated email. Please do not respond to this address.</p>";
			$message .= "<p>The following task reminders are set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->title." - ".$row->description." (".$row->progress."% completed)</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please login to the system and update these tasks. You will also receive a final email notice prior to the due date of your task.<br>";
			$message .= "If you are receiving incorrect notifications, please contact the system administrator on leroux@etude.co.za<br><br>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->cc('leroux@etude.co.za');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Task Reminders for today');
				$this->email->message($message);
			}
		}
		
		//reminder today
		$this->db->select("users_tasks.*");
		$this->db->select("users.fullname");
		$this->db->from('users_tasks');
		$this->db->join('users', 'users.uid = users_tasks.uid');
		$this->db->where('users_tasks.status', 1);
		$this->db->where('users_tasks.reminderdate', date("Y-m-d"));
		$this->db->group_by("users_tasks.uid");
		$bigquery = $this->db->get();
		
		foreach ($bigquery->result() as $bigrow){
		
			$this->db->select("users_tasks.*");
			$this->db->select("users.fullname");
			$this->db->from('users_tasks');
			$this->db->join('users', 'users.uid = users_tasks.uid');
			$this->db->where('users_tasks.status', 1);
			$this->db->where('users_tasks.uid', $bigrow->uid);
			$this->db->where('users_tasks.reminderdate', date("Y-m-d"));
			$this->db->order_by("users_tasks.uid","ASC");
			$query = $this->db->get();
			
			$user = $query->first_row();
			$message = "<p>Dear ".$user->fullname.",</p>";
			$message .= "</p>This is a system generated email. Please do not respond to this address.</p>";
			$message .= "<p>The following task reminders are set for today:</p>";
			$message .= "<ul>";
			foreach ($query->result() as $row){
				$message .= "<li>".$row->title." - ".$row->description." (".$row->progress."% completed)</li>";
			}		
			$message .= "</ul>";
			$message .= "<p>Please login to the system and update these tasks. You will also receive a final email notice prior to the due date of your task.<br>";
			$message .= "If you are receiving incorrect notifications, please contact the system administrator on leroux@etude.co.za<br><br>";
			$message .= "Regards<br>OpenDesk</p>";
			
			if ($action == 0){
				echo $message;
				echo "<hr>";
			}else{
				$this->email->from('admin@opendesk.co.za', 'OpenDesk');
				$this->email->cc('leroux@etude.co.za');
				$this->email->to($user->imap_email);
				$this->email->subject('OpenDesk - Task Reminders for today');
				$this->email->message($message);
			}
		}
	}
	
	function email_contactus(){
		$this->load->library('user_agent');
		
		$message = "Name: ".$_POST['firstname']." ".$_POST['lastname']."<br>";
		$message .= "Phone: ".$_POST['phone']."<br>";
		$message .= "Email: ".$_POST['email']."<br><br>";
		$message .= "Message: ".$_POST['message']."<br>";
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to('admin@opendesk.co.za');
		$this->email->subject('OpenDesk - Contact Us Form Received');
		$this->email->message($message);
		
		$this->email->send();
		
		redirect($this->agent->referrer());
	}
	
	function email_demorequest(){
		$this->load->library('user_agent');
		
		$message = "Name: ".$_POST['name']."<br>";
		$message .= "Email: ".$_POST['email']."<br><br>";
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to('admin@opendesk.co.za');
		$this->email->subject('OpenDesk - Demo Request Received');
		$this->email->message($message);
		
		$this->email->send();
		
		redirect($this->agent->referrer());
	}
}
?>