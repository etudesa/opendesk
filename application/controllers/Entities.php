<?php

class Entities extends CI_Controller {

	function leads(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['status'] = "1";
		$data['uid'] = $this->uri->segment(3);
    if ($data['uid'] != NULL){
      $data['title'] = "My Leads";
    }else{
      $data['title'] = "All Leads";
    }
		$this->session->set_userdata('menuitem', 'leads');
    
    $data['query'] = $this->database_model->getEntities($this->session->userdata('organization'),1 ,1, NULL,  $data['uid'], NULL);
		
    $this->layout->buildPage('entities/index', $data);
	}
	
	function prospects(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['status'] = "2";
		$data['uid'] = $this->uri->segment(3);
    if ($data['uid'] != NULL){
      $data['title'] = "My Prospects";
    }else{
      $data['title'] = "All Prospects";
    }
		$this->session->set_userdata('menuitem', 'prospects');
		
    $data['query'] = $this->database_model->getEntities($this->session->userdata('organization'),1 ,2, NULL,  $data['uid'], NULL);

    $this->layout->buildPage('entities/index', $data);
	}
	
	function clients(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['status'] = "3";
		$data['uid'] = $this->uri->segment(3);
    if ($data['uid'] != NULL){
      $data['title'] = "My Clients";
    }else{
      $data['title'] = "All Clients";
    }
    
		$this->session->set_userdata('menuitem', 'clients');

    $data['query'] = $this->database_model->getEntities($this->session->userdata('organization'),1 ,3, NULL, $data['uid'], NULL);
		
		$this->db->select("*");
		$this->db->from('options_entityfilters');
		$this->db->where('options_entityfilters.eid', $this->session->userdata('organization'));
		$data['filters'] = $this->db->get();
		
    $this->layout->buildPage('entities/index', $data);
	}
	
	function filter(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Clients";
		$data['status'] = "3";
		$this->session->set_userdata('menuitem', 'clients');

		$this->db->select("*");
		$this->db->from('options_entityfilters');
		$this->db->where('options_entityfilters.oefid', $this->uri->segment(3));
		$data['filters'] = $this->db->get();
		$filter = $data['filters']->first_row();
		$data['subtitle'] = "[".$filter->title."]";
		
		$data['query'] = $this->db->query($filter->query);
		
        $this->layout->buildPage('entities/index', $data);
	}
	
	function exports(){
		//generate export here using PDF library
		$this->load->library('pdf');
		$data['title'] = "";
    $data['mode'] = "modal";
		$data['eid'] = $this->uri->segment(3);
		$data['pageheader'] = FALSE;
		$data['pagetopbar'] = FALSE;
		$data['pagesidebar'] = FALSE;
		$data['pagefooter'] = FALSE;

    $_SESSION["layout"] = "portrait";
    $_SESSION["header"] = "yes";
    $_SESSION["footer"] = "yes";
        
		$this->db->select("entities.*");
		$this->db->select("entities_contacts.*");
		$this->db->select("organizations_users.fullname as entityowner");
		$this->db->select("persons.*");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('organizations_users', 'organizations_users.uid = entities.createdby');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact', 'left outer');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$data['entity'] = $entities->first_row();
		
		$this->db->select("entities_products.*");
		$this->db->select("options_productproviders.description as productproviderdescription");
		$this->db->select("options_producttypes.description as producttypedescription");
		$this->db->select("options_productstatus.description as productstatusdescription");
		$this->db->select("options_productcategories.description as productcategorydescription");
		$this->db->from('entities_products');
		$this->db->join('options_productproviders', 'options_productproviders.oeppid = entities_products.provider');
		$this->db->join('options_producttypes', 'options_producttypes.oeptid = entities_products.type');
		$this->db->join('options_productstatus', 'options_productstatus.oepsid = entities_products.status');
		$this->db->join('options_productcategories', 'options_productcategories.oepcid = entities_products.category');
		$this->db->where('entities_products.eid', $data['eid']);
		$data['products'] = $this->db->get();
		
		$this->layout->buildPage('entities/export', $data);	
	}
	
	function view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['eid'] = $this->uri->segment(3);
		
    $data['profile'] = $this->database_model->viewOrganization($this->session->userdata('organization'), NULL ,NULL, NULL, NULL);

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
    
    if ($data['entity']){
      if ($data['entity']->organization == $this->session->userdata('organization')){
        $data['title'] = $data['entity']->name;
        $data['subtitle'] = $data['entity']->entitystatus;

        $data['entitygroups'] = $this->db->get('organizations_groups');
        $data['entitymodes'] = $this->db->get('options_entitymodes');
        $data['entitystatus'] = $this->db->get('options_entitystatus');
        $data['entitytypes'] = $this->database_model->getEntityTypes($this->session->userdata('organization'));

        $data['contacts'] = $this->database_model->getContacts($data['eid'],NULL ,NULL, NULL, NULL, NULL);		 
        $data['sms'] = $this->database_model->getCorrespondence($data['eid'],1 ,NULL);		 
        $data['emails'] = $this->database_model->getCorrespondence($data['eid'],3 ,NULL);		 		
        $data['comments'] = $this->database_model->getCorrespondence($data['eid'],4 ,NULL);		 

        $data['pendingtickets'] = $this->database_model->getTickets($data['eid'], NULL, NULL, NULL,NULL, NULL, 1 );		 		
        $data['completedtickets'] = $this->database_model->getTickets($data['eid'], NULL, NULL, NULL,NULL, NULL, 2 );		 
        $data['unresolvedtickets'] = $this->database_model->getTickets($data['eid'], NULL, NULL, NULL,NULL, NULL, 3 );		 

        $data['products'] = $this->database_model->getProducts($data['eid'], NULL, NULL, NULL,NULL, NULL );		 
        $data['services'] = $this->database_model->getServices($data['eid'], NULL, NULL, NULL,NULL, NULL );		 		
        $data['medicals'] = $this->database_model->getMedicals($data['eid']);		 
        $data['financials'] = $this->database_model->getFinancials($data['eid']);		 
        $data['activities'] = $this->database_model->getActivities($data['eid'], NULL, NULL);		 
        $data['all_files'] = $this->database_model->getFiles($data['eid'], NULL);
        $data['seafile_files'] = $this->database_model->getFiles($data['eid'],1);

        $this->layout->buildPage('entities/view', $data);
      }else{        
        $data['title'] = "Access Denied";
        $data['subtitle'] = "";
        $data['message'] = "You do not have the rights to view this entity. Please contact admin@opendesk.co.za";
        $this->layout->buildPage('entities/error', $data);
      }
    }else{
      $data['title'] = "We could not find the entity you are looking for.";
      $data['subtitle'] = "";
      $data['message'] = "Please try to search for the ID number instead.";
      $this->layout->buildPage('entities/error', $data);
    }
	}
	
	function access(){
		$this->load->library('email');
		
		//send out welcome email to primary contact
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($entity->email);
		$this->email->subject('OpenDesk - Access Granted');
		if ($entity->primarycontact != ""){
			$header = "Dear ".$entity->firstname.",<br><br>";
			$content = "We are excited to welcome your business to the OpenDesk platform. The profile for ".$entity->name." has been activated and is now accessible using the following secure link: <br>";
			$content .= "- https://www.opendesk.co.za<br><br>";
			$footer = "If this email was received in error or you have lost your login credentials, please contact the system administrator on admin@opendesk.co.za.<br><br>";
			$footer .= "OpenDesk";
			$this->email->message($header.$content.$footer);
			$this->email->send();
		}
		//echo $header.$content.$footer;
		
		//send out welcome email to entity owner
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($entity->useremail);
		$this->email->subject('OpenDesk - Access Granted');
		$header = "Dear ".$entity->userfullname.",<br><br>";
		$content = "The profile for ".$entity->name." on OpenDesk has been activated for online access The following person is listed as the primary contact and received a notication: <br>";
		$content .= "- ".$entity->firstname." ".$entity->lastname."<br><br>";
		$footer .= "OpenDesk";
		$this->email->message($header.$content.$footer);
		$this->email->send();
		//echo $header.$content.$footer;
		
		$this->db->trans_complete();
		
		redirect('entities/manage/'.$eid);
	}
	
	function manage(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', '');		
		
		if ($this->uri->segment(3) != NULL){
			$data['eid'] = $this->uri->segment(3);
		}else{
			$data['eid'] = $this->session->userdata('profile');
		}

		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$entity = $entities->first_row();
		$data['title'] = $entity->name;
		
		if ($entity->access != NULL){
			
			$this->db->select("entities.*");
			$this->db->select("entities_settings.*");
			$this->db->select("entities_options.*");
			$this->db->from('entities');
			$this->db->join('entities_settings', 'entities_settings.eid = entities.eid');
			$this->db->join('entities_options', 'entities_options.eid = entities.eid');
			$this->db->where('entities.eid', $data['eid']);
			$entity = $this->db->get();
			$data['entity'] = $entity->first_row();
			$data['title'] = $data['entity']->name;

			$this->db->select("organizations_users.*");
			$this->db->select("options_userstatus.description as userstatus");
			$this->db->select("options_useraccesslevels.description as useraccesslevel");
			$this->db->from('organizations_users');
			$this->db->join('options_userstatus', 'options_userstatus.ousid = organizations_users.status');
			$this->db->join('options_useraccesslevels', 'options_useraccesslevels.oualid = organizations_users.accesslevel');
			$this->db->where('organizations_users.profile', $data['eid']);
			$data['organizations_users'] = $this->db->get();
			$data['userscount'] = $data['users']->num_rows();
	
			$this->db->select("entities_groups.*");
			$this->db->from('entities_groups');
			$this->db->where('entities_groups.eid', $data['eid']);
			$data['groups'] = $this->db->get();
			$data['groupscount'] = $data['groups']->num_rows();
			
			$this->db->select("entities_settings.*");
			$this->db->from('entities_settings');
			$this->db->where('entities_settings.eid', $data['eid']);
			$query = $this->db->get();
			$data['setting'] = $query->first_row();
	
			$this->db->select("entities_options.productcategories");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_productcategories = $data['entities_options']->productcategories;
			$data['entity_productcategories'] = explode(',',$entity_productcategories);
			$this->db->select("options_entityproductcategories.*");
			$this->db->from('options_entityproductcategories');
			$this->db->order_by('options_entityproductcategories.description','ASC');
			$data['all_productcategories'] = $this->db->get();
	
			$this->db->select("entities_options.producttypes");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_producttypes = $data['entities_options']->producttypes;
			$data['entity_producttypes'] = explode(',',$entity_producttypes);
			$this->db->select("options_entityproducttypes.*");
			$this->db->from('options_entityproducttypes');
			$this->db->where('options_entityproducttypes.profile', $data['eid']);
			$this->db->order_by('options_entityproducttypes.description','ASC');
			$data['all_producttypes'] = $this->db->get();
	
			$this->db->select("entities_options.productproviders");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_productproviders = $data['entities_options']->productproviders;
			$data['entity_productproviders'] = explode(',',$entity_productproviders);
			$this->db->select("options_entityproductproviders.*");
			$this->db->from('options_entityproductproviders');
			$this->db->order_by('options_entityproductproviders.description','ASC');
			$data['all_productproviders'] = $this->db->get();
			
			$this->db->select("entities_options.tickettypes");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_tickettypes = $data['entities_options']->tickettypes;
			$data['entity_tickettypes'] = explode(',',$entity_tickettypes);
			$this->db->select("options_entitytickettypes.*");
			$this->db->from('options_entitytickettypes');
			$this->db->where('options_entitytickettypes.profile', $data['eid']);
			$this->db->order_by('options_entitytickettypes.description','ASC');
			$data['all_tickettypes'] = $this->db->get();
			
			$this->db->select("entities_options.servicetypes");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_servicetypes = $data['entities_options']->servicetypes;
			$data['entity_servicetypes'] = explode(',',$entity_servicetypes);
			$this->db->select("options_entityservicetypes.*");
			$this->db->from('options_entityservicetypes');
			$this->db->where('options_entityservicetypes.profile', $data['eid']);
			$this->db->order_by('options_entityservicetypes.description','ASC');
			$data['all_servicetypes'] = $this->db->get();
			
			$this->db->select("entities_options.entitytypes");
			$this->db->from('entities_options');
			$this->db->where('entities_options.eid', $data['eid']);
			$profile_options = $this->db->get();
			$data['entities_options'] = $profile_options->first_row();
			$entity_entitytypes = $data['entities_options']->entitytypes;
			$data['entity_entitytypes'] = explode(',',$entity_entitytypes);
			$this->db->select("options_entitytypes.*");
			$this->db->from('options_entitytypes');
			$this->db->order_by('options_entitytypes.description','ASC');
			$data['all_entitytypes'] = $this->db->get();
			
			$this->layout->buildPage('entities/manage', $data);
		}else{
			$data['message'] = "This feature is not activated for the entity.";
			$this->layout->buildPage('entities/error', $data);
		}
	}
	
	function open(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', '');		
		
		if ($this->uri->segment(3) != NULL){
			$data['eid'] = $this->uri->segment(3);
		}else{
			$data['eid'] = $this->session->userdata('profile');
		}

		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$entity = $entities->first_row();
		$data['title'] = $entity->name;
		
		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$result = $this->db->get();
		$profile = $result->first_row();
		$this->session->set_userdata('entityname', $profile->name);
		$this->session->set_userdata('profile', $profile->eid);
		
		redirect('dashboard');
	}
	
	function add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['title'] = "New Entity";
	
    if ($this->uri->segment(3) != NULL){
			$data['status'] = $this->uri->segment(3);
		}else{
			$data['status'] = NULL;
		}
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization'), NULL, NULL, NULL);
		$data['entitytypes'] = $this->database_model->getEntityTypes($this->session->userdata('organization'));
		$data['entitystatus'] = $this->db->get('options_entitystatus');
    $data['persons'] = $this->database_model->getPersons($this->session->userdata('organization'));
		
    $this->layout->buildPage('entities/add', $data);	
	}

	function insert(){	
		
		$this->db->select("entities.name");
		$this->db->from('entities');
		$this->db->like('entities.name', $_POST['name']); 
		$result = $this->db->get();
		
		if ($result->num_rows() > 0){
			$this->session->set_flashdata('message', 'The entity could not be created since a matching entity already exists. Please try again.');
			redirect('entities/add/'.$_POST['status']);
		}else{
			$this->db->trans_start();
			
			//insert entity data
			$entity = array(
			   'mode' => "1",
			   'name' => $_POST['name'] ,
			   'registration' => $_POST['registration'] ,
			   'status' => $_POST['status'] ,
			   'createdby' => $_POST['createdby'] ,
			   'type' => $_POST['type'] ,
			   'updated' => date("Y-m-d"),
			   'organization' => $this->session->userdata('organization'),
			);
			$this->db->insert('entities', $entity);
			$eid = $this->db->insert_id();
			
			if (isset($_POST['person'])){
				if ($_POST['person'] == ""){
					
					//insert new person data
					$person = array(
					   'organization' => $this->session->userdata('organization'),
					   'firstname' => $_POST['firstname'] ,
					   'lastname' => $_POST['lastname'] ,
					   'preferredname' => $_POST['preferredname'] ,
					   'idnumber' => $_POST['idnumber'] ,
					   'dateofbirth' => $_POST['dateofbirth'] ,
					   'physicaladdress' => $_POST['physicaladdress'] ,
					   'postaladdress' => $_POST['postaladdress'] ,
					   'officephone' => $_POST['officephone'] ,
					   'homephone' => $_POST['homephone'] ,
					   'mobile' => $_POST['mobile'] ,
					   'email' => $_POST['email'] ,
					   'updated' => date('Y-m-d')
					);
					$this->db->insert('persons', $person);
					$pid = $this->db->insert_id();
				}else{
					$pid = $_POST['person'];
				}
				
				//insert contact data
				$contact = array(
				   'eid' => $eid ,
				   'pid' => $pid
				   //'relationship' => $_POST['relationship'],
				);
				$this->db->insert('entities_contacts', $contact);
				$ecid = $this->db->insert_id();
				
				//set the new contact as primary contact
				$this->db->where('eid', $eid);
				$this->db->update('entities', array('primarycontact' => $ecid));
        
        
        /*get entity settings
        $this->db->select("entities_settings.*");
				$this->db->from('entities_settings');
				$this->db->where('entities_settings.eid', $this->session->userdata('profile'));
				$result = $this->db->get();
				$entitysettings = $result->first_row();
				
				//get user settings				
				$this->db->select("users_settings.*");
				$this->db->from('users_settings');
				$this->db->where('users_settings.uid', $this->session->userdata('uid'));
				$result = $this->db->get();
				$usersettings = $result->first_row();
				
				if ($usersettings->seafile == 1){
					$params = array('user' => $usersettings->seafile_username, 'password' => $usersettings->seafile_password, 'token' => NULL);
					$this->load->library('seafile', $params);
					$seafile_library = $entitysettings->seafile_library;
					$this->seafile->createNewDirectory($seafile_library,$_POST['name']);
				}
        */
        
			}
			
			$this->db->trans_complete();
			redirect('entities/view/' . $eid);
		}
	}
	
	function edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		
		$data['entity'] = $this->database_model->viewEntity($data['eid'], NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization'), NULL, NULL, NULL);
		$data['entitytypes'] = $this->database_model->getEntityTypes($this->session->userdata('organization'));
		$data['entitystatus'] = $this->db->get('options_entitystatus');
		$data['groups'] = $this->database_model->getOrganizationGroups($this->session->userdata('organization'),1,NULL);
		
    $this->layout->buildPage('entities/edit', $data);
	}
	
	function update(){
		
		$this->db->trans_start();
		
		//get user details
		$user = $this->database_model->viewUser($this->session->userdata('uid'));
		
		//get old entity details
		$data['entity'] = $this->database_model->viewEntity($_POST['eid'], NULL, NULL, NULL, NULL, NULL);
		
    /*
		if ($user->seafile == 1){
			$params = array('user' => $user->seafile_username, 'password' => $user->seafile_password, 'token' => $user->seafile_token);
			$this->load->library('seafile', $params);
			switch ($_POST['status']){
				case 1 : $this->seafile->renameDirectory($user->seafile_leadlibrary,$data['entity']->name, $_POST['name']);break;
				case 2 : $this->seafile->renameDirectory($user->seafile_prospectlibrary,$data['entity']->name, $_POST['name']);break;
				case 3 : $this->seafile->renameDirectory($user->seafile_clientlibrary,$data['entity']->name, $_POST['name']);break;
			}
		}
    */
		
		$this->db->where('eid', $_POST['eid']);
		$this->db->update('entities', $_POST);
		
		$this->db->trans_complete();
		
		redirect('entities/view/' . $_POST['eid']);
	}
	
	function delete(){
		
		$this->db->trans_start();
		$this->db->delete('entities', array('eid' => $this->uri->segment(3)));
		$this->db->delete('entities_activities', array('eid' => $this->uri->segment(3)));
		$this->db->delete('entities_communications', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_correspondence', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_contacts', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_financials', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_medicals', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_products', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_services', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_tickets', array('eid' => $this->uri->segment(3)));
    $this->db->delete('entities_tickets_notes', array('eid' => $this->uri->segment(3)));
		$this->db->trans_complete();

		redirect('entities/'.$this->uri->segment(4));
	}
	
	function primary(){
		$data['eid'] = $this->uri->segment(3);
		$data['ecid'] = $this->uri->segment(4);
	
		$this->db->trans_start();
		$this->db->where('eid', $data['eid']);
		$this->db->update('entities', array('primarycontact' => $data['ecid']));
		$this->db->trans_complete();

		redirect('entities/view/' . $data['eid']);
	}
	
	function mode(){
		$data['eid'] = $this->uri->segment(3);
		$data['oemid'] = $this->uri->segment(4);
	
		$this->db->trans_start();
		$this->db->where('eid', $data['eid']);
		$this->db->update('entities', array('mode' => $data['oemid']));
		$this->db->trans_complete();

		redirect('entities/view/' . $data['eid']);
	}
	
	function status(){
		$data['eid'] = $this->uri->segment(3);
		$data['oesid'] = $this->uri->segment(4);
	
		$this->db->trans_start();
		$this->db->where('eid', $data['eid']);
		$this->db->update('entities', array('status' => $data['oesid']));
		$this->db->trans_complete();

		redirect('entities/view/' . $data['eid']);
	}
	
	function contacts_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		
		$data['entity'] = $this->database_model->viewEntity($data['eid'] , NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		$data['relationships'] = $this->db->get("options_contactrelationships");
		$data['persons'] = $this->database_model->getPersons($this->session->userdata('organization'));
		
    $this->layout->buildPage('entities/contacts/add', $data);
	}
	
	function contacts_insert(){
		
		$this->db->trans_start();
		if ($_POST['pid'] != ""){
			$contact = array(
			   'pid' => $_POST['pid'] ,
			   'eid' => $_POST['eid'] ,
			   'relationship' => $_POST['relationship']
			);
			$this->db->insert('entities_contacts', $contact);
		}else{
			$person = array(
			   'firstname' => $_POST['firstname'] ,
			   'lastname' => $_POST['lastname'] ,
			   'preferredname' => $_POST['preferredname'] ,
			   'dateofbirth' => $_POST['dateofbirth'] ,
			   'physicaladdress' => $_POST['physicaladdress'] ,
			   'postaladdress' => $_POST['postaladdress'] ,
			   'officephone' => $_POST['officephone'] ,
			   'homephone' => $_POST['homephone'] ,
			   'email' => $_POST['email'],
			   'mobile' => $_POST['mobile'],
			   'organization' => $this->session->userdata('organization'),
			);
			$this->db->insert('persons', $person);
			$pid = $this->db->insert_id();
			$contact = array(
			   'pid' => $pid ,
			   'eid' => $_POST['eid'] ,
			   'relationship' => $_POST['relationship']
			);
			$this->db->insert('entities_contacts', $contact);
		}
		$this->db->trans_complete();
		
		redirect('entities/view/' . $_POST['eid']);
	}
	
	function contacts_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['ecid'] = $this->uri->segment(4);
		

		$data['contact'] = $this->database_model->viewContact($data['ecid']);
		$data['title'] = $data['contact']->name;
		$data['relationships'] = $this->db->get("options_contactrelationships");
		
    $this->layout->buildPage('entities/contacts/edit', $data);
	}
	
	function contacts_update(){
		$this->db->trans_start();
		$person = array(
		   'firstname' => $_POST['firstname'] ,
		   'lastname' => $_POST['lastname'] ,
		   'preferredname' => $_POST['preferredname'] ,
		   'dateofbirth' => $_POST['dateofbirth'] ,
		   'physicaladdress' => $_POST['physicaladdress'] ,
		   'postaladdress' => $_POST['postaladdress'] ,
		   'homephone' => $_POST['homephone'] ,
		   'officephone' => $_POST['officephone'] ,
		   'email' => $_POST['email'],
		   'mobile' => $_POST['mobile'],
		   'organization' => $this->session->userdata('organization'),
		   'field1' => $_POST['field1'],
		   'field2' => $_POST['field2'],
		   'field3' => $_POST['field3'],
		   'field4' => $_POST['field4'],
		   'field5' => $_POST['field5']
		);
		$this->db->where('pid', $_POST['pid']);
		$this->db->update('persons', $person);
		$contact = array(
		   'pid' => $_POST['pid'] ,
		   'eid' => $_POST['eid'] ,
		   'relationship' => $_POST['relationship']
		);
		$this->db->where('ecid', $_POST['ecid']);
		$this->db->update('entities_contacts', $contact);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_1_2');
	}
	
	function contacts_delete(){
		$this->db->trans_start();
		$this->db->delete('entities_contacts', array('ecid' => $this->uri->segment(4)));
		$this->db->trans_complete();

		redirect('entities/view/'.$this->uri->segment(3));
	}
	
	function comments_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		
		$data['entity'] = $this->database_model->viewEntity($data['eid'] , NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization') , NULL, NULL, NULL, NULL, NULL);
		
    $this->layout->buildPage('entities/comments/add', $data);
	}
	
	function comments_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_correspondence', $_POST);
		$this->db->trans_complete();
		
		redirect('entities/view/' . $_POST['eid']);
	}
	
	function tickets_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		
		$data['entity'] = $this->database_model->viewEntity($data['eid'] , NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		
		$data['products'] = $this->database_model->getProducts($data['eid'], NULL, NULL, NULL,NULL, NULL );		 
		$data['services'] = $this->database_model->getServices($data['eid'], NULL, NULL, NULL,NULL, NULL );		 		    
    $data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL,NULL,NULL);
		
		$data['entitytickettypes'] = $this->database_model->getTicketTypes($this->session->userdata('organization'));
		$data['entityticketstatus'] = $this->db->get('options_ticketstatus');
		
    $this->layout->buildPage('entities/tickets/add', $data);
	}
	
	function tickets_insert(){
		
		$this->db->trans_start();
		$this->db->insert('entities_tickets', $_POST);
		$this->db->trans_complete();
		
		$this->db->select("entities.name");
		$this->db->from('entities');
		$this->db->where('entities.eid', $_POST['eid']);
		$result = $this->db->get();
		$entity = $result->first_row();
		
		//-------compile message content
		$content = $_POST['title']." - ".$_POST['description']."<br>";
		$content .= "- Title: ".$_POST['title']."<br>";
		$content .= "- Description: ".$_POST['description']."<br>";	
		$content .= "- Type: ".$_POST['type']."<br>";	
		$content .= "- Due Date: ".$_POST['duedate']."<br>";	
		$content .= "- Reminder Date: ".$_POST['reminderdate']."<br>";	
		
		//-------------send email notice to ticket owner
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.uid', $_POST['ownedby']);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Ticket Created - '.$entity->name);
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following ticket was created and you are the owner: <br><br>";
		$footer = "<br>OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;
		
		//-----------------send email notice to assigned person
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.uid', $_POST['assignedto']);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Ticket Created - '.$entity->name);
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following ticket was created and is assigned to you: <br><br>";
		$footer = "OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;
		
		redirect('entities/view/' . $_POST['eid']);
	}
	
	function tickets_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['etid'] = $this->uri->segment(4);
		
		$data['entity'] = $this->database_model->viewEntity($data['eid'] , NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		
		$data['ticket'] = $this->database_model->viewTicket($data['etid']);
		
		$data['percentage'] = 0;
		$total = 0;
		if ($data['ticket']->field1_description != ""){
			$total++;
		}
		if ($data['ticket']->field2_description != ""){
			$total++;
		}
		if ($data['ticket']->field3_description != ""){
			$total++;
		}
		if ($data['ticket']->field4_description != ""){
			$total++;
		}
		if ($data['ticket']->field5_description != ""){
			$total++;
		}
		
		
		$amount = 0;
		if ($data['ticket']->field1 == 1){
			$amount++;
		}
		if ($data['ticket']->field2 == 1){
			$amount++;
		}
		if ($data['ticket']->field3 == 1){
			$amount++;
		}
		if ($data['ticket']->field4 == 1){
			$amount++;
		}
		if ($data['ticket']->field5 == 1){
			$amount++;
		}
		
		if ($total != 0){
			$data['percentage'] = ($amount/$total)*100;
		}
		
		$data['product'] = 	$this->database_model->viewProduct($data['ticket']->epid);
    $data['service'] = 	$this->database_model->viewService($data['ticket']->esid);

		$this->db->select("entities_tickets_notes.*");
		$this->db->from('entities_tickets_notes');
		$this->db->where('entities_tickets_notes.etid', $data['etid']);
		$data['entityticketnotes'] = $this->db->get();
		
    $data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL,NULL,NULL);
		
		$data['entitytickettypes'] = $this->database_model->getTicketTypes($this->session->userdata('organization'));
		$data['entityticketstatus'] = $this->db->get('options_ticketstatus');
		
    $this->layout->buildPage('entities/tickets/edit', $data);
	}

	function tickets_update(){
		
		$sendout = FALSE;
		
		if (isset($_POST['etid'])){
			$etid = $_POST['etid'];
			$eid = $_POST['eid'];
		}else{
			$eid =  $this->uri->segment(3);
			$etid =  $this->uri->segment(4);
			$field =  $this->uri->segment(5);
			$action = $this->uri->segment(6);
		}

		$oldticket = 	$this->database_model->viewTicket($etid);
		
		if (isset($_POST['etid'])){
			$this->db->trans_start();
			$this->db->where('etid', $_POST['etid']);
			$this->db->update('entities_tickets', $_POST);
			$this->db->trans_complete();
			
			//-------compile message content
			$content = $_POST['title']." - ".$_POST['description']."<br>";
			if ($oldticket->title != $_POST['title']){
				$content .= "- Title changed to ".$_POST['title']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->description != $_POST['description']){
				$content .= "- Description changed to ".$_POST['description']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->type != $_POST['type']){
				$content .= "- Type changed to ".$_POST['type']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->reminderdate != $_POST['reminderdate']){
				$content .= "- Reminder date changed to ".$_POST['reminderdate']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->duedate != $_POST['duedate']){
				$content .= "- Due date changed to ".$_POST['reminderdate']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->assignedto != $_POST['assignedto']){
				$content .= "- Assigned user changed to ".$_POST['assignedto']."<br>";	
				$sendout = TRUE;
			}
			if ($oldticket->status != $_POST['status']){
				$content .= "- Status changed to ".$_POST['status']."<br>";	
				$sendout = TRUE;
			}
			
			//---------send email notice to ticket owner
			$this->db->from('organizations_users');
			$this->db->where('organizations_users.uid', $_POST['ownedby']);		
			$users = $this->db->get();
			$user = $users->first_row();
			
			$this->email->from('admin@opendesk.co.za', 'OpenDesk');
			$this->email->to($user->email);
			$this->email->subject('OpenDesk Ticket Updated - '.$oldticket->name);
			$header = "Dear ".$user->fullname.",<br><br>";
			$message = "The following ticket was updated and you are the owner: <br><br>";
			$footer = "<br>OpenDesk";
			
			if ($sendout == TRUE){
				$this->email->message($header.$message.$content.$footer);
				$this->email->send();
				//echo $header.$message.$content.$footer;
			}
			
			//-----------------send email notice to assigned person
			$this->db->from('organizations_users');
			$this->db->where('organizations_users.uid', $_POST['assignedto']);		
			$users = $this->db->get();
			$user = $users->first_row();
			
			$this->email->from('admin@opendesk.co.za', 'OpenDesk');
			$this->email->to($user->email);
			$this->email->subject('OpenDesk Ticket Updated - '.$oldticket->name);
			$header = "Dear ".$user->fullname.",<br><br>";
			$message = "The following ticket was updated and is assigned to you: <br><br>";
			$footer = "<br>OpenDesk";
			
			if ($sendout == TRUE){
				$this->email->message($header.$message.$content.$footer);
				$this->email->send();
				//echo $header.$message.$content.$footer;
			}
			
			redirect('entities/view/'.$eid.'#tab_3');
		}else{
			switch($field){
				case "1": $ticket = array("field1" => $action);break;
				case "2": $ticket = array("field2" => $action);break;
				case "3": $ticket = array("field3" => $action);break;
				case "4": $ticket = array("field4" => $action);break;
				case "5": $ticket = array("field5" => $action);break;
				default: break;
			}
			$this->db->trans_start();
			$this->db->where('etid', $etid);
			$this->db->update('entities_tickets', $ticket);
			$this->db->trans_complete();
			
			redirect('entities/tickets_edit/'.$eid.'/'.$etid.'#tab_3');
		}

	}
	
	function tickets_delete(){
		$this->db->trans_start();
		$this->db->delete('entities_tickets', array('etid' => $this->uri->segment(4)));
		$this->db->trans_complete();

		redirect('entities/view/'.$this->uri->segment(3));
	}
	
	function tickets_notes_insert(){
		$sendout = false;
		$oldticket = 		$data['ticket'] = $this->database_model->viewTicket($_POST['etid']);
		
		$this->db->trans_start();
		$this->db->insert('entities_tickets_notes', $_POST);
		$this->db->trans_complete();
		
		//-------compile message content
		$content = $oldticket->title." - ".$oldticket->description."<br>";
		$content .= " - ".$_POST['description']."<br>";	
		
		//-------------send email notice to ticket owner
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.uid', $oldticket->ownedby);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Ticket Updated - '.$oldticket->name);
		
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following ticket was updated and is assigned to you: <br><br>";
		$footer = "<br>OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;
		
		//-----------------send email notice to assigned person
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.uid', $oldticket->assignedto);		
		$users = $this->db->get();
		$user = $users->first_row();
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk Ticket Updated - '.$oldticket->name);
		
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following ticket was updated and you are the owner: <br><br>";
		$footer = "<br>OpenDesk";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		//echo $header.$message.$content.$footer;

		redirect('entities/tickets_edit/'.$_POST['eid'].'/'.$_POST['etid']);
	}
	
	function products_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

    $data['productcategories'] = $this->database_model->getProductCategories($this->session->userdata('organization'));
    $data['producttypes'] = $this->database_model->getProductTypes($this->session->userdata('organization'));
    $data['productproviders'] = $this->database_model->getProductProviders($this->session->userdata('organization'));
    
    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		
    $this->layout->buildPage('entities/products/add', $data);
	}

	function products_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_products', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_1_3');
	}

	function products_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['epid'] = $this->uri->segment(4);

    $data['productcategories'] = $this->database_model->getProductCategories($this->session->userdata('organization'));
    $data['producttypes'] = $this->database_model->getProductTypes($this->session->userdata('organization'));
    $data['productproviders'] = $this->database_model->getProductProviders($this->session->userdata('organization'));

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		
		$data['product'] = $this->database_model->viewProduct($data['epid']);

    $this->layout->buildPage('entities/products/edit', $data);
	}

	function products_update(){
		$this->db->trans_start();
		$this->db->where('epid', $_POST['epid']);
		$this->db->update('entities_products', $_POST);
		$activity = array(
		   'eid' => $_POST['eid'] ,
		   'type' => 3 ,
		   'description' => 'Product Updated',
		   'createdby' => $this->session->userdata('uid'),
		   'updated' => date('Y-m-d h:m:s')

		);
		$this->db->insert('entities_activities', $activity);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid']);
	}

	function products_delete(){
		$this->db->trans_start();
		$this->db->delete('entities_products', array('epid' => $this->uri->segment(4)));
		$this->db->trans_complete();

		redirect('entities/view/'.$this->uri->segment(3));
	}

	function services_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

		$data['servicetypes'] = $this->database_model->getServiceTypes($this->session->userdata('organization'));

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;

    $this->layout->buildPage('entities/services/add', $data);
	}

	function services_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_services', $_POST);		
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid']);
	}

	function services_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['esid'] = $this->uri->segment(4);

    $data['servicetypes'] = $this->database_model->getServiceTypes($this->session->userdata('organization'));

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;
		
		$data['service'] = $this->database_model->viewService($data['esid']);

    $this->layout->buildPage('entities/services/edit', $data);
	}

	function services_update(){
		$this->db->trans_start();
		$this->db->where('esid', $_POST['esid']);
		$this->db->update('entities_services', $_POST);
		$activity = array(
		   'eid' => $_POST['eid'] ,
		   'type' => 3 ,
		   'description' => 'Service Updated',
		   'createdby' => $this->session->userdata('uid'),
		   'updated' => date('Y-m-d h:m:s')

		);
		$this->db->insert('entities_activities', $activity);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid']);
	}

	function services_delete(){
		$this->db->trans_start();
		$this->db->delete('entities_services', array('esid' => $this->uri->segment(4)));
		$this->db->trans_complete();

		redirect('entities/view/'.$this->uri->segment(3));
	}
	
	function medicals_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

		$this->db->select("options_entitymedicaldiets.*");
		$this->db->from('options_entitymedicaldiets');
		$data['medicaldiets'] = $this->db->get();

		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$data['entity'] = $entities->first_row();
		$data['title'] = $data['entity']->name;

        $this->layout->buildPage('entities/medicals/add', $data);
	}
	
	function medicals_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_medicals', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_1_5');
	}
	
	function medicals_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['emid'] = $this->uri->segment(4);

		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$data['entity'] = $entities->first_row();
		$data['title'] = $data['entity']->name;

		$this->db->select("entities_medicals.*");
		//$this->db->select("options_entityproducttypes.*");
		//$this->db->select("options_entityproductcategories.description as productcategory");
		$this->db->from("entities_medicals");
		//$this->db->join('options_entityproductcategories', 'options_entityproductcategories.oepcid = entities_products.category');
		//$this->db->join('options_entityproducttypes', 'options_entityproducttypes.oeptid = entities_products.type');
		$this->db->where('entities_medicals.emid', $data['emid']);
		$query = $this->db->get();
		$data['medical'] = $query->first_row();

        $this->layout->buildPage('entities/medicals/edit', $data);
	}
	
	function financials_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;

    $this->layout->buildPage('entities/financials/add', $data);
	}
	
	function financials_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_financials', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_1_6');
	}
	
	function financials_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['efid'] = $this->uri->segment(4);

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;

		$this->db->select("entities_financials.*");
		$this->db->from("entities_financials");
		$this->db->where('entities_financials.efid', $data['efid']);
		$query = $this->db->get();
		$data['financial'] = $this->database_model->viewFinancial($data['efid']);

    $this->layout->buildPage('entities/financials/edit', $data);
	}
	
	function financials_update(){
		$this->db->trans_start();
		$this->db->where('efid', $_POST['efid']);
		$this->db->update('entities_financials', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_1_6');
	}
    	
	function files_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;

    $this->layout->buildPage('entities/files/add', $data);
	}
  
	function files_insert(){
		$this->db->trans_start();
		$this->db->insert('entities_files', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab_5_2');
	}
	
  function files_edit(){		
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);
		$data['efid'] = $this->uri->segment(4);

    $data['entity'] = $this->database_model->viewEntity($data['eid'],NULL ,NULL, NULL, NULL, NULL);
		$data['title'] = $data['entity']->name;

		$this->db->select("entities_files.*");
		$this->db->from("entities_files");
		$this->db->where('entities_files.efid', $data['efid']);
		$query = $this->db->get();
		$data['file'] = $this->database_model->viewFile($data['efid']);

    $this->layout->buildPage('entities/files/edit', $data);
  }
  
	function files_update(){
		$this->db->trans_start();
		$this->db->where('efid', $_POST['efid']);
		$this->db->update('entities_files', $_POST);
		$this->db->trans_complete();

		redirect('entities/view/' . $_POST['eid'].'#tab5_2');
	}
	
	function sms_balance(){
    $username = $this->session->userdata('sms_username');
		$password = $this->session->userdata('sms_password');
		$api_id = $this->session->userdata('sms_api');			
		$this->curl->open();
		$data['sms'] = $this->curl->http_get("https://api.clickatell.com/http/getbalance?user=".$username ."&password=".$password."&api_id=".$api_id);
		$this->curl->close();
	}

	function sms_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['eid'] = $this->uri->segment(3);

		$this->db->select("entities.*");
		$this->db->from('entities');
		$this->db->where('entities.eid', $data['eid']);
		$entities = $this->db->get();
		$data['entity'] = $entities->first_row();
		$data['title'] = $data['entity']->name;

		$data['contacts'] = $this->database_model->getContacts($data['eid'], NULL, NULL, NULL);

    $username = $this->session->userdata('sms_username');
		$password = $this->session->userdata('sms_password');
		$api_id = $this->session->userdata('sms_api');			
		$this->curl->open();
		$data['sms'] = $this->curl->http_get("https://api.clickatell.com/http/getbalance?user=".$username ."&password=".$password."&api_id=".$api_id);
		$this->curl->close();

    $this->layout->buildPage('entities/sms/add', $data);
	}

	function sms_send(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "SMS Result";
		$data['subtitle'] = "";

    $user = 'LeRoux';
    $password = 'BcEdFIcXCHPaRF';
    $api_id = '3568738';

		$to = $_POST['mobile'];
		$text = $_POST['message'];			
		$this->curl->open();
		$newtext = preg_replace("/ /", "+", $text);
		$content = $this->curl->http_get("http://api.clickatell.com/http/sendmsg?user=$user&password=$password&api_id=$api_id&to=$to&text=$newtext");
		$this->curl->close();

		$data['result'] = $content;

        $this->layout->buildPage('entities/sms/result', $data);
	}
  
	function get_person(){
		$pid = $this->uri->segment(3);
		$this->db->select("persons.*");
		$this->db->from('persons');
		$this->db->where('persons.pid', $pid);
		$result = $this->db->get();
		$row = $result->first_row();
		//echo $row->lastname;
		echo json_encode($row);
	}
	
}
?>