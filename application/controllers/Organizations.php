<?php

class Organizations extends CI_Controller {

  function manage(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', '');		
		
		if ($this->uri->segment(3) != NULL){
			$data['orgid'] = $this->uri->segment(3);
		}else{
			$data['orgid'] = $this->session->userdata('organization');
		}
    
    $user = $this->database_model->viewUser($this->session->userdata('uid'));
		
		if ($this->session->userdata('accesslevel') <= 2){
     
      $organization = $this->database_model->viewOrganization($data['orgid'],NULL,NULL,NULL,NULL);
		  $data['title'] = $organization->name;
      
      $data['organization'] = $organization;
      $data['users'] = $this->database_model->getUsers($data['orgid'],NULL,NULL,NULL);
			$data['userscount'] = $data['users']->num_rows();
	
			$data['groups'] = $this->database_model->getOrganizationGroups($data['orgid'],NULL,NULL);
			$data['groupscount'] = $data['groups']->num_rows();
			
			$this->db->select("organizations_settings.*");
			$this->db->from('organizations_settings');
			$this->db->where('organizations_settings.organization', $data['orgid']);
			$query = $this->db->get();
			$data['setting'] = $query->first_row();
	
      $data['all_productcategories'] = $this->database_model->getProductCategories(NULL);
      $result = $this->database_model->getProductCategories($data['orgid']);
      $data['organizations_productcategories'] = $result->result_array();
        
      $data['all_producttypes'] = $this->database_model->getProductTypes(NULL);
      $result = $this->database_model->getProductTypes($data['orgid']);
      $data['organizations_producttypes'] = $result->result_array();
      
      $data['all_productproviders'] = $this->database_model->getProductProviders(NULL);
      $result = $this->database_model->getProductProviders($data['orgid']);
			$data['organizations_productproviders'] = $result->result_array();
      
      $data['all_tickettypes'] = $this->database_model->getTicketTypes(NULL);
      $result = $this->database_model->getTicketTypes($data['orgid']);
      $data['organizations_tickettypes'] = $result->result_array();
      
      $data['all_servicetypes'] = $this->database_model->getServiceTypes(NULL);
      $result = $this->database_model->getServiceTypes($data['orgid']);
      $data['organizations_servicetypes'] = $result->result_array();
  		
      $data['all_entitytypes'] = $this->database_model->getEntityTypes(NULL);
      $result = $this->database_model->getEntityTypes($data['orgid']);
      $data['organizations_entitytypes'] = $result->result_array();
			
			$this->layout->buildPage('organizations/manage', $data);
    }else{
      $data['title'] = "Access Denied";
      $data['subtitle'] = "";
      $data['message'] = "You do not have access to manage this organization. Please contact admin@opendesk.co.za";
      $this->layout->buildPage('organizations/error', $data);
    }
	}	
	
	function add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['title'] = "New Organization";
		
		$this->db->where('users.organization', $this->session->userdata('profile'));
		$data['users'] = $this->db->get('users');
		
    $data['organizationtypes'] = $this->db->get('options_organizationtypes');
		$data['organizationstatus'] = $this->db->get('options_organizationstatus');
		
		$this->db->select("persons.*");
		$this->db->from('persons');
		$this->db->where('persons.organization', $this->session->userdata('profile'));
		$data['persons'] = $this->db->get();
		
    $this->layout->buildPage('organizations/add', $data);	
	}

	function insert(){		
    
    $this->db->select("organizations.name");
		$this->db->from('organizations');
		$this->db->like('organizations.name', $_POST['name']); 
		$result = $this->db->get();
		
		if ($result->num_rows() > 0){
			$this->session->set_flashdata('message', 'The organization could not be created since a matching organizations already exists. Please try again.');
			redirect('organizations/add/');
		}else{
			$this->db->trans_start();
			
			//insert entity data
			$organization = array(
			   'mode' => "1",
			   'name' => $_POST['name'] ,
			   'registration' => $_POST['registration'] ,
			   'status' => 1,
			   'updated' => date("Y-m-d")
			);
			$this->db->insert('organizations', $organization);
			$orgid = $this->db->insert_id();
				
      //insert organizations settings
      $settings = array(
         'organization' => $orgid ,
         'email' => 0,
         'sms' => 0,
         'seafile' => 0,
         'correspondence' => 0,
         'support' => 0,
         'governance' => 0,
         'fiduciary' => 0,
         'files' => 0
      );
      $this->db->insert('organizations_settings', $settings);
      $esid = $this->db->insert_id();
      
      //insert organizations options
      $options = array(
         'organization' => $orgid ,
         'productcategories' => NULL,
         'productproviders' => NULL,
         'producttypes' => NULL,
         'servicetypes' => 0,
         'tickettypes' => 0,
         'entitytypes' => "1,2,3,4,5,6,7"
      );
      $this->db->insert('organizations_options', $options);
      $eoid = $this->db->insert_id();
      
      $this->db->query("UPDATE users SET organizations = CONCAT(organizations,',',".$orgid.") WHERE uid = ".$this->session->userdata('uid'));
			
			$this->db->trans_complete();
			redirect('organizations/manage/' . $orgid);
		}
	}
	
	function edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$data['organization'] = $this->uri->segment(3);
		
		$this->db->select("organizations.*");
		$this->db->from('organizations');
		$this->db->where('organizations.orgid', $data['organization']);
		$entities = $this->db->get();
		$data['organization'] = $entities->first_row();
		$data['title'] = $data['organization']->name;
		$this->db->where('organizations_users.organization', $this->session->userdata('organization'));
		$data['users'] = $this->db->get('organizations_users');
		
		$this->db->select("organizations_options.entitytypes");
		$this->db->from('organizations_options');
		$this->db->where('organizations_options.organization', $this->session->userdata('organization'));
		$query = $this->db->get();
		$profile_optionsentitytypes = $query->first_row();
		$this->db->select("options_entitytypes.*");
		$this->db->from('options_entitytypes');
		$this->db->where_in('oetid', explode(',',$profile_optionsentitytypes->entitytypes));
		$data['entitytypes'] = $this->db->get();
		
		$data['entitystatus'] = $this->db->get('options_entitystatus');
		
		$this->db->select("organizations_groups.*");
		$this->db->from('organizations_groups');
		$this->db->where('organizations_groups.organization', $this->session->userdata('organization'));
		$data['groups'] = $this->db->get();
		
    $this->layout->buildPage('organizations/edit', $data);
	}
	
	function update(){	
		$this->db->trans_start();
		
		//get user details
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->where('users.uid', $this->session->userdata('uid'));
		$result = $this->db->get();
		$user = $result->first_row();
		
		//get old entity details
		$this->db->select("organizations.name");
		$this->db->from('organizations');
		$this->db->where('organizations.orgid', $_POST['orgid']);
		$result = $this->db->get();
		$data['organization'] = $result->first_row();
		
    /*
		if ($user->seafile == 1){
			$params = array('user' => $user->seafile_username, 'password' => $user->seafile_password, 'token' => $user->seafile_token);
			$this->load->library('seafile', $params);
			switch ($_POST['status']){
				case 1 : $this->seafile->renameDirectory($user->seafile_leadlibrary,$data['entity']->name, $_POST['name']);break;
				case 2 : $this->seafile->renameDirectory($user->seafile_prospectlibrary,$data['entity']->name, $_POST['name']);break;
				case 3 : $this->seafile->renameDirectory($user->seafile_clientlibrary,$data['entity']->name, $_POST['name']);break;
			}
		}
    */
		
		$this->db->where('orgid', $_POST['orgid']);
		$this->db->update('organizations', $_POST);
		
		$this->db->trans_complete();
		
		redirect('organizations/view/' . $_POST['orgid']);
	}
	
	function delete(){	
		$this->db->trans_start();
		$this->db->delete('entities', array('eid' => $this->uri->segment(3)));
		$this->db->delete('entities_contacts', array('eid' => $this->uri->segment(3)));
		$this->db->trans_complete();

		redirect('entities/'.$this->uri->segment(4));
	}	
	
	function sms_balance(){
    $username = $this->session->userdata('sms_username');
		$password = $this->session->userdata('sms_password');
		$api_id = $this->session->userdata('sms_api');			
		$this->curl->open();
		$data['sms'] = $this->curl->http_get("https://api.clickatell.com/http/getbalance?user=".$username ."&password=".$password."&api_id=".$api_id);
		$this->curl->close();
	}
	
  function users_view(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', '');

		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
    
    $data['user'] = $this->database_model->viewUser($data['uid']);
    $data['orgid'] = $data['user']->organization;

    $data['title'] = $data['user']->fullname;
    $data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);

		$data['accesslevels'] = $this->db->get('options_useraccesslevels');
		$data['entities'] =  $this->database_model->getEntities($data['orgid'],NULL, NULL, NULL, $data['uid'] , NULL);
		$data['tasks'] = $this->database_model->getUserTasks($data['orgid'],$data['uid'] , NULL);
		$data['tickets'] = $this->database_model->getTickets(NULL,NULL, $data['uid'], NULL, NULL, NULL, NULL);
    $data['folders'] = $this->database_model->getFiles($data['orgid'], NULL);
    
    /* 
    //get imap emails
		if ($data['user']->imap == 1){
			$data['folder'] = 'INBOX';
			$imap_username = $data['user']->imap_email;
			$imap_password = $data['user']->imap_password;
			$imap_server = $data['user']->imap_server;
			$imap_folder = $data['user']->imap_folder;
			$imap_port = $data['user']->imap_port;
			$imap_flags = $data['user']->imap_flags;
			
			//$dsn = sprintf('{%s}%s', IMAP_SERVER, "INBOX");		
			$dsn = "{".$imap_server.":".$imap_port."/".$imap_flags."}".$imap_folder;
			$imap = $this->mail->openMailbox($imap_username,$imap_password,$dsn);
			//$imap = $this->mail->openMailbox("admin@opendesk.co.za","Admin@opend3sk",$dsn);
			$data['mailboxinfo'] = $this->mail->getMailBoxInfo($imap);
			$data['messages'] = $this->mail->getAllMessages($imap,$dsn);
			imap_close($imap);
		}
    */
    
    $this->layout->buildPage('organizations/users/view', $data);
  }
  
	function users_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
    
		if ($this->uri->segment(3) != NULL){
			$data['orgid'] = $this->uri->segment(3);
		}else{
			$data['orgid'] = $this->session->userdata('organization');
		}
    
    $organization = $this->database_model->viewOrganization($data['orgid'],NULL,NULL,NULL,NULL);
    $data['title'] = $organization->name;
		
		$this->db->from('options_useraccesslevels');
		$data['accesslevels'] = $this->db->get();
		
    
    $this->layout->buildPage('organizations/users/add', $data);
	}
	
	function users_insert(){	
		
		$this->load->helper('string');
		
		$this->db->select("organizations_users.username");
		$this->db->from('organizations_users');
		$this->db->where('organizations_users.username', $_POST['username']); 
		$result = $this->db->get();
    
    
		if ($result->num_rows() > 0){
			$this->session->set_flashdata('message', 'The user could not be created since a matching user already exists. Please try again.');
			redirect('organizations/users_add');
		}else{
    
      if (isset($_POST['password'])){
        $password = $_POST['password'];
        $encpassword = md5($password);
        $_POST['password'] = $encpassword;
      }else{
        $password = random_string('alnum',8);
        $encpassword = md5($password);
        $_POST['password'] = $encpassword;
      }

      $this->db->trans_start();
      $this->db->insert('organizations_users', $_POST);
      $uid = $this->db->insert_id();
      $this->db->trans_complete();

      $user = $this->database_model->viewUser($uid);

      //send welcome email to the user
      $header 	= "Dear ".$user->fullname.",<br><br>";
      $message 	= "Congratulations, you have been granted access to the OpenDesk system.<br>";
      $message 	.= "The following information is currently associated with your user account:<br><br>";
      $message 	.= "Full Name: ".$user->fullname."<br>";
      $message 	.= "Email Address: ".$user->email."<br>";
      $message 	.= "Organization: ".$user->organizationname."<br><br>";
      $message 	.= "In order to log in to the system, please make use of the following credentials:<br><br>";
      $message 	.= "Secure URL: https://www.opendesk.co.za/<br>";
      $message 	.= "Username: ".$user->username."<br>";
      $message 	.= "Password: ".$password."<br><br>";
      $message 	.= "Please note that the above information is confidential and it remains your responsibility to keep the content of this email private and secure.<br><br>";
      $footer 	= "OpenDesk<br><br>";
      $this->email->from('admin@opendesk.co.za', 'OpenDesk');
      $this->email->to($user->email);
      $this->email->subject('OpenDesk - User Account Created');
      $this->email->message($header.$message.$footer);
      $this->email->send();
      //echo $header.$message.$footer;

      redirect('organizations/manage/'.$_POST['organization'].'#tab_1_2');
    }
	}
	
	function users_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'settings');
		
		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
    $data['user'] = $this->database_model->viewUser($data['uid']);    
    $organization = $this->database_model->viewOrganization($data['user']->organization,NULL,NULL,NULL,NULL);
    $data['title'] = $organization->name;
		
		$data['accesslevels'] = $this->db->get('options_useraccesslevels');
		
    $this->layout->buildPage('organizations/users/edit', $data);
	}
	
	function users_update(){
		
		$this->load->helper('string');
    
		
      if (isset($_POST['password'])){
        $password = $_POST['password'];
        $encpassword = md5($password);
        $_POST['password'] = $encpassword;
        $uid = $_POST['uid'];

        $this->db->trans_start();
        $this->db->where('uid', $uid);
        $this->db->update('organizations_users', $_POST);
        $this->db->trans_complete();
        
        $this->db->select("organizations_users.*");
        $this->db->select("organizations.name as organizationname, organizations.orgid");
        $this->db->from('organizations_users');
        $this->db->join('organizations', 'organizations.orgid = organizations_users.organization', 'left outer');
        $this->db->where('organizations_users.uid', $uid);
        $result = $this->db->get();
        $user = $result->first_row();

        $header 	= "Dear ".$user->fullname.",<br><br>";
        $message 	= "Thank you, we have received and processed a request to change the password on your user profile.<br>";
        $message 	.= "Please take note of the following and inform us if this was not requested by you.<br><br>";
        $message 	.= "Full Name: ".$user->fullname."<br>";
        $message 	.= "Email Address: ".$user->email."<br>";
        $message 	.= "Organization: ".$user->organizationname."<br><br>";
        $message 	.= "In order to log in to the system, please make use of the following credentials:<br><br>";
        $message 	.= "Secure URL: https://opendesk.co.za/<br>";
        $message 	.= "Username: ".$user->username."<br>";
        $message 	.= "Password: ".$_POST['password']."<br><br>";
        $footer 	= "The OpenDesk Team<br><br>";
        $footer 	.= "Please note that the above information is confidential and it remains your responsibility to keep the content of this email private and secure.";

        $this->email->from('admin@opendesk.co.za', 'OpenDesk');
        $this->email->to($user->email);
        $this->email->subject('OpenDesk - User Password Changed');
        $this->email->message($header.$message.$footer);
        $this->email->send();
        
      }elseif(isset($_POST['uid'])){
        $this->db->select("organizations_users.username");
        $this->db->from('organizations_users');
        $this->db->where('organizations_users.username', $_POST['username']); 
        $this->db->where('organizations_users.uid !=', $_POST['uid']); 
        $result = $this->db->get();

        if ($result->num_rows() > 0){
          $this->session->set_flashdata('message', 'The user could not be updated since a matching username already exists. Please try again.');
          redirect('organizations/users_edit/'.$_POST['uid']);
        }else{
          $uid = $_POST['uid'];
          $this->db->trans_start();
          $this->db->where('uid', $uid);
          $this->db->update('organizations_users', $_POST);
          $this->db->trans_complete();

          $this->db->select("organizations_users.*");
          $this->db->select("organizations.name as organizationname, organizations.orgid");
          $this->db->from('organizations_users');
          $this->db->join('organizations', 'organizations.orgid = organizations_users.organization', 'left outer');
          $this->db->where('organizations_users.uid', $uid);
          $result = $this->db->get();
          $user = $result->first_row();

          $header 	= "Dear ".$user->fullname.",<br><br>";
          $message 	= "Thank you, we have received and processed a request to change the details on your user profile.<br>";
          $message 	.= "Please take note of the following and inform us if this was not requested by you.<br><br>";
          $message 	.= "Full Name: ".$user->fullname."<br>";
          $message 	.= "Email Address: ".$user->email."<br>";
          $message 	.= "Organization: ".$user->organizationname."<br><br>";
          $message 	.= "In order to log in to the system, please make use of the following credentials:<br><br>";
          $message 	.= "Secure URL: https://opendesk.co.za/<br>";
          $message 	.= "Username: ".$user->username."<br><br>";
          $footer 	= "The OpenDesk Team<br><br>";
          $footer 	.= "Please note that the above information is confidential and it remains your responsibility to keep the content of this email private and secure.";

          $this->email->from('admin@opendesk.co.za', 'OpenDesk');
          $this->email->to($user->email);
          $this->email->subject('OpenDesk - User Account Updated');
          $this->email->message($header.$message.$footer);
          $this->email->send();
        }        
      }else{
        $password = random_string('alnum',8);
        $encpassword = md5($password);
        $_POST['password'] = $encpassword;
        $uid =  $this->uri->segment(3);

        $this->db->trans_start();
        $this->db->where('uid', $uid);
        $this->db->update('organizations_users', $_POST);
        $this->db->trans_complete();
        
        $this->db->select("organizations_users.*");
        $this->db->select("organizations.name as organizationname, organizations.orgid");
        $this->db->from('organizations_users');
        $this->db->join('organizations', 'organizations.orgid = organizations_users.organization', 'left outer');
        $this->db->where('organizations_users.uid', $uid);
        $result = $this->db->get();
        $user = $result->first_row();

        $header 	= "Dear ".$user->fullname.",<br><br>";
        $message 	= "Thank you, we have received and processed a request to reset the password on your user profile.<br>";
        $message 	.= "Please take note of the following and inform us if this was not requested by you.<br><br>";
        $message 	.= "Full Name: ".$user->fullname."<br>";
        $message 	.= "Email Address: ".$user->email."<br>";
        $message 	.= "Organization: ".$user->organizationname."<br><br>";
        $message 	.= "In order to log in to the system, please make use of the following credentials:<br><br>";
        $message 	.= "Secure URL: https://opendesk.co.za/<br>";
        $message 	.= "Username: ".$user->username."<br>";
        $message 	.= "Password: ".$_POST['password']."<br><br>";
        $footer 	= "The OpenDesk Team<br><br>";
        $footer 	.= "Please note that the above information is confidential and it remains your responsibility to keep the content of this email private and secure.";

        $this->email->from('admin@opendesk.co.za', 'OpenDesk');
        $this->email->to($user->email);
        $this->email->subject('OpenDesk - User Password Reset');
        $this->email->message($header.$message.$footer);
        $this->email->send();
      }
    redirect('organizations/manage/'.$_POST['organization'].'#tab_1_2');
	}
	
	function users_delete(){
		$this->db->trans_start();
		$this->db->delete('users', array('uid' => $this->uri->segment(3)));
		$this->db->trans_complete();

		redirect('organizations/manage/'.$_POST['organization'].'#tab_1_2');
	}
	
	function users_tasks_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		
		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
		$data['user'] = $this->database_model->viewUser($data['uid']);
		$data['title'] = $data['user']->fullname;
		
    $data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL,NULL,NULL);
		
    $this->layout->buildPage('organizations/users/tasks/add', $data);
	}
	
	function users_tasks_insert(){	
		$this->db->trans_start();
		
		$this->db->insert('organizations_users_tasks', $_POST);
		
		//-------compile message content
		$content = $_POST['title']."<br>";
		$content .= "- Title: ".$_POST['title']."<br>";
		$content .= "- Description: ".$_POST['description']."<br>";	
		$content .= "- Due Date: ".$_POST['duedate']."<br>";	
		$content .= "- Reminder Date: ".$_POST['reminderdate']."<br>";	
		
		//-------------send email notice to task owner
		$user = $this->database_model->viewUser($_POST['uid']);
    
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk - User Task Created');
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following task was created and you are the owner: <br><br>";
		$footer = "<br><br>The OpenDesk Team";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		
		$this->db->trans_complete();
		
		redirect('organizations/users_view/' . $_POST['uid'].'#tab_1_3');
	}

	function users_tasks_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'settings');
		$data['utid'] = $this->uri->segment(3);		
    
		$data['task'] = $this->database_model->viewUserTask($data['utid']);
		$data['title'] = $data['task']->fullname;
		$data['users'] = $this->database_model->getUsers($this->session->userdata('organization'),NULL, NULL, NULL, NULL);
		
		$data['taskstatus'] = $this->db->get('options_usertaskstatus');
		
    $this->layout->buildPage('organizations/users/tasks/edit', $data);
	}
	
	function users_tasks_update(){		
		
		$this->db->trans_start();
		
		$this->db->where('utid', $_POST['utid']);
		$task = array(
		   'utid' => $_POST['utid'],
		   'uid' => $_POST['uid'],
		   'title' => $_POST['title'],
		   'description' => $_POST['description'],
		   'reminderdate' => $_POST['reminderdate'],
		   'duedate' => $_POST['duedate'],
		   'progress' => $_POST['progress'],
		   'status' => $_POST['status']
		);
		$this->db->update('organizations_users_tasks', $task);
		
		
		//-------compile message content
		$content = $_POST['title']."<br>";
		$content .= "- Title: ".$_POST['title']."<br>";
		$content .= "- Description: ".$_POST['description']."<br>";	
		$content .= "- Due Date: ".$_POST['duedate']."<br>";	
		$content .= "- Reminder Date: ".$_POST['reminderdate']."<br>";	
		$content .= "- Progress: ".$_POST['progress']."%<br>";	
		$content .= "- Status: ".$_POST['status']."<br>";	
		
		//-------------send email notice to task owner
		$user = $this->database_model->viewUser($_POST['uid']);
		
		$this->email->from('admin@opendesk.co.za', 'OpenDesk');
		$this->email->to($user->email);
		$this->email->subject('OpenDesk - User Task Updated');
		$header = "Dear ".$user->fullname.",<br><br>";
		$message = "The following task was updated and you are the owner: <br><br>";
		$footer = "<br><br>The OpenDesk Team";
		
		$this->email->message($header.$message.$content.$footer);
		$this->email->send();
		
		$this->db->trans_complete();

		redirect('organizations/users_view/' . $_POST['user_id'].'#tab_1_3');
	}
  
	function users_settings_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['title'] = "Edit Settings";
		$data['subtitle'] = "";

		if ($this->uri->segment(3) != ""){
			$data['uid'] = $this->uri->segment(3);
		}else{
			$data['uid'] = $this->session->userdata('uid');
		}
		
    $data['user'] = $this->database_model->viewUser($data['uid']);
		$data['title'] = $data['user']->fullname;
		
		$this->layout->buildPage('organizations/users/settings/edit', $data);
	}
	
	function users_settings_update(){		
		$this->db->trans_start();
		$this->db->where('uid', $_POST['uid']);
		$this->db->update('organizations_users', $_POST);
		$this->db->trans_complete();

		redirect('organizations/users_view/' . $_POST['user_id'].'#tab_2');
	}
  
	function groups_add(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
    
		if ($this->uri->segment(3) != NULL){
			$data['orgid'] = $this->uri->segment(3);
		}else{
			$data['orgid'] = $this->session->userdata('organization');
		}
    
    $organization = $this->database_model->viewOrganization($data['orgid'],NULL,NULL,NULL,NULL);
    $data['title'] = $organization->name;
    
    $this->layout->buildPage('organizations/groups/add', $data);
	}
	
	function groups_insert(){	
	
		$this->db->trans_start();
		$this->db->insert('organizations_groups', $_POST);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$_POST['organization'].'#tab_1_3');
	}
	
	function groups_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
	
		$this->session->set_userdata('menuitem', 'settings');
		$data['egid'] = $this->uri->segment(3);
    		
		$this->db->select("organizations_groups.*");
		$this->db->from('organizations_groups');
		$this->db->where('organizations_groups.egid', $data['egid']);
		$group = $this->db->get();
		$data['group'] = $group->first_row();
    
    $organization = $this->database_model->viewOrganization($data['group']->organization,NULL,NULL,NULL,NULL);
    $data['title'] = $organization->name;

    $this->layout->buildPage('organizations/groups/edit', $data);
	}
	
	function groups_update(){
	
		$data['egid'] = $this->uri->segment(3);
		
		$this->db->trans_start();
		$this->db->where('organizations_groups.egid', $_POST['egid']);
		$this->db->update('organizations_groups', $_POST);
		$this->db->trans_complete();

		redirect('organizations/manage/'.$_POST['organization'].'#tab_1_3');
	}
	
	function groups_unsubscribe(){
	
		$data['egid'] = $this->uri->segment(3);
		$data['eid'] = $this->uri->segment(4);
		
		$this->db->trans_start();
		$this->db->where('organizations_groups.eid', $_POST['eid']);
		$this->db->update('organizations_groups', $group);
		$this->db->trans_complete();

		redirect('organizations/manage#tab_1_3');
	}
	
	function settings_edit(){
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "";
		$this->session->set_userdata('menuitem', 'settings');
		$data['section'] = $this->uri->segment(3);
		$data['orgid'] = $this->uri->segment(4);
		
    $data['user'] = $this->database_model->viewUser($this->session->userdata('uid'));    
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'], NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;

    $this->layout->buildPage('organizations/settings/edit', $data);
	}
	
	function settings_update(){
		if ($_POST['section'] == 1){
			$this->db->trans_start();
			$data = array(
				'sms_apiid' => $_POST['sms_apiid'],
				'sms_username' => $_POST['sms_username'],
				'sms_password' => $_POST['sms_password'],
			);
			$this->db->where('organization', $_POST['organization']);
			$this->db->update('organizations_settings', $data);
			$this->db->trans_complete();
		}elseif($_POST['section'] == 2){
			$this->db->trans_start();
			$data = array(
				'imap_from' => $_POST['imap_from'],
				'imap_smtp' => $_POST['imap_smtp'],
        'imap_folder' => $_POST['imap_folder'],
        'imap_port' => $_POST['imap_port'],
        'imap_flags' => $_POST['imap_flags']
			);
			$this->db->where('organization', $_POST['organization']);
			$this->db->update('organizations_settings', $data);
			$this->db->trans_complete();
		}elseif($_POST['section'] == 3){
			$this->db->trans_start();
			$data = array(
				'seafile_token' => $_POST['seafile_token'],
        'seafile_library' => $_POST['seafile_library'],
			);
			$this->db->where('organization', $_POST['organization']);
			$this->db->update('organizations_settings', $data);
			$this->db->trans_complete();
		}elseif($_POST['section'] == 4){
			$this->db->trans_start();
			$data = array(
				'correspondence' => $_POST['correspondence'],
				'support' => $_POST['support'],
				'governance' => $_POST['governance'],
				'fiduciary' => $_POST['fiduciary'],
				'files' => $_POST['files'],
			);
			$this->db->where('organization', $_POST['organization']);
			$this->db->update('organizations_settings', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['organization']);
	}
	
	function options_productcategories_edit(){
		$data['orgid'] = $this->uri->segment(3);
		$data['oepcid'] = $this->uri->segment(4);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Categories";
		$data['functionname'] = "options_productcategories";
		$data['functionid'] = "oepcid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_productcategories.*");
		$this->db->from('options_productcategories');
		$this->db->where('options_productcategories.oepcid', $data['oepcid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_productcategories_update(){
		if ($this->uri->segment(3) == "single"){
			$this->db->trans_start();
			$this->db->where('oepcid', $_POST['oepcid']);
			$this->db->update('options_productcategories', $_POST);
			$this->db->trans_complete();
		}elseif ($this->uri->segment(3) == "multiple"){
			$productcategories = implode(",", $_POST['options']);
			$eid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'productcategories' => $productcategories
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_2');
	}
	
	function options_productcategories_add(){
		$data['orgid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Categories";
		$data['functionname'] = "options_productcategories";
		$data['functionid'] = "oepcid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
    
		$this->layout->buildPage('organizations/options/add', $data);
	}
	
	function options_productcategories_insert(){
		$this->db->trans_start();
		$productcategory = array(
			'description' => $_POST['description']
		);
		$this->db->insert('options_productcategories', $productcategory);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_2');
	}
	
	function options_producttypes_edit(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oeptid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Types";
		$data['functionname'] = "options_producttypes";
		$data['functionid'] = "oeptid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_producttypes.*");
		$this->db->from('options_producttypes');
		$this->db->where('options_producttypes.oeptid', $data['oeptid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_producttypes_update(){
		if ($this->uri->segment(3) == "single"){
			$this->db->trans_start();
			$this->db->where('oeptid', $_POST['oeptid']);
			$this->db->update('options_producttypes', $_POST);
			$this->db->trans_complete();
		}elseif ($this->uri->segment(3) == "multiple"){
			$producttypes = implode(",", $_POST['options']);
			$eid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'producttypes' => $producttypes
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_3');
	}
	
	function options_producttypes_add(){
		$data['orgid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Types";
		$data['functionname'] = "options_producttypes";
		$data['functionid'] = "oeptid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->layout->buildPage('organizations/options/add', $data);
	}
	
	function options_producttypes_insert(){
		$this->db->trans_start();
		$this->db->insert('options_producttypes', $_POST);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_3');
	}

	function options_productproviders_edit(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oeppid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Providers";
		$data['functionname'] = "options_productproviders";
		$data['functionid'] = "oeppid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_productproviders.*");
		$this->db->from('options_productproviders');
		$this->db->where('options_productproviders.oeppid', $data['oeppid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_productproviders_update(){
		if ($this->uri->segment(3) == "single"){
			$this->db->trans_start();
			$this->db->where('oeppid', $_POST['oeppid']);
			$this->db->update('options_productproviders', $_POST);
			$this->db->trans_complete();
		}elseif ($this->uri->segment(3) == "multiple"){
			$productproviders = implode(",", $_POST['options']);
			$orgid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'productproviders' => $productproviders
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_4');
	}
	
	function options_productproviders_add(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oeppid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Product Providers";
		$data['functionname'] = "options_productproviders";
		$data['functionid'] = "oeppid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->layout->buildPage('entities/options/add', $data);
	}
	
	function options_productproviders_insert(){
	
		$this->db->trans_start();
		$productprovider = array(
			'description' => $_POST['description']
		);
		$this->db->insert('options_productproviders', $productprovider);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_4');
	}
	
	function options_tickettypes_edit(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oettid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Ticket Types";
		$data['functionname'] = "options_tickettypes";
		$data['functionid'] = "oettid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_tickettypes.*");
		$this->db->from('options_tickettypes');
		$this->db->where('options_tickettypes.oettid', $data['oettid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_tickettypes_update(){
		
		if ($this->uri->segment(3) == "multiple"){
			$tickettypes = implode(",", $_POST['options']);
			$orgid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'tickettypes' => $tickettypes
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}else{
			$this->db->trans_start();
			$this->db->where('oettid', $_POST['oettid']);
			$this->db->update('options_tickettypes', $_POST);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_5');
	}
	
	function options_tickettypes_add(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oettid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Ticket Types";
		$data['functionname'] = "options_tickettypes";
		$data['functionid'] = "oettid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->layout->buildPage('organizations/options/add', $data);
	}
	
	function options_tickettypes_insert(){
	
		$this->db->trans_start();
		$this->db->insert('options_tickettypes', $_POST);
		$this->db->trans_complete();
		
		redirect('organizations/manage/#tab_3_5');
	}
	
	function options_servicetypes_edit(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oestid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Service Types";
		$data['functionname'] = "options_servicetypes";
		$data['functionid'] = "oestid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_servicetypes.*");
		$this->db->from('options_servicetypes');
		$this->db->where('options_servicetypes.oestid', $data['oestid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_servicetypes_update(){
		if ($this->uri->segment(3) == "single"){
			$this->db->trans_start();
			$this->db->where('oestid', $_POST['oestid']);
			$this->db->update('options_servicetypes', $_POST);
			$this->db->trans_complete();
		}elseif ($this->uri->segment(3) == "multiple"){
			$servicetypes = implode(",", $_POST['options']);
			$orgid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'servicetypes' => $servicetypes
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_6');
	}
	
	function options_servicetypes_add(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oestid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Service Types";
		$data['functionname'] = "options_servicetypes";
		$data['functionid'] = "oestid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->layout->buildPage('organizations/options/add', $data);
	}
	
	function options_servicetypes_insert(){
		$this->db->trans_start();
		$this->db->insert('options_servicetypes', $_POST);
		$this->db->trans_complete();
		
		redirect('entities/manage/'.$_POST['orgid'].'#tab_3_6');
	}
	
	function options_entitytypes_edit(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oetid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Entity Types";
		$data['functionname'] = "options_entitytypes";
		$data['functionid'] = "oetid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_entitytypes.*");
		$this->db->from('options_entitytypes');
		$this->db->where('options_entitytypes.oetid', $data['oetid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/options/edit', $data);
	}
	
	function options_entitytypes_update(){
		if ($this->uri->segment(3) == "single"){
			$this->db->trans_start();
			$this->db->where('oetid', $_POST['oetid']);
			$this->db->update('options_entitytypes', $_POST);
			$this->db->trans_complete();
		}elseif ($this->uri->segment(3) == "multiple"){
			$entitytypes = implode(",", $_POST['options']);
			$orgid = $_POST['orgid'];
			$this->db->trans_start();
			$data = array(
				'entitytypes' => $entitytypes
			);
			$this->db->where('organization', $_POST['orgid']);
			$this->db->update('organizations_options', $data);
			$this->db->trans_complete();
		}
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_7');
	}
	
	function options_entitytypes_add(){
		$data['orgid'] = $this->session->userdata('organization');
		$data['oestid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Entity Types";
		$data['functionname'] = "options_entitytypes";
		$data['functionid'] = "oestid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->layout->buildPage('organizations/options/add', $data);
	}
	
	function options_entitytypes_insert(){
	
		$this->db->trans_start();
		$entitytype = array(
			'description' => $_POST['description']
		);
		$this->db->insert('options_entitytypes', $entitytype);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$_POST['orgid'].'#tab_3_7');
	}
	
	function options_personsfields_update(){
		$this->db->trans_start();
		$this->db->where('opfid', $_POST['opfid']);
		$this->db->update('options_personsfields', $_POST);
		$this->db->trans_complete();
		
		redirect('organizations/manage/'.$eid.'#tab_3_8');
	}
	
	function workflows_tickets_edit()	{
		$data['orgid'] = $this->session->userdata('organization');
		$data['oettid'] = $this->uri->segment(3);
		$data['mode'] = "page";
		$data['today'] = date("Y-m-d");
		$data['subtitle'] = "Ticket Workflow";
		$data['functionname'] = "options_tickettypes";
		$data['functionid'] = "oettid";
		
		$data['organization'] = $this->database_model->viewOrganization($data['orgid'],NULL, NULL, NULL, NULL, NULL);
		$data['title'] = $data['organization']->name;
		
		$this->db->select("options_tickettypes.*");
		$this->db->from('options_tickettypes');
		$this->db->where('options_tickettypes.oettid', $data['oettid']);
		$query = $this->db->get();
		$data['row'] = $query->first_row('array');
		
		$this->layout->buildPage('organizations/workflows/edit', $data);
	}
	
	function get_person(){
		$pid = $this->uri->segment(3);
		$this->db->select("persons.*");
		$this->db->from('persons');
		$this->db->where('persons.pid', $pid);
		$result = $this->db->get();
		$row = $result->first_row();
		//echo $row->lastname;
		echo json_encode($row);
	}
	
	function check_seafile(){  
		//get user details
		$this->db->select("users.*");
		$this->db->select("users_settings.*");
		$this->db->from('users');
		$this->db->join('users_settings', 'users_settings.uid = users.uid');
		$this->db->where('users.uid', $this->session->userdata('uid'));
		$result = $this->db->get();
		$user = $result->first_row();
		
		$params = array('user' => $user->seafile_username, 'password' => $user->seafile_password, 'token' => NULL);
		$this->load->library('seafile', $params);

    echo "Ping - ". $this->seafile->ping()."<br>";
    echo "Token - ".$this->seafile->getToken()."<br>";
    echo "Libraries - ".print_r($this->seafile->listLibraries());
  }	
		
  /*
		echo '<h5>Check Account Info</h5>';
		print_r($this->seafile->checkAccountInfo());
		
		echo '<h5>List available libraries</h5>';
		$listLibraries=$this->seafile->listLibraries();
		foreach($listLibraries as $lib)
		{
			echo '<br>LibrariesName: '.$lib->name;
			echo '<br>ID: '.$lib->id;
	
		}
		
		echo '<h5>List available Directories in library</h5>';
		$folders=$this->seafile->listDirectoryEntries('a343a854-1d46-4f49-8711-b3571335bf2a');
		foreach($folders as $fd)
		{
			if($fd->type=='dir'){
				echo '<br>Directory Name: '.$fd->name;
				echo '<br>ID: '.$fd->id;
			    
				echo '<br>==========================================================';
			}
		}
		
		echo '<h5>List available Files in library</h5>';
		foreach($folders as $fd)
		{
			if($fd->type=='file'){
				echo '<br>File Name: '.$fd->name;
				echo '<br>File Size: '.$fd->size;
				echo '<br>ID: '.$fd->id;
				echo '<br>==========================================================';
			}
		
		}
		
		echo '<h5>Create folder</h5>';
		$folderinfo=$this->seafile->createNewDirectory('a343a854-1d46-4f49-8711-b3571335bf2a','TESTFOLDER');
		print_r($folderinfo);
		
		echo '<h5>Rename folder</h5>';
		$folderinfo=$this->seafile->renameDirectory('a343a854-1d46-4f49-8711-b3571335bf2a','TESTFOLDER','Test');
		print_r($folderinfo);
		
		echo '<h5>Delete folder</h5>';
		$folderinfo=$this->seafile->deleteDirectory('a343a854-1d46-4f49-8711-b3571335bf2a','Test');
		print_r($folderinfo);
		
		//echo '<h5>Search file</h5>';
		//$search=$this->seafile->searchLibraries('CIPC New Application.docx');
		//if($search->error_msg)
		//	echo $search->error_msg;
		//else
		//	print_r($search);
		
		
		//echo '<h5>Download file</h5>';	
		//$downloadlink=$this->seafile->downloadFile('a343a854-1d46-4f49-8711-b3571335bf2a','CIPC New Application.docx');
		//print_r($downloadlink);	
		*/
	
}
?>