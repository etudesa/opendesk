<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/*
*	Seafile Library - PHP class wrapper
*
*
*	Seafile - Next-generation Open Source Cloud Storage
*	http://seafile.com/
*
*	Seafile API
*	http://manual.seafile.com/develop/web_api.html
*
*
*
*/
class Seafile {
	private $seafile_user;
	private $seafile_pass;
	private $seafile_url;
	private $seafile_full_url;
	# Seafile port - default 8000
	private $seafile_port;
	private $seafile_token;
	private $seafile_status_message = array(
		'200'	=>	'OK',
		'201'	=>	'CREATED',
		'202'	=>	'ACCEPTED',
		'301'	=>	'MOVED_PERMANENTLY',
		'400'	=>	'BAD_REQUEST',
		'403'	=>	'FORBIDDEN',
		'404'	=>	'NOT_FOUND',
		'409'	=>	'CONFLICT',
		'429'	=>	'TOO_MANY_REQUESTS',
		'440'	=>	'REPO_PASSWD_REQUIRED',
		'441'	=>	'REPO_PASSWD_MAGIC_REQUIRED',
		'500'	=>	'INTERNAL_SERVER_ERROR',
		'520'	=>	'OPERATION_FAILED'
	);
	public $seafile_code;
	public $seafile_status;
	private $handle;
	private $http_options = array();
	private $response_object;
	public $response_object_to_array = false;
	# Curl info
	public $response_info;
	
	function __construct($params){
	
		if(!is_callable('curl_init'))
			echo  "Curl extension is required";
		
		$this->ci = &get_instance();	
	
		$this->ci->load->config('seafile');
		
		
		if($this->ci->config->item('url') && $this->ci->config->item('url')!='' && filter_var($this->ci->config->item('url'), FILTER_VALIDATE_URL))
			$this->seafile_url = $this->ci->config->item('url');
		else
		{
			echo "Error Seafile URL is missing or bad URL format";
		}
			
		if($this->ci->config->item('port') && $this->ci->config->item('port')!='')
			$this->seafile_port = (int) $this->ci->config->item('port');
		else
		{
			echo "Error Seafile Port is missing or invalid";
		}
			
		$this->seafile_full_url = $this->seafile_url.':'.$this->seafile_port;
		
		//if($this->ci->config->item('user') && $this->ci->config->item('user')!='' && filter_var($this->ci->config->item('user'), FILTER_VALIDATE_EMAIL))
		if($params['user'] && $params['user']!='' && filter_var($params['user'], FILTER_VALIDATE_EMAIL))
			$this->seafile_user = filter_var(strtolower(trim(preg_replace('/\\s+/', '', $params['user']))), FILTER_SANITIZE_EMAIL);
		else
		{
			echo "Error Seafile user is missing or bad email format";
			
		}
			
		//if($this->ci->config->item('password') && $this->ci->config->item('password')!='')
		if($params['password'] && $params['password']!='')
			$this->seafile_pass = $params['password'];
		else
		{
			echo "Error Seafile  password is required";
			
		}
		
/*    
		if($params['token'] && $params['token']!=''){
			$this->seafile_token = $params['token'];
    }else{
			//echo "Error Seafile token is required";
			$this->seafile_token = $this->getToken();
    }
*/
		/*
		*	Default curl config
		*/
		$this->http_options[CURLOPT_RETURNTRANSFER] = true;
		$this->http_options[CURLOPT_FOLLOWLOCATION] = false;
		$this->http_options[CURLOPT_SSL_VERIFYPEER] = false;
		
		/*
		*	Return seafile token
		*/
		$this->seafile_token = $this->getToken();
		
	}
	
	/*
	*	Return Seafile token
	*	
	*	@return - The Seafile token
	*/
	public function getToken(){
	
		$data = $this->decode($this->post($this->seafile_full_url.'/api2/auth-token/', array(
			'username'=> $this->seafile_user, 
			'password'=> $this->seafile_pass
		)));
		return (string)$data->token;
	}
	
	
	/*
	*	Ping Seafile server when logged
	*	
	*	@return array - The account infos
	*/
	public function ping(){
		return $this->decode($this->get($this->seafile_full_url.'/api2/auth/ping/', array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	/*
	*	Return Seafile account information
	*	
	*	@return array - The available libraries
	*/
	public function checkAccountInfo(){
		return $this->decode($this->get($this->seafile_full_url.'/api2/account/info/', array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	/*
	*	Return Seafile default library
	*	
	*	@return array - Default library infos
	*/
	public function getDefaultLibrary(){
	
		return $this->decode($this->get($this->seafile_full_url.'/api2/default-repo/', array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	/*
	*	List Seafile libraries
	*	
	*	@return array - The available libraries
	*/
	public function listLibraries(){
		return $this->decode($this->get($this->seafile_full_url.'/api2/repos/', array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	/*
	*	Return Seafile library infos
	*	
	*	@param string $library_id
	*	@return array - The library infos
	*/
	public function getLibraryInfo($library_id){
		return $this->decode($this->get($this->seafile_full_url.'/api2/repos/'.$library_id.'/', array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	/*
	*	Return Seafile libraries infos
	*	
	*	@param array $libraries
	*	@return array - The libraries infos
	*/
	public function getLibrariesInfo($libraries){
		if(is_array($libraries)){
		
			$return = array();
			foreach($libraries as $lib):
				$return[] = $this->decode($this->get($this->seafile_full_url.'/api2/repos/'.$lib->id.'/', array(
					CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
				)));
			endforeach;
			return $return;
			
		}else{
			return $this->getLibraryInfo($libraries[0]->id);
		}
	}
	
	
	/*
	*	Create a new library
	*	
	*	@param array $libraries
	*	@return array - The libra
	*/
	public function createLibrary($name, $desc, $password = false){
	
		if($password){
			$post = array(
				'name' 		=> $name,
				'desc' 			=> $desc,
				'passwd'		=> $password
			);
		}else{
			$post = array(
				'name' 		=> $name,
				'desc' 			=> $desc
			);
		}
	
		return $this->decode($this->post($this->seafile_full_url.'/api2/repos/', $post,
			array(
				CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token,'Content-Type: multipart/form-data')
			)
		));
	}
	
	
	/*
	*	List Seafile directory entries
	*	
	*	@param string $library_id - Seafile library id
	*	@param string $directory_name - Seafile directory name
	*	@return array - The files in driectory
	*/
	public function listDirectoryEntries($library_id,$directory_name){
	
		return $this->decode($this->get($this->seafile_full_url.'/api2/repos/'.$library_id.'/dir/?p=/'.urlencode($directory_name), 
			array(CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	
	
	
	/*
	*	Create a new directory in a library
	*	
	*	@param string $library_id - Seafile library id
	*	@param string $directory_name - New directory name
	*	@return array - The link for new directory
	*/
	public function createNewDirectory($library_id, $directory_name){
		return $this->decode($this->post($this->seafile_full_url.'/api2/repos/'.$library_id.'/dir/?p=/'.urlencode($directory_name), array('operation' => 'mkdir'),	
			array(CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token,'Content-Type: multipart/form-data'))
		));
	}
	
	
	/*
	*	Rename a directory in a library
	*	
	*	@param string $library_id - Seafile library id
	*	@param string $directory_name - Old directory name
	*	@param string $directory_new_name - New directory name
	*	@return array - The link for new directory
	*/
	public function renameDirectory($library_id, $directory_name, $directory_new_name){
		return $this->decode($this->post($this->seafile_full_url.'/api2/repos/'.$library_id.'/dir/?p=/'.urlencode($directory_name), array(
				'operation' 	=> 'rename',
				'newname'	=> $directory_new_name
			),
			array(
				CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token,'Content-Type: multipart/form-data')
			)
		));
	}
	
	
	/*
	*	Download link for Seafile file
	*	
	*	@param string $library_id - Seafile library id
	*	@param string $file_name - Seafile file name
	*	@return string - The link for download
	*/
	public function downloadFile($library_id, $file_name){
	
		return $this->decode($this->get($this->seafile_full_url.'/api2/repos/'.$library_id.'/file/?p=/'.$file_name, array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
		
	}
	
	
	/*
	*	Get Upload link for Seafile file upload
	*	
	*	@param string $library_id - Seafile library id
	*	@return string - The link for download
	*/
	public function getUploadLink($library_id, $directory_name){
		return $this->decode($this->get($this->seafile_full_url.'/api2/repos/'.$library_id.'/upload-link/?p=/'.urlencode($directory_name),
			array(CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
	}
	
	/*
	*	Upload file for Seafile
	*	
	*	@param string $upload_link - Seafile upload link
	*	@param string $file - file
	*	@param string $filename - filename
	*	@param string $upload_filename - Seafile upload directory
	*	@return string - result
	*/
	public function uploadFile($upload_link, $file, $filename, $upload_directory){
		$curlFile = new Curlfile($file);
		
		return ($this->decode($this->post($upload_link,
			array(
				'file' 	=> $curlFile,
				'filename'	=> $filename,
				'parent_dir' => "/".$upload_directory
			),
			array(	
				CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
			)
		)));
		
	}	
	
	
	/*
	*	Decode answer to object format instead of json
	*
	*	@param array $data - The json encoded response
	*	@param bool $this->response_object_to_array default false - If true return array from json instead of object
	*
	*/
	public function decode($data){
	
		if(!$this->response_object_to_array)
			return json_decode($data);
		else
			return json_decode($data, true);
	}
	
	
	/*
	*	Analyse curl answer
	*
	*	@param array $res The curl object
	*	@throws Exception
	*
	*/
	private function http_parse_message($res) {
	
		if(! $res)
			return curl_error($this->handle);
		$this->response_info = curl_getinfo($this->handle);
		$code = $this->response_info['http_code'];
		
		$this->seafile_code = $code;
		$this->seafile_status = $this->seafile_status_message[$code];
		if($code == 404)
			return ($this->seafile_code. ' - '.$this->seafile_status . ' - ' .curl_error($this->handle));
		if($code >= 400 && $code <=600)
			return($this->seafile_code. ' - '.$this->seafile_status . ' - ' .'Server response status was: ' . $code . ' with response: [' . $res . ']');
		if(!in_array($code, range(200,207)))
			return($this->seafile_code. ' - '.$this->seafile_status . ' - ' .'Server response status was: ' . $code . ' with response: [' . $res . ']');
	}
	
	/*
	*	Perform a GET call to server
	* 
	*	Additionaly in $response_object and $response_info are the 
	*	response from server and the response info as it is returned 
	*	by curl_exec() and curl_getinfo() respectively.
	* 
	*	@param string $url The url to make the call to.
	*	@param array $http_options Extra option to pass to curl handle.
	*	@return string The response from curl if any
	*/
	public function get($url, $http_options = array()) {
		
		$http_options = $http_options + $this->http_options;
		$this->handle = curl_init($url);
		if(! curl_setopt_array($this->handle, $http_options))
			return("Error setting cURL request options");
		
		$this->response_object = curl_exec($this->handle);
		$this->http_parse_message($this->response_object);
		curl_close($this->handle);
		return $this->response_object;
	}
	/*
	*	Perform a POST call to the server
	* 
	*	Additionaly in $response_object and $response_info are the 
	*	response from server and the response info as it is returned 
	*	by curl_exec() and curl_getinfo() respectively.
	* 
	*	@param string $url The url to make the call to.
	*	@param string|array The data to post. Pass an array to make a http form post.
	*	@param array $http_options Extra option to pass to curl handle.
	*	@return string The response from curl if any
	*/
	public function post($url, $fields = array(), $http_options = array()) {
		
		$http_options = $http_options + $this->http_options;
		$http_options[CURLOPT_POST] = true;
		$http_options[CURLOPT_POSTFIELDS] = $fields;
		
		//if(is_array($fields))
			//$http_options[CURLOPT_HTTPHEADER] = array('Content-Type: multipart/form-data');
			
		
		$this->handle = curl_init($url);
		if(! curl_setopt_array($this->handle, $http_options))
			echo("Error setting cURL request options.");
			
			
		
		$this->response_object = curl_exec($this->handle);
		
		
		$this->http_parse_message($this->response_object);
		
		curl_close($this->handle);
		
	
		return $this->response_object;
	}
	/*
	*	Perform a PUT call to the server
	* 
	*	Additionaly in $response_object and $response_info are the 
	*	response from server and the response info as it is returned 
	*	by curl_exec() and curl_getinfo() respectively.
	* 
	*	@param string $url The url to make the call to.
	*	@param string|array The data to post.
	*	@param array $http_options Extra option to pass to curl handle.
	*	@return string The response from curl if any
	*/
	public function put($url, $data = '', $http_options = array()) {
		
		$http_options = $http_options + $this->http_options;
		$http_options[CURLOPT_CUSTOMREQUEST] = 'PUT';
		$http_options[CURLOPT_POSTFIELDS] = $data;
		$this->handle = curl_init($url);
		if(! curl_setopt_array($this->handle, $http_options))
			return("Error setting cURL request options.");
		$this->response_object = curl_exec($this->handle);
		$this->http_parse_message($this->response_object);
		curl_close($this->handle);
		return $this->response_object;
	}
	/*
	* Perform a DELETE call to server
	* 
	* Additionaly in $response_object and $response_info are the 
	* response from server and the response info as it is returned 
	* by curl_exec() and curl_getinfo() respectively.
	* 
	* @param string $url The url to make the call to.
	* @param array $http_options Extra option to pass to curl handle.
	* @return string The response from curl if any
	*/
	public function delete($url, $http_options = array()) {
		
		$http_options = $http_options + $this->http_options;
		$http_options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
		$this->handle = curl_init($url);
		if(! curl_setopt_array($this->handle, $http_options))
			return("Error setting cURL request options.");
		$this->response_object = curl_exec($this->handle);
		$this->http_parse_message($this->response_object);
		curl_close($this->handle);
		return $this->response_object;
	}
	
	
	
	/*
	*	Delete a folder in a library
	*	
	*	@param string $library_id - Seafile library id
	*	@param string $directory_name - directory name to be deleted
	*	@return array - The link for new directory
	*/
	
	public function deleteDirectory($library_id,$directory_name)
	{
	return $this->decode($this->delete($this->seafile_full_url.'/api2/repos/'.$library_id.'/dir/?p=/'.urlencode($directory_name), 
			array(
				CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
			)
		));	
		
	
	}
	
	
	/*
	*	Search Libraries
	*	
	*	@param string $keyword - search file name
	*	@param number $per_page  - number of records per page
	*	@return array - results
	*/
	
	public function searchLibraries($keyword,$per_page='')
	{
		return $this->decode($this->get($this->seafile_full_url.'/api2/search/?q='.urlencode($keyword), array(
			CURLOPT_HTTPHEADER => array('Authorization: Token '.$this->seafile_token)
		)));
		
	}
}