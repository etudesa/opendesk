<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Database_model extends CI_Model 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 */
	function __construct(){
		parent::__construct();
    $this->db->db_select($this->session->userdata('database'));
	}
	
	/**
	 * get entities
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getEntities($organization, $mode, $status, $type, $createdby, $updated){

    $this->db->select("entities.*");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		if (isset($status)){ $this->db->where('entities.status', $status); }
    if (isset($mode)){ $this->db->where('entities.mode', $mode); }
    if (isset($type)){ $this->db->where('entities.type', $type); }
    if (isset($createdby)){ $this->db->where('entities.createdby', $createdby); }    
    if (isset($updated)){ $this->db->where('entities.updated', $updated); }    
		$this->db->where('entities.organization', $organization);
		$this->db->order_by('entities.name','ASC');
		$query = $this->db->get();
                      
    return $query;
  }
  
  	/**
	 * view entity
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function viewEntity($entity, $mode, $status, $type, $createdby, $updated){
		$this->db->select("entities.*");
		$this->db->select("entities_contacts.pid");
		$this->db->select("organizations_users.fullname as entityowner");
		$this->db->select("persons.lastname, persons.firstname, persons.preferredname, persons.idnumber, persons.dateofbirth, persons.physicaladdress, persons.postaladdress, persons.homephone, persons.officephone, persons.mobile, persons.email");
		$this->db->select("options_entitytypes.description as entitytype");
		$this->db->select("options_entitystatus.description as entitystatus");
		$this->db->select("options_entitymodes.description as entitymode");
		$this->db->from('entities');
		$this->db->join('organizations_users', 'organizations_users.uid = entities.createdby');
		$this->db->join('entities_contacts', 'entities_contacts.ecid = entities.primarycontact', 'left outer');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
		$this->db->join('options_entitytypes', 'options_entitytypes.oetid = entities.type');
		$this->db->join('options_entitystatus', 'options_entitystatus.oesid = entities.status');
		$this->db->join('options_entitymodes', 'options_entitymodes.oemid = entities.mode');
		$this->db->where('entities.eid', $entity);
		if (isset($status)){ $this->db->where('entities.status', $status); }
    if (isset($mode)){ $this->db->where('entities.mode', $mode); }
    if (isset($type)){ $this->db->where('entities.type', $type); }
		$this->db->order_by('entities.name','ASC');
		$query = $this->db->get();

    return $query->first_row();
  }
  
  
	/**
	 * get contacts
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getContacts($entity, $status, $accesslevel, $updated){  
		$this->db->select("entities_contacts.*");
		$this->db->select("options_contactrelationships.description as relationshipdescription");
		$this->db->select("persons.*");
		$this->db->from('entities_contacts');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
		$this->db->join('options_contactrelationships', 'options_contactrelationships.oecrid = entities_contacts.relationship', 'left outer');
		$this->db->where('entities_contacts.eid', $entity);     
		$query = $this->db->get();
	
		return $query;
  }
  
	/**
	 * view contact
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function viewContact($contact){  
    $this->db->select("entities.*");
    $this->db->select("entities_contacts.*");
    $this->db->select("persons.*");
    $this->db->from('entities');
    $this->db->join('entities_contacts', 'entities_contacts.eid = entities.eid');
    $this->db->join('persons', 'persons.pid = entities_contacts.pid', 'left outer');
    $this->db->where('entities_contacts.ecid', $contact);
    $query = $this->db->get();

    return $query->first_row();
  }
  
	/**
	 * get correspondence
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getCorrespondence($entity, $type, $createdby){
		$this->db->select("entities_correspondence.*");
		$this->db->select("organizations_users.fullname as entityowner");
		$this->db->select("options_correspondencetypes.fa_icon, options_correspondencetypes.description as correspondencetypedescription");
		$this->db->from('entities_correspondence');
		$this->db->join('organizations_users', 'organizations_users.uid = entities_correspondence.createdby');
		$this->db->join('options_correspondencetypes', 'options_correspondencetypes.oectid = entities_correspondence.type');
		$this->db->where('entities_correspondence.eid', $entity);
		$this->db->where('entities_correspondence.type', $type);
		$this->db->order_by('entities_correspondence.updated', 'DESC');
		$query = $this->db->get();
                      
    return $query;
  }
  
	/**
	 * get tickets
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getTickets($entity, $type, $ownedby, $assignedto, $reminderdate, $duedate, $status){
		$this->db->select("entities_tickets.*");
    $this->db->select("entities.name as entityname");
		$this->db->select("organizations_users.fullname as ticketowner");
		$this->db->select("options_tickettypes.description as tickettype");
		$this->db->select("options_ticketstatus.description as ticketstatus");
		$this->db->from('entities_tickets');
		$this->db->join('entities', 'entities.eid = entities_tickets.eid');
    $this->db->join('organizations_users', 'organizations_users.uid = entities_tickets.ownedby');
		$this->db->join('options_tickettypes', 'options_tickettypes.oettid = entities_tickets.type');
		$this->db->join('options_ticketstatus', 'options_ticketstatus.oetsid = entities_tickets.status');
		if (isset($entity)){ $this->db->where('entities_tickets.eid', $entity);}
    if (isset($type)){ $this->db->where('entities_tickets.type', $type); }
    if (isset($ownedby)){ $this->db->where('entities_tickets.ownedby', $ownedby); }
    if (isset($status)){ $this->db->where('entities_tickets.status', $status); }
		$this->db->order_by('entities_tickets.duedate', 'ASC');
		$query = $this->db->get();
                      
    return $query;
  }
  
  function viewTicket($ticket){
		$this->db->select("entities_tickets.*");
		$this->db->select("options_tickettypes.oettid, options_tickettypes.description as tickettypedescription, options_tickettypes.field1_description, options_tickettypes.field2_description, options_tickettypes.field3_description, options_tickettypes.field4_description, options_tickettypes.field5_description");
		$this->db->from('entities_tickets');
		$this->db->join('options_tickettypes', 'options_tickettypes.oettid = entities_tickets.type');
		$this->db->where('entities_tickets.etid', $ticket);
		$query = $this->db->get();
    
    return $query->first_row();
  }
  
	/**
	 * get products
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getProducts($entity, $type, $category, $provider, $issuedate, $status){
		$this->db->select("entities_products.*");
		$this->db->select("options_productproviders.description as productproviderdescription");
		$this->db->select("options_producttypes.description as producttypedescription");
		$this->db->select("options_productstatus.description as productstatusdescription");
		$this->db->select("options_productcategories.description as productcategorydescription");
		$this->db->from('entities_products');
		$this->db->join('options_productproviders', 'options_productproviders.oeppid = entities_products.provider');
		$this->db->join('options_producttypes', 'options_producttypes.oeptid = entities_products.type');
		$this->db->join('options_productstatus', 'options_productstatus.oepsid = entities_products.status');
		$this->db->join('options_productcategories', 'options_productcategories.oepcid = entities_products.category');
		$this->db->where('entities_products.eid', $entity);
		$query = $this->db->get();
                      
    return $query;
  }
  
	/**
	 * view product
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function viewProduct($product){
		$this->db->select("entities_products.*");
		$this->db->select("options_producttypes.*");
		$this->db->select("options_productproviders.description as productproviderdescription");
		$this->db->select("options_producttypes.description as producttypedescription");
		$this->db->select("options_productstatus.description as productstatusdescription");
		$this->db->select("options_productcategories.description as productcategorydescription");
		$this->db->from("entities_products");
		$this->db->join('options_productproviders', 'options_productproviders.oeppid = entities_products.provider');
		$this->db->join('options_producttypes', 'options_producttypes.oeptid = entities_products.type');
		$this->db->join('options_productstatus', 'options_productstatus.oepsid = entities_products.status');
		$this->db->join('options_productcategories', 'options_productcategories.oepcid = entities_products.category');
		$this->db->where('entities_products.epid', $product);
		$query = $this->db->get();
                     
		return $query->first_row();
  }
	/**
	 * get services
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getServices($entity, $type, $issuedate, $status){
		$this->db->select("entities_services.*");
		$this->db->select("options_servicetypes.description as servicetypedescription");
		$this->db->select("options_servicestatus.description as servicestatusdescription");
		$this->db->from('entities_services');
		$this->db->join('options_servicetypes', 'options_servicetypes.oestid = entities_services.type');
		$this->db->join('options_servicestatus', 'options_servicestatus.oessid = entities_services.status');
		$this->db->where('entities_services.eid', $entity);
		$query = $this->db->get();
                      
    return $query;
  }
  
	/**
	 * view service
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function viewService($service){
		$this->db->select("entities_services.*");
	  $this->db->select("options_servicetypes.description as servicetypedescription");
		$this->db->select("options_servicestatus.description as servicestatusdescription");
		$this->db->from("entities_services");
		$this->db->join('options_servicetypes', 'options_servicetypes.oestid = entities_services.type');
		$this->db->join('options_servicestatus', 'options_servicestatus.oessid = entities_services.status');
		$this->db->where('entities_services.esid', $service);
		$query = $this->db->get();
    
		return $query->first_row();
  }
	/**
	 * get activities
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getActivities($entity, $type, $createdby){
		$this->db->select("entities_activities.*");
		$this->db->select("organizations_users.fullname as userfullname");
		$this->db->select("options_activitytypes.description as activitytypedescription");
		$this->db->from('entities_activities');
		$this->db->join('organizations_users', 'organizations_users.uid = entities_activities.createdby');
		$this->db->join('options_activitytypes', 'options_activitytypes.oeatid = entities_activities.type');
		$this->db->where('entities_activities.eid', $entity);
		$this->db->order_by('entities_activities.updated', 'DESC');
		$query = $this->db->get();
                      
    return $query;
  }
  
	/**
	 * get medicals
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getMedicals($entity){
		$this->db->select("entities_medicals.*");
		$this->db->from('entities_medicals');
		$this->db->where('entities_medicals.eid', $entity);
		$query = $this->db->get();
                      
    return $query;
  }
  
	/**
	 * get financials
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function getFinancials($entity){
		$this->db->select("entities_financials.*");
		$this->db->from('entities_financials');
		$this->db->where('entities_financials.eid', $entity);
		$query = $this->db->get();
                      
    return $query;
  }
  
  
  function viewFinancial($financial){
		$this->db->select("entities_financials.*");
		$this->db->from("entities_financials");
		$this->db->where('entities_financials.efid', $financial);
		$query = $this->db->get();
    
		return $query->first_row();
  }
  
	/**
	 * get files
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getFiles($entity, $type){  
		$this->db->select("entities_files.*");
		$this->db->from('entities_files');
		$this->db->where('entities_files.eid', $entity);
		if (isset($type)){ $this->db->where('entities_files.type', $type); }
		$query = $this->db->get();
	
		return $query;
  }
  
	/**
	 * view file
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
  function viewFile($file){
		$this->db->select("entities_files.*");
		$this->db->from("entities_files");
		$this->db->where('entities_files.efid', $file);
		$query = $this->db->get();
    
		return $query->first_row();
  }
  
	/**
	 * Verify that an email address is found on the database
	 *
	 * @access	public
	 * @param	null
	 * @return	string
	 */	   
	function verifyEmail($fromaddress){
		$this->db->select('entities.*');
		$this->db->from('entities');
		$this->db->join('entities_contacts', 'entities_contacts.eid = entities.eid');
		$this->db->join('persons', 'persons.pid = entities_contacts.pid');
		$this->db->where('persons.email', $fromaddress);
		$query = $this->db->get();
		$entity = $query->first_row();
		
		if (isset($entity->eid)){
			return $entity->eid;
		}else{
			return NULL;
		}
		
	}
  
	/**
	 * Get the person details
	 *
	 * @access	public
	 * @param	null
	 * @return	string
	 */	 
  function getPerson($person){
		$this->db->select("persons.*");
		$this->db->from('persons');
		$this->db->where('persons.pid', $person);
		$result = $this->db->get();
    
    return $result->first_row();
  }
  
	/**
	 * get organizations
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getOrganizations($status, $type, $createdby, $updated){
  	$this->db->select("organizations.*");
		$this->db->select("options_organizationstatus.description as organizationstatus");
		$this->db->select("options_organizationtypes.description as organizationtype");
		$this->db->from('organizations');
		$this->db->join('options_organizationstatus', 'options_organizationstatus.oosid = organizations.status');
		$this->db->join('options_organizationtypes', 'options_organizationtypes.ootid = organization.type');
		if (isset($status)){ $this->db->where('organizations.status', $status); }
    if (isset($type)){ $this->db->where('organizations.type', $type); }
		$this->db->order_by('organizations.name','ASC');
		$query = $this->db->get();
                      
    return $query;
  }
    
	/**
	 * view organization
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function viewOrganization($organization, $status, $type, $createdby, $updated){
  	$this->db->select("organizations.*");
    $this->db->select("organizations_users.*");
    $this->db->select("organizations_settings.*");
    $this->db->select("organizations_options.*");
		$this->db->select("options_organizationstatus.description as organizationstatus");
		$this->db->from('organizations');
    $this->db->join('organizations_users', 'organizations_users.organization = organizations.orgid');
    $this->db->join('organizations_settings', 'organizations_settings.organization = organizations.orgid');
    $this->db->join('organizations_options', 'organizations_options.organization = organizations.orgid');
		$this->db->join('options_organizationstatus', 'options_organizationstatus.oosid = organizations.status');
		if (isset($status)){ $this->db->where('organizations.status', 1); }
		$this->db->where('organizations.orgid', $organization);
		$this->db->order_by('organizations.name','ASC');
		$query = $this->db->get();
                      
    return $query->first_row();
  }
  
	/**
	 * get users
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getUsers($organization, $status, $accesslevel, $updated){  
		$this->db->select("organizations_users.*");
		$this->db->select("options_userstatus.description as userstatus");
		$this->db->select("options_useraccesslevels.description as useraccesslevel");
		$this->db->from('organizations_users');
		$this->db->join('options_userstatus', 'options_userstatus.ousid = organizations_users.status');
		$this->db->join('options_useraccesslevels', 'options_useraccesslevels.oualid = organizations_users.accesslevel');
		$this->db->where('organizations_users.organization', $organization);
		if (isset($accesslevel)){ $this->db->where('organizations_users.accesslevel', $accesslevel); }
		if (isset($status)){ $this->db->where('organizations_users.status', $status); }
		$query = $this->db->get();
	 
		return $query;
  }
  
  	/**
	 * view user
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function viewUser($user){
		$this->db->select("organizations_users.*");
    $this->db->select("organizations.name as organizationname");
		$this->db->from('organizations_users');
    $this->db->join('organizations', 'organizations.orgid = organizations_users.organization');
		$this->db->where('organizations_users.uid', $user);
		$query = $this->db->get();

    return $query->first_row();
  }
  
 	/**
	 * get user tasks
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function getUserTasks($organization, $user, $status){
    $this->db->select("options_usertaskstatus.description as usertaskstatus");
		$this->db->select("organizations_users_tasks.*");
		$this->db->select("organizations_users.*");
		$this->db->from('organizations_users_tasks');
		$this->db->join('organizations_users', 'organizations_users_tasks.uid = organizations_users.uid');
    $this->db->join('options_usertaskstatus', 'organizations_users_tasks.status = options_usertaskstatus.outsid');
		if (isset($user)){ $this->db->where('organizations_users_tasks.uid', $user); }
		if (isset($organization)){ $this->db->where('organizations_users.organization', $organization); }
		if (isset($status)){ $this->db->where('organizations_users_tasks.status', $status); }
		$query = $this->db->get();
    
    return $query;
  }
  
   	/**
	 * get user tasks
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function viewUserTask($usertask){
    $this->db->select("options_usertaskstatus.description as usertaskstatus");
		$this->db->select("organizations_users_tasks.*");
		$this->db->select("organizations_users.*");
		$this->db->from('organizations_users_tasks');
		$this->db->join('organizations_users', 'organizations_users_tasks.uid = organizations_users.uid');
    $this->db->join('options_usertaskstatus', 'organizations_users_tasks.status = options_usertaskstatus.outsid');
		$this->db->where('organizations_users_tasks.utid', $usertask);
		$query = $this->db->get();
    
    return $query->first_row();
  }

 	/**
	 * get organization groups
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getOrganizationGroups($organization, $status, $updated){  
		$this->db->select("organizations_groups.*");
		$this->db->from('organizations_groups');
		$this->db->where('organizations_groups.organization', $organization);
		$query = $this->db->get();
	 
		return $query;
  }
 
 	/**
	 * get organization communications
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function getOrganizationCommunications($organization, $type, $mode, $status, $updated){  
		$this->db->select("organizations_communications.*");
		$this->db->select("organizations_groups.name as groupname");
		$this->db->select("options_communicationmodes.description as communicationmode");
		$this->db->select("options_communicationstatus.description as communicationstatus");
		$this->db->from('organizations_communications');
		$this->db->join('organizations_groups', 'organizations_communications.group = organizations_groups.egid');
		$this->db->join('options_communicationmodes', 'organizations_communications.mode = options_communicationmodes.ocmid');
		$this->db->join('options_communicationstatus', 'organizations_communications.status = options_communicationstatus.ocsid');
		if (isset($type)){ $this->db->where('organizations_communications.type', $type); }
		$this->db->where('organizations_communications.organization', $organization);
		$query = $this->db->get();
	 
		return $query;
  }
  
 	/**
	 * view organization communication
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */	   
	function viewOrganizationCommunications($organizationcommunication){  
		$this->db->select("organizations_communications.*");
		$this->db->select("organizations_groups.name as groupname");
		$this->db->select("options_communicationmodes.description as communicationmode");
		$this->db->select("options_communicationstatus.description as communicationstatus");
		$this->db->from('organizations_communications');
		$this->db->join('organizations_groups', 'organizations_communications.group = organizations_groups.egid');
		$this->db->join('options_communicationmodes', 'organizations_communications.mode = options_communicationmodes.ocmid');
		$this->db->join('options_communicationstatus', 'organizations_communications.status = options_communicationstatus.ocsid');
		$this->db->where('organizations_communications.ecid', $organizationcommunication);
		$query = $this->db->get();

		return $query->first_row();
  }
  
 	/**
	 * get entity tickets
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function getEntityTickets($organization, $entity, $products, $service, $ownedby, $assignedto, $type, $reminderdate, $duedate, $status){
		$this->db->select("entities_tickets.*");
		$this->db->select("entities.*");
		$this->db->select("options_tickettypes.description as tickettypedescription");
		$this->db->from('entities');
		$this->db->join('entities_tickets', 'entities_tickets.eid = entities.eid');
		$this->db->join('options_tickettypes', 'options_tickettypes.oettid = entities_tickets.type');
		if (isset($ownedby)){  $this->db->where('entities_tickets.ownedby', $ownedby); }
    if (isset($assignedto)){ $this->db->where('entities_tickets.assignedto', $assignedto); } 
		if (isset($status)){ $this->db->where('entities_tickets.status', $status); }
		$this->db->where('entities.organization', $organization);		
		$this->db->order_by("entities_tickets.duedate","ASC");
		$query = $this->db->get();
    
    return $query;
  }
  
  /**
	 * get entity products
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function getEntityProducts($entity, $category, $type, $provider, $status, $issuedate){
		$this->db->select("entities_products.*");
		$this->db->select("options_productcategories.description as productcategorydescription");
		$this->db->select("options_producttypes.description as producttypedescription");
		$this->db->from('entities_products');
		$this->db->where('entities_products.eid', $entity);
		$this->db->join('options_producttypes', 'options_producttypes.oeptid = entities_products.type');
		$this->db->join('options_productcategories', 'options_productcategories.oepcid = entities_products.category');
		$query = $this->db->get();
    
    return $query;
  }
 
 	/**
	 * get entity services
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function getEntityServices($entity, $identifier, $type, $status, $issuedate){
		$this->db->select("entities_services.*");
		$this->db->select("options_servicetypes.description as servicetypedescription");
		$this->db->from('entities_services');
		$this->db->where('entities_services.eid', $entity);		
		$this->db->join('options_servicetypes', 'options_servicetypes.oestid = entities_services.type');
		$query = $this->db->get();
    
    return $query;
  }
  
 	/**
	 * get entity types
	 *
	 * @access	public
	 * @param	null
	 * @return array
	 */
  function getEntityTypes($organization){
    $this->db->select("options_entitytypes.*");
    $this->db->from('options_entitytypes');
    $this->db->order_by('options_entitytypes.description','ASC');   
    $options = $this->db->get();   

		if (isset($organization)){      
      $this->db->select("organizations_options.entitytypes");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $entitytypes = explode(',',$row->entitytypes);
      
      $this->db->select("options_entitytypes.*");
      $this->db->from('options_entitytypes');
      $this->db->where_in('options_entitytypes.oetid', $entitytypes);
      return $this->db->get();
    }else{
      return $options;
    }
  }
  
  function getPersons($organization){
		$this->db->select("persons.*");
		$this->db->from('persons');
		$this->db->where('persons.organization', $organization);
		$query = $this->db->get();
    
    return $query;
  }
  
  function getProductCategories($organization){
    $this->db->select("options_productcategories.*");
    $this->db->from('options_productcategories');
    $this->db->order_by('options_productcategories.description','ASC');    
    $options = $this->db->get();    
    
    if (isset($organization)){    
      $this->db->select("organizations_options.productcategories");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $productcategories = explode(',',$row->productcategories);
      
      $this->db->select("options_productcategories.*");
      $this->db->from('options_productcategories');
      $this->db->where_in('options_productcategories.oepcid', $productcategories);
      return $this->db->get();
    }else{
      return $options;
    }
    
  }
  
  function getProductTypes($organization){
    $this->db->select("options_producttypes.*");
    $this->db->from('options_producttypes');
    $this->db->order_by('options_producttypes.description','ASC');       
    $options = $this->db->get();
    
    if (isset($organization)){           
      $this->db->select("organizations_options.producttypes");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $producttypes = explode(',',$row->producttypes);
      
      $this->db->select("options_producttypes.*");
      $this->db->from('options_producttypes');
      $this->db->where_in('options_producttypes.oeptid', $producttypes);
      return $this->db->get();
    }else{
      return $options;
    }
  }
  
  function getProductProviders($organization){
    $this->db->select("options_productproviders.*");
    $this->db->from('options_productproviders');
    $this->db->order_by('options_productproviders.description','ASC');
    $options = $this->db->get();
    
    if (isset($organization)){       
      $this->db->select("organizations_options.productproviders");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $productproviders = explode(',',$row->productproviders);
      
      $this->db->select("options_productproviders.*");
      $this->db->from('options_productproviders');
      $this->db->where_in('options_productproviders.oeppid', $productproviders);
      return $this->db->get();
    }else{
      return $options;
    }
  }
  
  function getTicketTypes($organization){
    $this->db->select("options_tickettypes.*");
    $this->db->from('options_tickettypes');
    $this->db->order_by('options_tickettypes.description','ASC');
    $options = $this->db->get();
    
    if (isset($organization)){      
      $this->db->select("organizations_options.tickettypes");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $tickettypes = explode(',',$row->tickettypes);
      
      $this->db->select("options_tickettypes.*");
      $this->db->from('options_tickettypes');
      $this->db->where_in('options_tickettypes.oettid', $tickettypes);
      return $this->db->get();
    }else{
      return $options;
    }
    
  }
  
  function getServiceTypes($organization){
    $this->db->select("options_servicetypes.*");
    $this->db->from('options_servicetypes');
    $this->db->order_by('options_servicetypes.description','ASC');
    $options = $this->db->get();
    
    if (isset($organization)){        
      $this->db->select("organizations_options.servicetypes");
      $this->db->from('organizations_options');
      $this->db->where('organizations_options.organization', $organization);
      $options = $this->db->get();
      $row = $options->first_row();
      $servicetypes = explode(',',$row->servicetypes);
      
      $this->db->select("options_servicetypes.*");
      $this->db->from('options_servicetypes');
      $this->db->where_in('options_servicetypes.oestid', $servicetypes);
      return $this->db->get();
    }else{
      return $options;
    }
  }
  
  function getOrganizationTypes(){
    
  }
  
  function getOrganizationStatus(){
    
  }
}
?>