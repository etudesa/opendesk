	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Search Results <small>"<?=$searchstring?>"</small>
					</h3>
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a data-toggle="modal" href="#advancedsearchmodal">Advanced Search</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="<?=site_url("dashboard")?>">
								Dashboard
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">
								Search Results
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Entities
								</a>
							</li>
							<? if (($profile->correspondence == 1) && ($entity->status >= 2)){ ?>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Correspondence
								</a>
							</li>
							<? } ?>
							<? if (($profile->support == 1) && ($entity->status == 3)){ ?>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Support
								</a>
							</li>
							<? } ?>
						</ul>
						<div class="tab-content">
							<!--tab_1-->
							<div class="tab-pane active" id="tab_1">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet-body">
											<table class="table table-striped table-bordered table-hover" id="entities">
											<thead>
											<tr>
												<th width="100%">
													 Name
												</th>
												<th>
													 Type
												</th>
												<th>
													 Status
												</th>
												<th>
													 Mode
												</th>
											</tr>
											</thead>
											<tbody>
											<? FOREACH($entities->result() as $row): ?>
											<tr class="odd gradeX">
												<td width="100%" nowrap>
													<a href="<?=site_url("entities/view/".$row->eid)?>">
													 <?=$row->name?>
													</a>
												</td>
												<td nowrap>
													 <?=$row->entitytype?>
												</td>
												<td nowrap>
													 <?=$row->entitystatus?>
												</td>
												<td nowrap>
													<?=$row->entitymode?>
												</td>
											</tr>
											<? ENDFOREACH; ?>
											</tbody>
											</table>
										</div>
										<!--end row-->										
									</div>
								</div>
							</div>
							<!--end tab-pane-->
							<!--tab_2-->
							<div class="tab-pane" id="tab_2">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet-body">
											<table class="table table-striped table-bordered table-hover" id="correspondence">
											<thead>
											<tr>
												<th width="100%">
													 Name
												</th>
												<th>
													 Type
												</th>
												<th>
													 Status
												</th>
												<th>
													 Mode
												</th>
											</tr>
											</thead>
											<tbody>
											<? FOREACH($correspondence->result() as $row): ?>
											<tr class="odd gradeX">
												<td width="100%" nowrap>
													<a href="<?=site_url("entities/view/".$row->eid)?>">
													 <?=$row->name?>
													</a>
												</td>
												<td nowrap>
													 <?=$row->entitytype?>
												</td>
												<td nowrap>
													 <?=$row->entitystatus?>
												</td>
												<td nowrap>
													<?=$row->entitymode?>
												</td>
											</tr>
											<? ENDFOREACH; ?>
											</tbody>
											</table>
										</div>
										<!--end row-->										
									</div>
								</div>
							</div>
							<!--end tab-pane-->
							<!--tab_3-->
							<div class="tab-pane" id="tab_3">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet-body">
											<table class="table table-striped table-bordered table-hover" id="support">
											<thead>
											<tr>
												<th width="100%">
													 Ticket Title
												</th>
												<th>
													 Entity Name
												</th>
												<th>
													 Ticket Type
												</th>
												<th>
													 Status
												</th>
												<th>
													 Due Date
												</th>
											</tr>
											</thead>
											<tbody>
											<? FOREACH($support->result() as $row): ?>
											<tr class="odd gradeX">
												<td width="100%" nowrap>
													<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>">
													 <?=$row->title?>
													</a>
												</td>
												<td nowrap>
													 <?=$row->name?>
												</td>
												<td nowrap>
													 <?=$row->entitytickettype?>
												</td>
												<td nowrap>
													 <?=$row->entityticketstatus?>
												</td>
												<td nowrap>
													<?=$row->duedate?>
												</td>
											</tr>
											<? ENDFOREACH; ?>
											</tbody>
											</table>
										</div>
										<!--end row-->										
									</div>
								</div>
							<!--end tab-pane-->
							</div>
						</div>
					<!--END TABS-->
					</div>
				</div>
			<!-- END PAGE CONTENT-->
			</div>
			
			<!--BEGIN MODAL FOR NEW TASK -->
			<div id="advancedsearchmodal" class="modal fade" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Advanced Search</h4>
							</div>
							<div class="modal-body">
								<form method="post" action="<?=site_url("dashboard/advancedsearch") ?>" name="advancedsearchform" id="advancedsearchform">
									<div class="form-group">
										<label class="control-label">Search For</label>
										<input type="text" class="form-control" placeholder="Enter your search phrase here" name="searchstring" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Entity Types</label>
										<select name="entitytype" class="form-control" >
										<option value="" selected>-- no filter --</option>
										<? FOREACH($entitytypes->result() as $row): ?>
											<option value="<?=$row->oetid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Entity Status</label>
										<select name="entitystatus" class="form-control" >
										<option value="" selected>-- no filter --</option>
										<? FOREACH($entitystatus->result() as $row): ?>
											<option value="<?=$row->oesid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Entity Modes</label>
										<select name="entitymode" class="form-control" >
										<option value="" selected>-- no filter --</option>
										<? FOREACH($entitymodes->result() as $row): ?>
											<option value="<?=$row->oemid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Owned By</label>
										<select name="entityowner" class="form-control" >
										<option value="" selected>-- no filter --</option>
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>"><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn green" onclick="document.advancedsearchform.submit()">Submit</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL FOR NEW TASK-->

		</div>
	</div>
	<!-- END CONTENT -->