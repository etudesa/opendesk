	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						<? if ($organization != NULL){ echo $organization->name; } ?> <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a data-toggle="modal" href="#newentitymodal">New Entity</a>
								</li>
								<li>
									<a data-toggle="modal" href="#newtaskmodal">New Task</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="<?=site_url("dashboard")?>">
								Dashboard
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN DASHBOARD STATS FOR USERS-->
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="dashboard-stat yellow">
						<div class="visual">
							<a href="<?=site_url("entities/leads/".$this->session->userdata('uid'))?>"><i class="fa fa-coffee"></i></a>
						</div>
						<div class="details">
							<div class="number">
								 <?=$leads?>
							</div>
							<div class="desc">
								 My Leads
							</div>
						</div>
						<a class="more" href="<?=site_url('entities/add/1')?>">
							 Create new lead <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue">
						<div class="visual">
							<a href="<?=site_url("entities/prospects/".$this->session->userdata('uid'))?>"><i class="fa fa-book"></i></a>
						</div>
						<div class="details">
							<div class="number">
								 <?=$prospects?>
							</div>
							<div class="desc">
								 My Prospects
							</div>
						</div>
						<a class="more" href="<?=site_url('entities/add/2')?>">
							 Create new prospect <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div class="dashboard-stat green">
						<div class="visual">
							<a href="<?=site_url("entities/clients/".$this->session->userdata('uid'))?>"><i class="fa fa-star"></i></a>
						</div>
						<div class="details">
							<div class="number">
								 <?=$clients?>
							</div>
							<div class="desc">
								 My Clients
							</div>
						</div>
						<a class="more" href="<?=site_url('entities/add/3')?>">
							 Create new client <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				
				<div class="col-md-12 col-sm-12">
					<div class="tabbable tabbable-custom">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Reminders
								</a>
							</li>
              <? if($organization->support == 1){ ?>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Tickets
								</a>
							</li>
              <? } ?>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Tasks
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible="0">
									<ul class="feeds">
										<? FOREACH($tickets->result() as $row): ?>
										<? if ($row->reminderdate == date("Y-m-d")){ ?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<span class="label label-sm label-warning">
															 <i class="fa fa-ticket"></i>
														</span>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 <a href="<?=site_url('entities/tickets_edit/'.$row->eid.'/'.$row->etid)?>"><?=$row->description?>&nbsp;<i><?=$row->tickettypedescription?> - <?=$row->name?></i></a>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<span class="label label-sm label-warning">
													 Due Today
												</span>
											</div>
										</li>
										<? $reminders++; ?>
										<? } ?>
										<? endforeach; ?>
										<? FOREACH($tasks->result() as $row): ?>
										<? if ($row->reminderdate == date("Y-m-d")){ ?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<span class="label label-sm label-warning">
															 <i class="fa fa-bell-o"></i>
														</span>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 <a href="<?=site_url('account/tasks_edit/'.$row->utid)?>"><?=$row->title?></a>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<span class="label label-sm label-warning">
													 Due Today
												</span>
											</div>
										</li>
										<? $reminders++; ?>
										<? } ?>
										<? ENDFOREACH; ?>
										<? if ($reminders == 0){ ?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<span class="label label-sm label-success">
															 <i class="fa fa-thumbs-up"></i>
														</span>
													</div>
													<div class="cont-col2">
														<div class="desc">
															Congratulations, you have no reminders for today!
														</div>
													</div>
												</div>
											</div>
										</li>
										<? } ?>
									</ul>
								</div>
							</div>
							<div class="tab-pane" id="tab_2">
								<div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible="0">
									<ul class="feeds">
										<? FOREACH($tickets->result() as $row): ?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<? if ($row->duedate < date("Y-m-d")){ ?>
														<span class="label label-sm label-danger">
															 <i class="fa fa-ticket"></i>
														</span>
														<? } ?>
														<? if ($row->duedate > date("Y-m-d")){ ?>
														<span class="label label-sm label-success">
															 <i class="fa fa-ticket"></i>
														</span>
														<? } ?>
														<? if ($row->duedate == date("Y-m-d")){ ?>
														<span class="label label-sm label-warning">
															 <i class="fa fa-ticket"></i>
														</span>
														<? } ?>
													</div>
													<div class="cont-col2">
														<div class="desc">
															<a href="<?=site_url('entities/tickets_edit/'.$row->eid.'/'.$row->etid)?>"><?=$row->description?>&nbsp;<i><?=$row->tickettypedescription?> - <?=$row->name?></i></a>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<? if ($row->duedate < date("Y-m-d")){ ?>
												<span class="label label-sm label-danger">
													 Overdue
												</span>
												<? } ?>
												<? if ($row->duedate > date("Y-m-d")){ ?>
												<span class="label label-sm label-success">
													 Pending
												</span>
												<? } ?>
												<? if ($row->duedate == date("Y-m-d")){ ?>
												<span class="label label-sm label-warning">
													 Due Today
												</span>
												<? } ?>
											</div>
										</li>
										<? endforeach; ?>
									</ul>
								</div>
							</div>
							<div class="tab-pane" id="tab_3">
								<div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible="0">
									<ul class="feeds">
										<? FOREACH($tasks->result() as $row): ?>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<? if ($row->duedate < date("Y-m-d")){ ?>
														<span class="label label-sm label-danger">
															 <i class="fa fa-bell-o"></i>
														</span>
														<? } ?>
														<? if ($row->duedate > date("Y-m-d")){ ?>
														<span class="label label-sm label-success">
															 <i class="fa fa-bell-o"></i>
														</span>
														<? } ?>
														<? if ($row->duedate == date("Y-m-d")){ ?>
														<span class="label label-sm label-warning">
															 <i class="fa fa-bell-o"></i>
														</span>
														<? } ?>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 <a href="<?=site_url('account/tasks_edit/'.$row->utid)?>"><?=$row->title?></a>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<? if ($row->duedate < date("Y-m-d")){ ?>
												<span class="label label-sm label-danger">
													 Overdue
												</span>
												<? } ?>
												<? if ($row->duedate > date("Y-m-d")){ ?>
												<span class="label label-sm label-success">
													 Pending
												</span>
												<? } ?>
												<? if ($row->duedate == date("Y-m-d")){ ?>
												<span class="label label-sm label-warning">
													 Due Today
												</span>
												<? } ?>
											</div>
										</li>
										<? endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS FOR USERS-->
			
			<!--BEGIN MODAL FOR NEW ENTITY -->
			<div id="newentitymodal" class="modal fade" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Create New Entity</h4>
							</div>
							<div class="modal-body">
								<form method="post" action="<?=site_url("entities/insert") ?>" name="entityform" id="entityform">
									<input type="hidden" name="updated" value="<?=date('Y-m-d h:m:s')?>">
									<input type="hidden" name="profile" value="<?=$this->session->userdata('profile')?>">
									<input type="hidden" name="mode" value="1">
                  <input type="hidden" name="registration" value="">
										<div class="form-group">
											<label class="control-label">What is the name you want to give your new entity?</label>
											<input type="text" class="form-control" placeholder="Enter the full name here..." name="name" value="">
										</div>
										<div class="form-group">
											<label class="control-label">Which status do you want it to start out on?</label>
											<select name="status" class="form-control">
											<? if ($entitystatus->num_rows() != 0) { ?>
												<option value="" disabled selected>-- select one of the following options --</option>
												<? FOREACH($entitystatus->result() as $row): ?>
													<option value="<?=$row->oesid?>"><?=$row->description?></option>
												<? ENDFOREACH; ?>
											<? } else { ?>
												<option value="">-- no entity status available, please specify these under profile options --</option>
											<? } ?>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">Who will be owner of this entity?</label>
											<select name="createdby" class="form-control">
											<? FOREACH($users->result() as $row): ?>
												<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
											<? ENDFOREACH; ?>
											</select>
										</div>
										<div class="form-group">
											<label class="control-label">What type of entity must this be?</label>
											<select name="type" class="form-control">
											<? if ($entitytypes->num_rows() != 0) { ?>
												<option value="" disabled selected>-- select one of the following options --</option>
												<? FOREACH($entitytypes->result() as $row): ?>
													<option value="<?=$row->oetid?>"><?=$row->description?></option>
												<? ENDFOREACH; ?>
											<? } else { ?>
												<option value="">-- no entity types available, please specify these under profile options --</option>
											<? } ?>
											</select>
										</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("entities/add")?>')">Advanced Form</button>
								<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn green" onclick="document.entityform.submit()">Submit</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL FOR NEW ENTITY-->
			
			<!--BEGIN MODAL FOR NEW TASK -->
			<div id="newtaskmodal" class="modal fade" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Create New Task</h4>
							</div>
							<div class="modal-body">
								<form method="post" action="<?=site_url("account/tasks_insert") ?>" name="taskform" id="taskform">
									<input type="hidden" name="status" value="1">
									<div class="form-group">
										<label class="control-label">What is the short title of this task?</label>
										<input type="text" class="form-control" placeholder="Enter short title with less than 10 words" name="title" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Give a full description of this task.</label>
										<input type="text" class="form-control" placeholder="Enter full description" name="description" value="">
									</div>
									<div class="form-group password-strength">
										<label class="control-label">When must the task reminder appear?</label>
										<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="reminderdate" placeholder="Click here to enable date picker"/>
									</div>
									<div class="form-group password-strength">
										<label class="control-label">When is this task due?</label>
										<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="duedate" placeholder="Click here to enable date picker"/>
									</div>
									<div class="form-group">
										<label class="control-label">Who is the owner of this task?</label>
										<select name="uid" class="form-control">
										<? FOREACH($users->result() as $row):?>
											<option value="<?=$row->uid?>"><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/tasks_add")?>')">Advanced Form</button>
								<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn green" onclick="document.taskform.submit()">Submit</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL FOR NEW TASK-->
      
			
			<!--BEGIN MODAL FOR NEW TICKET --
			<div id="newticketymodal" class="modal fade" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Create New Ticket</h4>
						</div>
						<div class="modal-body">
							<form action="#" role="form">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Which entity will this be linked to?</label>
											<select id="select2_entities_modal" class="form-control select2" name="eid">
												<option value=""></option>
												<? FOREACH($entities->result() as $row): ?>
												<option value="<?=$row->eid?>"><?=$row->name?></option>
												<? ENDFOREACH; ?>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">What type of ticket must this be?</label>
											<select id="select2_types_modal" class="form-control select2" name="type">
											<? if ($entitytickettypes->num_rows() != 0) { ?>
												<option value=""></option>
												<? FOREACH($entitytickettypes->result() as $row): ?>
													<option value="<?=$row->oettid?>"><?=$row->description?></option>
												<? ENDFOREACH; ?>
											<? } else { ?>
												<option value="">-- no ticket types available, please specify these under profile options --</option>
											<? } ?>
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Related Product or Service</label>
											<select name="productservice_id" class="bs-select form-control">
												<option value=""></option>
												<optgroup label="products">
													<? if ($products->num_rows() != 0) { ?>
													<? FOREACH($products->result() as $row): ?>
														<option value="<?=$row->epid?>"><?=$row->productcategorydescription?> - <?=$row->identifier?> - <?=$row->producttypedescription?></option>
													<? ENDFOREACH; ?>
													<? } else { ?>
														<option value="">-- no products available --</option>
													<? } ?>
												</optgroup>
												<optgroup label="services">
													<? if ($services->num_rows() != 0) { ?>
													<? FOREACH($services->result() as $row): ?>
														<option value="<?=$row->esid?>"><?=$row->servicetypedescription?> - <?=$row->identifier?></option>
													<? ENDFOREACH; ?>
													<? } else { ?>
														<option value="">-- no services available --</option>
													<? } ?>
												</optgroup>
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Short Title</label>
											<input type="text" class="form-control" placeholder="Enter ticket short title - no more than 30 characters" name="title">
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="control-label">Description</label>
											<textarea class="form-control" placeholder="Enter ticket full description" name="description" rows="2"></textarea>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Reminder Date</label>
											<input class="form-control input-long date-picker" placeholder="Click here to enable date picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="reminderdate"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Due Date</label>
											<input class="form-control input-long date-picker" placeholder="Click here to enable date picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="duedate"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Owned By</label>
											<select name="ownedby" class="form-control">
											<? FOREACH($users->result() as $row): ?>
												<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
											<? ENDFOREACH; ?>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Assigned To</label>
											<select name="assignedto" class="form-control">
											<? FOREACH($users->result() as $row): ?>
												<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
											<? ENDFOREACH; ?>
											</select>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("entities/tickets_add")?>')">Advanced Form</button>
							<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn green" onclick="document.ticketform.submit()">Submit</button>
						</div>
					</div>
				</div>
			</div>
			<!-- END MODAL FOR NEW TICKET -->

			
		</div>
	</div>
	<!-- END CONTENT -->
	
	<script>
		$( document ).ready(function() {
			$("#person").change(function(){
				var package = $(this).val();
				$.ajax({
				   type:'POST',
				   data:{package:package},
				   url: '<?=site_url("entities/get_person/")?>'+'/'+package,
				   dataType:"json",
				   success:function(data){

				   }
				})
			});
		});
	</script>