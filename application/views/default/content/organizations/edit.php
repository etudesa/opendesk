	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("entities/manage")?>">
								Manage
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("organizations/update") ?>">
                <input type="hidden" name="updated" value="<?=$today?>">
								<input type="hidden" name="orgid" value="<?=$this->session->userdata('organization')?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control" placeholder="Enter full name" name="name" value="<?=$organization->name?>">
									</div>
									<div class="form-group">
										<label class="control-label">Registration Number</label>
										<input type="text" class="form-control" placeholder="Enter ID/IT/CIPC number" name="registration" value="<?=$organization->registration?>">
									</div>
									<div class="form-group">
										<label class="control-label">Created By</label>
										<select name="createdby" class="form-control" disabled>
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $organization->createdby){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Type</label>
										<select name="type" class="form-control">
										<? FOREACH($entitytypes->result() as $row): ?>
											<option value="<?=$row->oetid?>" <? if ($row->oetid == $organization->type){ echo "selected";}?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Groups</label>
										<input type="hidden" id="groups" class="form-control select2" name="groups" value="<?=$organization->groups?>">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	
	<script>
		$( document ).ready(function() {
	        $("#groups").select2({
	            tags: [<? foreach ($groups->result() as $row){ echo "{id:\"".$row->egid."\", text:\"".$row->name."\"},"; } ?>]
	        });
		});
	</script>