	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage")?>">
								Organizations
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage#tab_3")?>">
								Options
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit <?=$subtitle?>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" action="<?=site_url("organizations/".$functionname."_update/single") ?>">
							<input type="hidden" name="<?=$functionid?>" value="<?=$row[$functionid]?>">
									<div class="form-body">
										<div class="form-group">
											<label class="control-label">Description</label>
											<input type="text" class="form-control" placeholder="Enter description" name="description" value="<?=$row['description']?>">
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- END CONTENT -->