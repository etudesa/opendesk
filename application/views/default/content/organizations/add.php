	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN MESSAGE SECTION-->
			<? if ($this->session->flashdata('message') != ""){ ?>
			<div class="alert alert-danger display">
				<button class="close" data-close="alert"></button>
				<span>
					<?=$this->session->flashdata('message');?>
				</span>
			</div>
			<? } ?>
			<!-- END MESSAGE SECTION-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Add Organization
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/insert") ?>">
								<div class="form-body">					
									<input type="hidden" name="updated" value="<?=$today?>">
									<input type="hidden" name="mode" value="1">
									<input type="hidden" name="groups" value="">
									<div class="form-group">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control" placeholder="Enter the full name of the organization" name="name" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Registration Number</label>
										<input type="text" class="form-control" placeholder="Enter ID/IT/CIPC number" name="registration" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Status</label>
										<select name="status" class="form-control">
											<option value="" disabled selected>-- specify the organization status --</option>
											<? FOREACH($organizationstatus->result() as $row): ?>
												<option value="<?=$row->oosid?>"><?=$row->description?></option>
											<? ENDFOREACH; ?>
										</select>
									</div>
  									<div class="form-group">
										<label class="control-label">Type</label>
										<select name="status" class="form-control">
											<option value="" disabled selected>-- specify the organization type --</option>
											<? FOREACH($organizationtypes->result() as $row): ?>
												<option value="<?=$row->ootid?>"><?=$row->description?></option>
											<? ENDFOREACH; ?>
										</select>
									</div>
									<h4 class="form-section">Main User Details</h4>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Username</label>
												<input type="text" class="form-control" placeholder="Enter the username..." name="username" id="username" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Full Name</label>
												<input type="text" class="form-control" placeholder="Enter the full name..." name="fullname" id="fullname" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Access Level</label>
												<input type="text" class="form-control" placeholder="Enter mobile number" name="mobile" id="mobile" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Email Address</label>
												<input type="text" class="form-control" placeholder="Enter email address" name="email" id="email" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	
	<script>
		$( document ).ready(function() {
			$("#person").change(function(){
				var package = $(this).val();
				$.ajax({
				   type:'POST',
				   data:{package:package},
				   url: '<?=site_url("entities/get_person/")?>'+'/'+package,
				   dataType:"json",
				   success:function(data){
				   	   $('#pid').val(data['pid']);
				   	   $('#firstname').val(data['firstname']);
				       $('#lastname').val(data['lastname']);
				       $('#preferredname').val(data['preferredname']);
				       $('#idnumber').val(data['idnumber']);
				       $('#dateofbirth').val(data['dateofbirth']);
				       $('#physicaladdress').val(data['physicaladdress']);
				       $('#postaladdress').val(data['postaladdress']);
				       $('#homephone').val(data['homephone']);
				       $('#officephone').val(data['officephone']);
				       $('#mobile').val(data['mobile']);
				       $('#email').val(data['email']);
				   }
				})
			});
		});
	</script>