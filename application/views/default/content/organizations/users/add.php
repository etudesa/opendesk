	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url('organizations/manage/'.$orgid)?>">
								Organizations
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url('organizations/manage/'.$orgid.'#tab_1_2')?>">
								Users
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
      
			<!-- BEGIN MESSAGE SECTION-->
			<? if ($this->session->flashdata('message') != ""){ ?>
			<div class="alert alert-danger display">
				<button class="close" data-close="alert"></button>
				<span>
					<?=$this->session->flashdata('message');?>
				</span>
			</div>
			<? } ?>
			<!-- END MESSAGE SECTION-->
      
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Add User
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("organizations/users_insert") ?>">
							<input type="hidden" name="updated" value="<?=date('Y-m-d');?>">
							<input type="hidden" name="organization" value="<?=$orgid?>">
							<input type="hidden" name="status" value="1">
								<div class="form-body">					
									<div class="form-group">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control" placeholder="Enter full name" name="fullname" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Username</label>
										<input type="text" class="form-control" placeholder="Enter username" name="username" value="">
									</div>
									<div class="form-group password-strength">
										<label class="control-label">Password</label>
										<input type="text" class="form-control" name="password" placeholder="Type a password" id="password_strength">
									</div>
									<div class="form-group">
										<label class="control-label">Access Level</label>
										<select name="accesslevel" class="form-control">
										<? FOREACH($accesslevels->result() as $row):?>
											<option value="<?=$row->oualid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Email Address</label>
										<input type="text" class="form-control" placeholder="Enter email" name="email" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Imap Username</label>
										<input type="text" class="form-control" placeholder="Enter imap username" name="imap_username" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Imap Password</label>
										<input type="text" class="form-control" placeholder="Enter imap password" name="imap_password" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Username</label>
										<input type="text" class="form-control" placeholder="Enter seafile username" name="seafile_username" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Password</label>
										<input type="text" class="form-control" placeholder="Enter seafile password" name="seafile_password" value="">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
		</div>
	</div>
	<!-- END CONTENT -->