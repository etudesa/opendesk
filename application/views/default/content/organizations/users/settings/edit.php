	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage/".$user->uid)?>">
								Organization
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/users_view/".$user->uid)?>">
								User
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/user_settings_edit/".$user->uid."#tab_2")?>">
								Settings
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Settings
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("organizations/users_settings_update") ?>">
							<input type="hidden" name="uid" value="<?=$user->uid?>">
								<div class="form-body">
                  <div class="form-group">
										<label class="control-label">Email Address</label>
										<input type="text" class="form-control" placeholder="Enter email address" name="email" value="<?=$user->email?>">
									</div>
									<div class="form-group">
										<label class="control-label">IMAP Username</label>
										<input type="text" class="form-control" placeholder="Enter imap username" name="imap_username" value="<?=$user->imap_username?>">
									</div>
									<div class="form-group">
										<label class="control-label">IMAP Password</label>
										<input type="text" class="form-control" placeholder="Enter imap password" name="imap_password" value="<?=$user->imap_password?>">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Username</label>
										<input type="text" class="form-control" placeholder="Enter seafile username" name="seafile_username" value="<?=$user->seafile_username?>">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Password</label>
										<input type="text" class="form-control" placeholder="Enter seafile password" name="seafile_password" value="<?=$user->seafile_password?>">
									</div>
								</div>
							<div class="form-actions">
								<button type="submit" class="btn green">Submit</button>
								<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
							</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->