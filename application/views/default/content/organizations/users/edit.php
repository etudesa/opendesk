	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage/".$user->organization)?>">
								Organizations
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage/".$user->organization."#tab_1_2")?>">
								Users
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
      
 			<!-- BEGIN MESSAGE SECTION-->
			<? if ($this->session->flashdata('message') != ""){ ?>
			<div class="alert alert-danger display">
				<button class="close" data-close="alert"></button>
				<span>
					<?=$this->session->flashdata('message');?>
				</span>
			</div>
			<? } ?>
			<!-- END MESSAGE SECTION-->
      
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit User
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("organizations/users_update") ?>">
								<input type="hidden" name="uid" value="<?=$user->uid?>">
								<input type="hidden" name="organization" value="<?=$user->organization?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control" placeholder="Enter full name" name="fullname" value="<?=$user->fullname?>">
									</div>
									<div class="form-group">
										<label class="control-label">Username</label>
										<input type="text" class="form-control" placeholder="Enter username" name="username" value="<?=$user->username?>">
									</div>
									<div class="form-group">
										<label class="control-label">Access Level</label>
										<select name="accesslevel" class="form-control">
										<? FOREACH($accesslevels->result() as $row):?>
											<option value="<?=$row->oualid?>" <? if ($row->oualid == $user->accesslevel){ echo "selected"; } ?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Email Address</label>
										<input type="text" class="form-control" placeholder="Enter email" name="email" value="<?=$user->email?>">
									</div>
									<div class="form-group">
										<label class="control-label">Imap Username</label>
										<input type="text" class="form-control" placeholder="Enter imap username" name="imap_username" value="<?=$user->imap_username?>">
									</div>
									<div class="form-group">
										<label class="control-label">Imap Password</label>
										<input type="text" class="form-control" placeholder="Enter imap password" name="imap_password" value="<?=$user->imap_password?>">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Username</label>
										<input type="text" class="form-control" placeholder="Enter seafile username" name="seafile_username" value="<?=$user->seafile_username?>">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Password</label>
										<input type="text" class="form-control" placeholder="Enter seafile password" name="seafile_password" value="<?=$user->seafile_password?>">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
									<a class="btn red" data-toggle="modal" href="#resetusermodal">Reset Password</a>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			<!--BEGIN MODAL FOR USER PASSWORD RESET -->
				<div class="modal fade" id="resetusermodal" tabindex="-1" role="resetusermodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Password Reset</h4>
							</div>
							<div class="modal-body">
								 You are about to reset the password for <strong><?=$user->username?></strong>. This will generate and send an email containing the credentials. Are you sure you want to do this?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("organizations/users_update/".$user->uid) ?>')">Yes</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			<!-- END MODAL -->
			
		</div>
	</div>
	<!-- END CONTENT -->