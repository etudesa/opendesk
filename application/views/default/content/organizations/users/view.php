	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Profile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/add")?>">
												New Entity
											</a>
										</li>
										<li>
											<a href="<?=site_url("organizations/users_tasks_add/".$uid)?>">
												New Task
											</a>
										</li>
									</ul>
								</li>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Settings &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("organizations/users_settings_edit/".$uid)?>">
												Edit Email Settings
											</a>
										</li>
										<li>
											<a href="<?=site_url("organizations/users_settings_edit/".$uid)?>">
												Edit Seafile Settings
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage")?>">
								Organization
							</a>
              <i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/users_view")?>">
								Users
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Profile
								</a>
							</li>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Settings
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1">
								<div class="row">
									<div class="col-md-2">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_1_1">
													<i class="fa fa-eye"></i> Overview
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_1_2">
													<i class="fa fa-map-marker"></i> My Entities
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_1_3">
													<i class="fa fa-map-marker"></i> My Tasks
												</a>
											</li>
                      <? if ($organization->support == 1){ ?>
											<li>
												<a data-toggle="tab" href="#tab_1_4">
													<i class="fa fa-map-marker"></i> My Tickets
												</a>
											</li>
                      <? } ?>
											<? if (($organization->imap_username != "") && ($organization->imap_password != "")){ ?>
											<li>
												<a data-toggle="tab" href="#tab_1_5">
													<i class="fa fa-map-marker"></i> My Emails
												</a>
											</li>
											<? } ?>
  										<? if (($organization->seafile_username == 1) && ($organization->seafile_password == 1)){ ?>
											<li>
												<a data-toggle="tab" href="#tab_1_6">
													<i class="fa fa-map-marker"></i> My Files
												</a>
											</li>
											<? } ?>
										</ul>
									</div>
									<div class="col-md-10">
										<div class="tab-content">
											<!--tab_1_1--overview---->
											<div class="tab-pane active" id="tab_1_1">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Profile Overview
														</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td width="30%">
														Full Name
													</td>
													<td>
														<?=$user->fullname?>
													</td>
												</tr>
												<tr>
													<td>
														Username
													</td>
													<td>
														<?=$user->username?>
													</td>
												</tr>
												<tr>
													<td>
														Password
													</td>
													<td>
														<a data-toggle="modal" href="#resetusermodal">reset password</a>
													</td>
												</tr>
												<tr>
													<td>
														User Access Level
													</td>
													<td>
														<? FOREACH($accesslevels->result() as $row):?>
															<? if ($row->oualid == $user->accesslevel){ echo $row->description; } ?>
														<? ENDFOREACH; ?>
													</td>
												</tr>
												</tbody>
												</table>
											</div>
											<!--tab_1_2--entities---->
											<div class="tab-pane" id="tab_1_2">
												<div class="row profile-account">
													<div class="col-md-12">
														<div class="portlet-body">
															<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
															<tr>
																<th>
																	 Name
																</th>
																<th>
																	 Type
																</th>
																<th>
																	 Status
																</th>
																<th>
																	 Mode
																</th>
																<th>
																	 Actions
																</th>
															</tr>
															</thead>
															<tbody>
                              <? if ($entities->num_rows() > 0){ ?>
                                <? FOREACH($entities->result() as $row): ?>
                                <tr class="odd gradeX">
                                  <td>
                                     <a href="<?=site_url("entities/view/".$row->eid)?>"><?=$row->name?></a>
                                  </td>
                                  <td>
                                     <?=$row->entitytype?>
                                  </td>
                                  <td>
                                     <?=$row->entitystatus?>
                                  </td>
                                  <td>
                                    <?=$row->entitymode?>
                                  </td>
                                  <td>
                                    <a href="<?=site_url("entities/edit/".$row->eid)?>" class="btn default btn-xs blue-stripe">
                                       Edit
                                    </a>
                                  </td>
                                </tr>
                                <? ENDFOREACH; ?>
                              <? }else{ ?>
                              <tr>
                                <td colspan="5">No Entities found.</td>
                              </tr>
                              <? } ?>
															</tbody>
															</table>
														</div>
													</div>
													<!--end col-md-12-->
												</div>
											</div>
											<!--tab_1_3--tasks---->
											<div class="tab-pane" id="tab_1_3">
												<div class="row profile-account">
													<div class="col-md-12">
														<div class="portlet-body">
															<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
															<tr>
																<th>
																	Title
																</th>
																<th>
																	Due Date
																</th>
																<th>
																	Status
																</th>
																<th>
																	Actions
																</th>
															</tr>
															</thead>
															<tbody>
                                <? if ($tasks->num_rows() > 0){ ?>
                                  <? FOREACH($tasks->result() as $row): ?>
                                  <tr class="odd gradeX">
                                    <td>
                                      <a href="<?=site_url("organizations/users_tasks_edit/".$row->utid)?>"> <?=$row->title?></a>
                                    </td>
                                    <td>
                                       <?=$row->duedate?>
                                    </td>
                                    <td>
                                       <?=$row->usertaskstatus?>
                                    </td>
                                    <td>
                                      <a href="<?=site_url("organizations/users_tasks_edit/".$row->utid)?>" class="btn default btn-xs blue-stripe">
                                         Edit
                                      </a>
                                    </td>
                                  </tr>
                                  <? ENDFOREACH; ?>
                                <? }else{ ?>
                                <tr>
                                  <td colspan="4">No Tasks found.</td>
                                </tr>
                                <? } ?>
															</tbody>
															</table>
														</div>
													</div>
													<!--end col-md-12-->
												</div>
											</div>
											<!--tab_1_4---tickets-->
											<div class="tab-pane" id="tab_1_4">
												<div class="row profile-account">
													<div class="col-md-12">
														<div class="portlet-body">
															<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
															<tr>
																<th>
																	 Title
																</th>
																<th>
																	Entity
																</th>
																<th>
																	Type
																</th>
																<th>
																	Due Date
																</th>
																<th>
																	Status
																</th>
																<th>
																	 Actions
																</th>
															</tr>
															</thead>
															<tbody>
                                 <? if ($tickets->num_rows() > 0){ ?>
                                    <? FOREACH($tickets->result() as $row): ?>
                                    <tr class="odd gradeX">
                                      <td nowrap>
                                         <a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>"><?=$row->title?></a>
                                      </td>
                                      <td nowrap>
                                         <a href="<?=site_url("entities/view/".$row->eid)?>"><?=$row->entityname?></a>
                                      </td>
                                      <td>
                                         <?=$row->tickettype?>
                                      </td>
                                      <td>
                                         <?=$row->duedate?>
                                      </td>
                                      <td>
                                         <?=$row->ticketstatus?>
                                      </td>
                                      <td>
                                        <a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>" class="btn default btn-xs blue-stripe">
                                           Edit
                                        </a>
                                      </td>
                                    </tr>
                                    <? ENDFOREACH; ?>
                                  <? }else{ ?>
                                  <tr>
                                    <td colspan="6">No Tickets found.</td>
                                  </tr>
                                  <? } ?>
															</tbody>
															</table>
														</div>
													</div>
													<!--end col-md-12-->
												</div>
											</div>
											<!--tab_1_5--emails---->
											<div class="tab-pane" id="tab_1_5">
												<div class="row profile-account">
													<div class="col-md-12">
														<div class="portlet-body">
															<table class="table table-striped table-bordered table-advance table-hover">
																<thead>
																<tr>
																	<th>
																		 <i class="fa fa-star-o"></i>
																	</th>
																	<th>
																		 From
																	</th>
																	<th>
																		 Subject
																	</th>
																	<th>
																		Attachments
																	</th>
																	<th>
																		Date
																	</th>
																</tr>
																</thead>
																<tbody>
																<? foreach ($messages as $message){ ?>
																<tr <? if($message->Unseen == "U"){?> class="unread" <? } ?> data-messageid="1">
																	<td class="inbox-small-cells">
																		<i class="fa fa-star-o"></i>
																	</td>
																	<td class="view-message hidden-xs" nowrap>
																		 <a href="<?=site_url("account/emails_view/".trim($message->Msgno)."/".trim($folder))?>"><?=$message->fromaddress ;?></a>
																	</td>
																	<td class="view-message ">
																		 <? if (isset($message->subject)){ echo substr($message->subject,0,75)."..."; }else{ echo "no subject";}?>
																	</td>
																	<td class="view-message inbox-small-cells">
																		<!--<i class="fa fa-paperclip"></i>-->
																	</td>
																	<td class="view-message text-right" nowrap>
																		 <?=date("j M Y", strtotime($message->date));?>
																	</td>
																</tr>
																<? } ?>
																</tbody>
															</table>
														</div>
													</div>
													<!--end col-md-12-->
												</div>
											</div>
  										<!--tab_1_6--files---->
											<div class="tab-pane" id="tab_1_6">
												<div class="row profile-account">
													<div class="col-md-12">
														<div class="portlet-body">
															<table class="table table-striped table-bordered table-advance table-hover">
																<thead>
																<tr>
																	<th>
																		 Name
																	</th>
																	<th>
																		 Type
																	</th>
																	<th>
																		Owner
																	</th>
																</tr>
																</thead>
																<tbody>
                                  <? //if ($folders->num_rows() > 0){ ?>  
                                    <? FOREACH($folders as $row): ?>  
                                    <tr>
                                      <td><?=$row->name?></td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                    <? ENDFOREACH; ?>
                                  <? //} ?>
																</tbody>
															</table>
														</div>
													</div>
													<!--end col-md-12-->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab_2">
								<div class="row">
									<div class="col-md-2">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_2_1">
													<i class="fa fa-eye"></i> Overview
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_2">
													<i class="fa fa-envelope-o"></i> IMAP Account
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_3">
													<i class="fa fa-files-o"></i> Seafile Account
												</a>
											</li>
										</ul>
									</div>
									<div class="col-md-10">
										<div class="tab-content">
											<!--tab_2_1--overview---->
											<div class="tab-pane active" id="tab_2_1">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Profile Overview
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															IMAP Account
														</td>
														<td>
															<? if (($organization->imap_username != "") && ($organization->imap_password != "")){ echo "Active";}else{ echo "Incomplete";}?>
														</td>
													</tr>
													<tr>
														<td>
															Seafile Account
														</td>
														<td>
															<? if (($organization->seafile_username != "") && ($organization->seafile_password != "")){ echo "Active";}else{ echo "Incomplete";}?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!--tab_2_2--email---->
											<div class="tab-pane" id="tab_2_2">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															IMAP Account
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Email Address
														</td>
														<td>
															<?=$organization->email?>
														</td>
													</tr>
													<tr>
														<td>
															Username
														</td>
														<td>
															<?=$organization->imap_username?>
														</td>
													</tr>
													<tr>
														<td>
															Password
														</td>
														<td>
															<?=$organization->imap_password?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!--tab_2_3--seafile---->
											<div class="tab-pane" id="tab_2_3">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Seafile Account
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Username
														</td>
														<td>
															<?=$organization->seafile_username?>
														</td>
													</tr>
													<tr>
														<td>
															Password
														</td>
														<td>
															<?=$organization->seafile_password?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- END PAGE CONTENT-->			
        
			<!--BEGIN MODAL FOR USER PASSWORD RESET -->
				<div class="modal fade" id="resetusermodal" tabindex="-1" role="resetusermodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Password Reset</h4>
							</div>
							<div class="modal-body">
								 You are about to reset the password for <strong><?=$user->username?></strong>. This will generate and send an email containing the credentials. Are you sure you want to do this?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("organizations/users_update/".$user->uid) ?>')">Yes</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			<!-- END MODAL -->
        
		</div>
	</div>
	<!-- END CONTENT -->