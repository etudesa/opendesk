	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage")?>">
								Organizations
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage#tab_4")?>">
								Options
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit <?=$subtitle?>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" action="<?=site_url("organizations/".$functionname."_update/1") ?>">
							<input type="hidden" name="<?=$functionid?>" value="<?=$row[$functionid]?>">
									<div class="form-body">
										<div class="form-group">
											<label class="control-label">Workflow Name</label>
											<input type="text" class="form-control" name="description" disabled value="<?=$row['description']?>">
										</div>
										<div class="form-group">
											<label class="control-label">Stage 1</label>
											<input type="text" class="form-control" placeholder="Enter the requirement for stage 1" name="field1_description" value="<?=$row['field1_description']?>">
										</div>
										<div class="form-group">
											<label class="control-label">Stage 2</label>
											<input type="text" class="form-control" placeholder="Enter the requirement for stage 2" name="field2_description" value="<?=$row['field2_description']?>">
										</div>
										<div class="form-group">
											<label class="control-label">Stage 3</label>
											<input type="text" class="form-control" placeholder="Enter the requirement for stage 3" name="field3_description" value="<?=$row['field3_description']?>">
										</div>
										<div class="form-group">
											<label class="control-label">Stage 4</label>
											<input type="text" class="form-control" placeholder="Enter the requirement for stage 4" name="field4_description" value="<?=$row['field4_description']?>">
										</div>
										<div class="form-group">
											<label class="control-label">Stage 5</label>
											<input type="text" class="form-control" placeholder="Enter the requirement for stage 5" name="field5_description" value="<?=$row['field5_description']?>">
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<!-- END CONTENT -->