	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage/".$organization->orgid)?>">
								Organizations
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage/".$organization->orgid."#tab_2")?>">
								Settings
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Settings
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("organizations/settings_update") ?>">
							<input type="hidden" name="organization" value="<?=$organization->orgid?>">
							<input type="hidden" name="section" value="<?=$section?>">
							<? if ($section == 1){ ?>
								<div class="form-body">
                  <div class="form-group">
										<label class="control-label">API ID</label>
										<input type="text" class="form-control" placeholder="Enter api id" name="sms_apiid" value="<?=$organization->sms_apiid?>">
									</div>
									<div class="form-group">
										<label class="control-label">Username</label>
										<input type="text" class="form-control" placeholder="Enter username" name="sms_username" value="<?=$organization->sms_username?>">
									</div>
									<div class="form-group">
										<label class="control-label">Password</label>
										<input type="text" class="form-control" placeholder="Enter password" name="sms_password" value="<?=$organization->sms_password?>">
									</div>
								</div>
							<? }elseif($section == 2){ ?>
								<div class="form-body">
                  <div class="form-group">
										<label class="control-label">From Address</label>
										<input type="text" class="form-control" placeholder="Enter from address" name="imap_from" value="<?=$organization->imap_from?>">
									</div>
									<div class="form-group">
										<label class="control-label">SMTP Server</label>
										<input type="text" class="form-control" placeholder="Enter smtp server" name="imap_smtp" value="<?=$organization->imap_smtp?>">
									</div>
									<div class="form-group">
										<label class="control-label">Inbox Folder</label>
										<input type="text" class="form-control" placeholder="Enter inbox folder" name="imap_folder" value="<?=$organization->imap_folder?>">
									</div>
									<div class="form-group">
										<label class="control-label">Port</label>
										<input type="text" class="form-control" placeholder="Enter port" name="imap_port" value="<?=$organization->imap_port?>">
									</div>
									<div class="form-group">
										<label class="control-label">Flags</label>
										<input type="text" class="form-control" placeholder="Enter flags" name="imap_flags" value="<?=$organization->imap_flags?>">
									</div>
								</div>
  							<? }elseif($section == 3){ ?>
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Seafile Token</label>
										<input type="text" class="form-control" placeholder="Enter seafile token" name="seafile_token" value="<?=$organization->seafile_token?>">
									</div>
									<div class="form-group">
										<label class="control-label">Seafile Library</label>
										<input type="text" class="form-control" placeholder="Enter seafile library ID" name="seafile_library" value="<?=$organization->seafile_library?>">
									</div>
								</div>
							<? }elseif($section == 4){ ?>
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Correspondence Module</label>
										<select name="correspondence" class="form-control">
											<option value="1" <? if ($organization->correspondence == 1){ echo "selected";} ?> >Enabled</option>
											<option value="0" <? if ($organization->correspondence == 0){ echo "selected";} ?> >Disabled</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Support Module</label>
										<select name="support" class="form-control">
											<option value="1" <? if ($organization->support == 1){ echo "selected";} ?> >Enabled</option>
											<option value="0" <? if ($organization->support == 0){ echo "selected";} ?> >Disabled</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Governance Module</label>
										<select name="governance" class="form-control">
											<option value="1" <? if ($organization->governance == 1){ echo "selected";} ?> >Enabled</option>
											<option value="0" <? if ($organization->governance == 0){ echo "selected";} ?> >Disabled</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Fiduciary Module</label>
										<select name="fiduciary" class="form-control">
											<option value="1" <? if ($organization->fiduciary == 1){ echo "selected";} ?> >Enabled</option>
											<option value="0" <? if ($organization->fiduciary == 0){ echo "selected";} ?> >Disabled</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Files Module</label>
										<select name="files" class="form-control">
											<option value="1" <? if ($organization->files == 1){ echo "selected";} ?> >Enabled</option>
											<option value="0" <? if ($organization->files == 0){ echo "selected";} ?> >Disabled</option>
										</select>
									</div>
								</div>
							<? } ?>
							<div class="form-actions">
								<button type="submit" class="btn green">Submit</button>
								<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
                <? if (($section == 3) && ($user->seafile_username != "") && ($user->seafile_password != "")) {?> <button type="button" class="btn blue" onclick="window.open('<?=site_url("organizations/check_seafile")?>')">Check Service</button> <? }else{ ?> <? } ?>
							</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->