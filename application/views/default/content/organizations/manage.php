	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Profile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("organizations/users_add/".$orgid)?>">
												Add User
											</a>
										</li>
										<li>
											<a href="<?=site_url("organizations/groups_add/".$orgid)?>">
												Add Group
											</a>
										</li>
									</ul>
								</li>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Settings &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li><a href="<?=site_url("organizations/settings_edit/1/".$orgid)?>">Edit SMS Service</a></li>
										<li><a href="<?=site_url("organizations/settings_edit/2/".$orgid)?>">Edit Email Service</a></li>
                    <li><a href="<?=site_url("organizations/settings_edit/3/".$orgid)?>">Edit File Service</a></li>
									</ul>
								</li>
								<? if ($this->session->userdata('accesslevel') <= 2){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Options &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<? if ($this->session->userdata('accesslevel') == 1){ ?><li><a href="<?=site_url("organizations/options_productcategories_add/".$orgid) ?>">Add Product Category &nbsp;&nbsp;</a></li><? } ?>
										<li><a href="<?=site_url("organizations/options_producttypes_add/".$orgid) ?>">Add Product Type &nbsp;&nbsp;</a></li>
										<? if ($this->session->userdata('accesslevel') == 1){ ?><li><a href="<?=site_url("organizations/options_productproviders_add/".$orgid) ?>">Add Product Provider &nbsp;&nbsp;</a></li><? } ?>
										<li><a href="<?=site_url("organizations/options_tickettypes_add/".$orgid) ?>">Add Ticket Type &nbsp;&nbsp;</a></li>
										<li><a href="<?=site_url("organizations/options_servicetypes_add/".$orgid) ?>">Add Service Type &nbsp;&nbsp;</a></li>
										<? if ($this->session->userdata('accesslevel') == 1){ ?><li><a href="<?=site_url("organizations/options_entitytypes_add/".$orgid) ?>">Add Entity Type &nbsp;&nbsp;</a></li><? } ?>
									</ul>
								</li>
								<? } ?>
								<li class="divider"></li>
								<li>
									<a href="<?=site_url("organizations/edit/".$orgid)?>">
										Edit Profile
									</a>
								</li>
								<? if ($this->session->userdata('accesslevel') == 1){ ?>
								<li>
									<a href="<?=site_url("organizations/delete/".$orgid)?>">
										Delete Profile
									</a>
								</li>
								<? } ?>
							</ul>
						</li>
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("organizations/manage")?>">
								Organizations
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
      
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_1" data-toggle="tab">
									 Profile
								</a>
							</li>
							<li>
								<a href="#tab_2" data-toggle="tab">
									 Settings
								</a>
							</li>
							<li>
								<a href="#tab_3" data-toggle="tab">
									 Options
								</a>
							</li>
						</ul>
						<div class="tab-content">
							<!--tab_1-->
							<div class="tab-pane active" id="tab_1">
								<div class="row profile-account">
										<div class="col-md-2">
											<ul class="ver-inline-menu tabbable margin-bottom-10">
												<li class="active">
													<a data-toggle="tab" href="#tab_1_1">
														<i class="fa fa-eye"></i> Overview
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_1_2">
														<i class="fa fa-map-marker"></i> Users
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_1_3">
														<i class="fa fa-shopping-cart"></i> Groups
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_1_4">
														<i class="fa fa-bell"></i> Reminders
													</a>
												</li>
											</ul>
										</div>
										<div class="col-md-10">
											<div class="tab-content">
												<!--tab_1_1 -->
												<div id="tab_1_1" class="tab-pane active">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th colspan="2">
																Profile Overview
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>
																Organization Name
															</td>
															<td>
																<?=$organization->name?>
															</td>
														</tr>
														<tr>
															<td>
																Users
															</td>
															<td>
																<?=$userscount?>
															</td>
														</tr>
														<tr>
															<td>
																Groups
															</td>
															<td>
																<?=$groupscount?>
															</td>
														</tr>
													</tbody>
													</table>
												</div>
												<!--tab_1_2 -->
												<div class="tab-pane" id="tab_1_2">
													<? if ($users->num_rows() == 0){ ?>
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th>
																		No Users
																	</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>There are currently no users found for this entity, please <a href="<?=site_url("organizations/users_add/".$orgid)?>">click here</a> to add a new user.</td>
																</tr>
															</tbody>
														</table>
													<? }else{ ?>
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
														<tr>
															<th>
																Full Name
															</th>
															<th>
																Username
															</th>
															<th>
																Accesslevel
															</th>
															<th>
																Status
															</th>
															<th>
																Last Updated
															</th>
															<th>
																Actions
															</th>
														</tr>
														</thead>
														<tbody>
														<? FOREACH($users->result() as $row): ?>
														<tr>
															<td>
																<?=$row->fullname?>
															</td>
															<td>
																<?=$row->username?>
															</td>
															<td>
																<?=$row->useraccesslevel?>
															</td>
															<td>
																<?=$row->userstatus?>
															</td>
															<td>
																<?=$row->updated?>
															</td>
															<td>
																<? if (($this->session->userdata('accesslevel') == 1) || ($row->uid == $this->session->userdata('uid'))) {?><a href="<?=site_url("organizations/users_edit/".$row->uid)?>" class="btn default btn-xs blue-stripe">Edit</a><? } ?>
															</td>
														</tr>
														<? ENDFOREACH; ?>
														</tbody>
														</table>
													<? } ?>
												</div>
												<!--tab_1_3-->
												<div class="tab-pane" id="tab_1_3">
													<div class="row">
														<div class="col-md-12">
															<div class="row">
																<div class="col-md-12 profile-info">
																<? if ($groups->num_rows() == 0){ ?>
																	<table class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																			<tr>
																				<th>
																					No Groups
																				</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>There are currently no groups found for this entity, please <a href="<?=site_url("organizations/groups_add/".$orgid)?>">click here</a> to add a new group.</td>
																			</tr>
																		</tbody>
																	</table>
																<? }else{ ?>
																	<table class="table table-striped table-bordered table-advance table-hover">
																	<thead>
																	<tr>
																		<th>
																			Group Name
																		</th>
																		<th>
																			Subject
																		</th>
																		<th>
																			Status
																		</th>
																		<th>
																			Last Updated
																		</th>
																		<th>
																			Actions
																		</th>
																	</tr>
																	</thead>
																	<tbody>
																	<? FOREACH($groups->result() as $row): ?>
																	<tr>
																		<td>
																			<?=$row->name?>
																		</td>
																		<td>
																			<?=$row->subject?>
																		</td>
																		<td>
																			<? if ($row->status == 1){ echo "Active"; }else{ echo "Inactive";} ?>
																		</td>
																		<td>
																			<?=$row->updated?>
																		</td>
																		<td>
																			<a href="<?=site_url("organizations/groups_edit/".$row->egid)?>" class="btn default btn-xs blue-stripe">
					                                        					 Edit
					                                    					</a>
																		</td>
																	</tr>
																	<? ENDFOREACH; ?>
																	</tbody>
																	</table>
																<? } ?>
																</div>
																<!--end col-md-12-->
															</div>
															<!--end row-->
					
														</div>
													</div>
												</div>
												<!--tab_1_3-->
												<div class="tab-pane" id="tab_1_4">
													<div class="row">
														<div class="col-md-12">
															<div class="row">
																<div class="col-md-12 profile-info">
																	<table class="table table-striped table-bordered table-advance table-hover">
																	<thead>
																	<tr>
																		<th>
																			Notice Name
																		</th>
																		<th>
																			Description
																		</th>
																		<th>
																			Status
																		</th>
																		<th>
																			Actions
																		</th>
																	</tr>
																	</thead>
																	<tbody>
																	<tr>
																		<td>
																			Daily Ticket Reminder
																		</td>
																		<td>
																			Email Reminder sent daily to remind the user of any pending tickets
																		</td>
																		<td>
																			Active
																		</td>
																		<td>
																			<a href="<?=site_url("cron/email_dailytickets/".$orgid."/0")?>" target="new" class="btn default btn-xs green-stripe">
					                                        					 View
					                                    					</a>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			Daily Task Reminder
																		</td>
																		<td>
																			Email Reminder sent daily to remind the user of any pending tasks
																		</td>
																		<td>
																			Active
																		</td>
																		<td>
																			<a href="<?=site_url("cron/email_dailytasks/0")?>" target="new" class="btn default btn-xs green-stripe">
					                                        					 View
					                                    					</a>
																		</td>
																	</tr>
																	</tbody>
																	</table>
																</div>
																<!--end col-md-12-->
															</div>
															<!--end row-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>
							<!--tab_2-->
							<div class="tab-pane" id="tab_2">
								<div class="row profile-account">
									<div class="col-md-2">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_2_1">
													<i class="fa fa-eye"></i> Overview
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_2">
													<i class="fa fa-phone"></i> SMS Service
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_2_3">
													<i class="fa fa-envelope"></i> Email Service
												</a>
											</li>
  											<li>
												<a data-toggle="tab" href="#tab_2_4">
													<i class="fa fa-file"></i> File Service
												</a>
											</li>
										</ul>
									</div>
									<div class="col-md-10">
										<div class="tab-content">
											<!-- begin tab_2_1 -->
											<div id="tab_2_1" class="tab-pane active">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Settings Overview
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															SMS Service
														</td>
														<td>
															<? if(($setting->sms_username != "") && ($setting->sms_password != "")){ echo "Active"; }else{ echo "Incomplete"; } ?>
														</td>
													</tr>
													<tr>
														<td>
															Email Service
														</td>
														<td>
															<? if(($setting->imap_from != "") && ($setting->imap_smtp != "") && ($setting->imap_folder != "") && ($setting->imap_port != "") && ($setting->imap_flags != "")){ echo "Active"; }else{ echo "Incomplete"; } ?>
														</td>
													</tr>
  												<tr>
														<td>
															File Service
														</td>
														<td>
															<? if(($setting->seafile_token != "") && ($setting->seafile_library != "")){ echo "Active"; }else{ echo "Incomplete"; } ?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!-- begin tab_2_2 -->
											<div id="tab_2_2" class="tab-pane">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															SMS Service
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															API ID
														</td>
														<td>
															<?=$setting->sms_apiid?>
														</td>
													</tr>
													<tr>
														<td>
															Username
														</td>
														<td>
															<?=$setting->sms_username?>
														</td>
													</tr>
													<tr>
														<td>
															Password
														</td>
														<td>
															<?=$setting->sms_password?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!-- begin tab_2_3 -->
											<div id="tab_2_3" class="tab-pane">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Email Service
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															From Address
														</td>
														<td>
														   <?=$setting->imap_from?>
														</td>
													</tr>
													<tr>
														<td>
															SMTP Server
														</td>
														<td>
															<?=$setting->imap_smtp?>
														</td>
													</tr>
													<tr>
														<td>
															Inbox Folder
														</td>
														<td>
															<?=$setting->imap_folder?>
														</td>
													</tr>
													<tr>
														<td>
															Port
														</td>
														<td>
															<?=$setting->imap_port?>
														</td>
													</tr>
													<tr>
														<td>
															Flags
														</td>
														<td>
															<?=$setting->imap_flags?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!-- begin tab_2_4 -->
											<div id="tab_2_4" class="tab-pane">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															File Service
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Seafile Token
														</td>
														<td>
															<?=$setting->seafile_token?>
														</td>
													</tr>
													<tr>
														<td>
															Seafile Library
														</td>
														<td>
															<?=$setting->seafile_library?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
										</div>	
									</div>
								</div>
							</div>
							<!--tab_3-->
							<div class="tab-pane" id="tab_3">
								<div class="row profile-account">
									<div class="col-md-2">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a data-toggle="tab" href="#tab_3_1">
													<i class="fa fa-eye"></i> Overview
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_2">
													<i class="fa fa-cog"></i> Product Categories
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_3">
													<i class="fa fa-cog"></i> Product Types
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_4">
													<i class="fa fa-cog"></i> Product Providers
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_5">
													<i class="fa fa-cog"></i> Ticket Types
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_6">
													<i class="fa fa-cog"></i> Service Types
												</a>
											</li>
											<li>
												<a data-toggle="tab" href="#tab_3_7">
													<i class="fa fa-cog"></i> Entity Types
												</a>
											</li>
										</ul>
									</div>
									<div class="col-md-10">
										<div class="tab-content">
											<!-- begin tab_3_1 -->
											<div id="tab_3_1" class="tab-pane active">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
													<tr>
														<th colspan="2">
															Options Overview
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Product Categories
														</td>
														<td>
															<?=sizeof($organizations_productcategories)?> of <?=$all_productcategories->num_rows()?>
														</td>
													</tr>
													<tr>
														<td>
															Product Types
														</td>
														<td>
															<?=sizeof($organizations_producttypes)?> of <?=$all_producttypes->num_rows()?>
														</td>
													</tr>
													<tr>
														<td>
															Product Providers
														</td>
														<td>
															<?=sizeof($organizations_productproviders)?> of <?=$all_productproviders->num_rows()?>
														</td>
													</tr>
													<tr>
														<td>
															Service Types
														</td>
														<td>
															<?=sizeof($organizations_servicetypes)?> of <?=$all_servicetypes->num_rows()?>
														</td>
													</tr>
													<tr>
														<td>
															Ticket Types
														</td>
														<td>
															<?=sizeof($organizations_tickettypes)?> of <?=$all_tickettypes->num_rows()?>
														</td>
													</tr>
													<tr>
														<td>
															Entity Types
														</td>
														<td>
															<?=sizeof($organizations_entitytypes)?> of <?=$all_entitytypes->num_rows()?>
														</td>
													</tr>
												</tbody>
												</table>
											</div>
											<!-- begin tab_3_2 -->
											<div id="tab_3_2" class="tab-pane form">
											<? if ($all_productcategories->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Product Categories
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently product categories linked to this entity, please <a href="<?=site_url("organizations/options_productcategories_add/".$orgid)?>">click here</a> to add a new user.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_productcategories_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th>Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_productcategories->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oepcid, array_column($organizations_productcategories, 'oepcid')) !== false) { ?>
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oepcid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oepcid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_productcategories_edit/".$orgid."/".$row->oepcid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
											<!-- begin tab_3_3 -->
											<div id="tab_3_3" class="tab-pane">
											<? if ($all_producttypes->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Product Types
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently no product types linked to this entity.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_producttypes_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th>Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_producttypes->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oeptid, array_column($organizations_producttypes, 'oeptid')) !== false) { ?>
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oeptid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oeptid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_producttypes_edit/".$row->oeptid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
											<!-- begin tab_3_4 -->
											<div id="tab_3_4" class="tab-pane">
											<? if ($all_productproviders->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Product Providers
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently no product providers linked to this entity.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_productproviders_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th>Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_productproviders->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oeppid, array_column($organizations_productproviders, 'oeppid')) !== false) { ?>
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oeppid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oeppid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_productproviders_edit/".$row->oeppid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
											<!-- begin tab_3_5 -->
											<div id="tab_3_5" class="tab-pane">
											<? if ($all_tickettypes->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Ticket Types
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently no ticket types linked to this entity.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_tickettypes_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th colspan="2">Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_tickettypes->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oettid, array_column($organizations_tickettypes, 'oettid')) !== false) { ?>  
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oettid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oettid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_tickettypes_edit/".$row->oettid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
                            <td>
                              <a href="<?=site_url("organizations/workflows_tickets_edit/".$row->oettid) ?>" class="btn default btn-xs green-stripe">edit</a>
                            </td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
											<!-- begin tab_3_6 -->
											<div id="tab_3_6" class="tab-pane">
											<? if ($all_servicetypes->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Service Types
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently no service types linked to this entity.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_servicetypes_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th>Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_servicetypes->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oestid, array_column($organizations_servicetypes, 'oestid')) !== false) { ?>
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oestid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oestid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_servicetypes_edit/".$row->oestid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
											<!-- begin tab_3_7 -->
											<div id="tab_3_7" class="tab-pane">
											<? if ($all_entitytypes->num_rows() == 0){ ?>
												<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
														<tr>
															<th>
																No Entity Types
															</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>There are currently no entity types linked to this entity.</td>
														</tr>
													</tbody>
												</table>
											<? }else{ ?>
												<form method="post" action="<?=site_url("organizations/options_entitytypes_update/multiple") ?>">
													<input type="hidden" name="orgid" value="<?=$orgid?>">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>Status</th>
														<th width="100%">
															Description
														</th>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<th>Actions</th>
														<? } ?>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($all_entitytypes->result() as $row): ?>
													<tr>
														<td>
                              <? if(array_search($row->oetid, array_column($organizations_entitytypes, 'oetid')) !== false) { ?>
																<input type="checkbox" checked="true" name="options[]" class="checkboxes" value="<?=$row->oetid?>"/>
															<? }else{ ?>
																<input type="checkbox" name="options[]" class="checkboxes" value="<?=$row->oetid?>"/>
															<? } ?>
														</td>
														<td>
															<?=$row->description?>
														</td>
														<? if ($this->session->userdata('accesslevel') == 1){ ?>
														<td>
															<a href="<?=site_url("organizations/options_entitytypes_edit/".$row->oetid) ?>" class="btn default btn-xs yellow-stripe">rename</a>
														</td>
														<? } ?>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
													<button type="submit" class="btn green">Update Selection</button>
												</form>
											<? } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
		</div>
	</div>
	<!-- END CONTENT -->