<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$this->session->userdata('fullname')?><small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					<? if ($this->session->userdata('accesslevel') <= 2){ ?>
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
							<!--
								<li>
									<a href="javascript:;">Mark as UnRead </a>
								</li>
								<li>
									<a href="javascript:;">Mark as Spam </a>
								</li>
								<li>
									<a href="<?=site_url("account/emails_reply/".$msgno."/1")?>">Reply to Sender </a>
								</li>
								<li>
									<a href="javascript:;">Reply to All </a>
								</li>
								<li class="divider"></li>
							-->
							<? if ($eid != NULL){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Archive Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a data-toggle="modal" href="#archiveemailentitymodal">To <?=$entity->name?></a>
										</li>
										<li>
											<a data-toggle="modal" href="#archiveemailticketmodal">To Ticket</a>
										</li>
									</ul>
								</li>
							<? } ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Link Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a data-toggle="modal" href="#linkemailentitymodal">To Entity</a>
										</li>
										<li>
											<a data-toggle="modal" href="#linkemailticketmodal">To Ticket</a>
										</li>
									</ul>
								</li>
								<li>
									<a data-toggle="modal" href="#deleteemailmodal">Delete Email</a>
								</li>
							</ul>
						</li>
						<? } ?>
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("account/view")?>">
								Account
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("account/view/#tab_1_5")?>">
								Emails
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row inbox">
				<div class="col-md-12 col-sm-12">
					<!-- BEGIN TICKETS PORTLET-->
					<div class="portlet box light-grey tasks-widget">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i><? if (isset($msgoverview[0]->subject)){ echo $msgoverview[0]->subject; }else{ echo "no subject";}?> <small>(<?=date("l j F Y", strtotime($msgoverview[0]->date));?>)</small>
							</div>
						</div>
						<div class="portlet-body">
							<div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
								<?=$msgbody?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			<!--BEGIN MODAL FOR LINK EMAIL TO ENTITY-->
				<div class="modal fade" id="linkemailentitymodal" tabindex="-1" role="linkemailentitymodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Link Message to Entity</h4>
							</div>
							<div class="modal-body">
								 Who do you want to link this message to?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/emails_link/".$msgno)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR LINK EMAIL -->
			
			<!--BEGIN MODAL FOR LINK EMAIL TO TICKET -->
				<div class="modal fade" id="linkemailticketmodal" tabindex="-1" role="linkemailticketmodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Link Message to Ticket</h4>
							</div>
							<div class="modal-body">
								 Which ticket do you want to link this message to?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/emails_link/".$msgno)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR LINK EMAIL -->
			
			<!--BEGIN MODAL FOR DELETE EMAIL -->
				<div class="modal fade" id="deleteemailmodal" tabindex="-1" role="deleteemailmodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Delete Email</h4>
							</div>
							<div class="modal-body">
								 Are you sure you want to delete this email?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/emails_delete/".$msgno)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR DELETE EMAIL -->
			
			<!--BEGIN MODAL FOR ARCHIVE EMAIL TO ENTITY-->
				<div class="modal fade" id="archiveemailentitymodal" tabindex="-1" role="archiveemailentitymodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Archive Email</h4>
							</div>
							<div class="modal-body">
								 Are you sure you want to archive this email and delete the message once the content is copied?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/emails_archive/".$msgno)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR ARCHIVE EMAIL TO ENTITY-->
			
			<!--BEGIN MODAL FOR ARCHIVE EMAIL TO TICKET -->
				<div class="modal fade" id="archiveemailticketmodal" tabindex="-1" role="archiveemailticketmodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Archive Email</h4>
							</div>
							<div class="modal-body">
								 Are you sure you want to archive this email and delete the message once the content is copied?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("account/emails_archive/".$msgno)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR ARCHIVE EMAIL TO TICKET-->
			
		</div>
	</div>
	<!-- END CONTENT -->