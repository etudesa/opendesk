<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?><small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("account/view")?>">
								Account
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("account/view/#tab_1_5")?>">
								Emails
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!-- BEGIN TICKETS PORTLET-->
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i><? if (isset($msgoverview[0]->subject)){ echo $msgoverview[0]->subject; }else{ echo "no subject";}?> <small>(<?=date("l j F Y", strtotime($msgoverview[0]->date));?>)</small>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("account/emails_archive") ?>">
								<input type="hidden" name="msgno" value="<?=$msgno?>">
								<input type="hidden" name="folder" value="<?=$folder?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Link to Entity</label>
										<select name="eid" class="form-control">
										<? FOREACH($entities->result() as $row): ?>
											<option value="<?=$row->eid?>"><?=$row->name?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Link to Ticket</label>
										<select name="createdby" class="form-control">
										<? FOREACH($tickets->result() as $row): ?>
											<option value="<?=$row->etid?>"><?=$row->title?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->