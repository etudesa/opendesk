	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								View
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_4")?>">
								Tickets
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Add Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/tickets_insert") ?>">
								<input type="hidden" name="eid" value="<?=$entity->eid?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Type of Ticket</label>
										<select id="select2_types" class="form-control select2" name="type">
										<? if ($entitytickettypes->num_rows() != 0) { ?>
											<option value=""></option>
											<? FOREACH($entitytickettypes->result() as $row): ?>
												<option value="<?=$row->oettid?>"><?=$row->description?></option>
											<? ENDFOREACH; ?>
										<? } else { ?>
											<option value="">-- no ticket types available, please specify these under profile options --</option>
										<? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Releted Product</label>
										<select id="select2_products" class="form-control select2" name="epid">
											<? if ($products->num_rows() != 0) { ?>
												<option value=""></option>
											<? FOREACH($products->result() as $row): ?>
												<option value="<?=$row->epid?>"><?=$row->productcategorydescription?> - <?=$row->identifier?> - <?=$row->producttypedescription?></option>
											<? ENDFOREACH; ?>
											<? } else { ?>
												<option value="">-- no products available --</option>
											<? } ?>
										</select>
										<label class="control-label">Related Service</label>
										<select id="select2_services" class="form-control select2" name="esid">
										<? if ($services->num_rows() != 0) { ?>
										<? FOREACH($services->result() as $row): ?>
											<option value=""></option>
											<option value="<?=$row->esid?>"><?=$row->servicetypedescription?> - <?=$row->identifier?></option>
										<? ENDFOREACH; ?>
										<? } else { ?>
											<option value="">-- no services available --</option>
										<? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Short Title</label>
										<input type="text" class="form-control" placeholder="Enter ticket short title - no more than 30 characters" name="title">
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea class="form-control" placeholder="Enter ticket full description" name="description"></textarea>
									</div>
									<div class="form-group">
										<label class="control-label">Reminder Date</label>
										<input class="form-control input-long date-picker" placeholder="Click here to enable date picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="reminderdate"/>
									</div>
									<div class="form-group">
										<label class="control-label">Due Date</label>
										<input class="form-control input-long date-picker" placeholder="Click here to enable date picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="duedate"/>
									</div>
									<div class="form-group">
										<label class="control-label">Owned By</label>
										<select name="ownedby" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Assigned To</label>
										<select name="assignedto" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Status</label>
										<select name="status" class="form-control">
										<? FOREACH($entityticketstatus->result() as $row): ?>
											<option value="<?=$row->oetsid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
								</div>
								<div class="form-actions">
									<? if (($products->num_rows() == 0) && ($services->num_rows() == 0)){ ?>
										<button type="submit" class="btn green" disabled>No products or services available</button>
									<? } else {?>
										<button type="submit" class="btn green">Submit</button>
									<? } ?>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->