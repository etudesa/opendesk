	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								View
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_3")?>">
								Tickets
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-7">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Ticket
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN EDIT TICKET FORM-->
							<form method="post" action="<?=site_url("entities/tickets_update") ?>">
								<input type="hidden" name="eid" value="<?=$ticket->eid?>">
								<input type="hidden" name="etid" value="<?=$ticket->etid?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Short Title</label>
										<input type="text" class="form-control" placeholder="Enter ticket short title" name="title" value="<?=$ticket->title?>">
									</div>
									<div class="form-group">
										<label class="control-label">Description</label>
										<textarea class="form-control" placeholder="" name="description"><?=$ticket->description?></textarea>
									</div>
									<div class="form-group">
										<label class="control-label">Type</label>
										<select name="type" class="form-control">
										<? FOREACH($entitytickettypes->result() as $row): ?>
											<option value="<?=$row->oettid?>" <? if ($row->oettid == $ticket->type){ echo "selected";}?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Reminder Date</label>
										<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?=$ticket->reminderdate?>" name="reminderdate"/>
									</div>
									<div class="form-group">
										<label class="control-label">Due Date</label>
										<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?=$ticket->duedate?>" name="duedate"/>
									</div>
									<div class="form-group">
										<label class="control-label">Owned By</label>
										<select name="ownedby" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $ticket->ownedby){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Assigned To</label>
										<select name="assignedto" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $ticket->assignedto){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Status</label>
										<select name="status" class="form-control">
										<? FOREACH($entityticketstatus->result() as $row): ?>
											<option value="<?=$row->oetsid?>"  <? if ($row->oetsid == $ticket->status){ echo "selected";}?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END EDIT TICKET FORM-->
						</div>
					</div>
				</div>
				
				<!-- BEGIN WORKFLOW OVERVIEW -->
				<!--TODO: consider implementing togglers for dynamic fields here -->
				<div class="col-md-5 col-sm-4">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Ticket Workflow
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
								<a href="javascript:;" class="reload"></a>
							</div>
						</div>
						<div class="portlet-body">
							
							<? if (($ticket->field1_description != "") || ($ticket->field2_description != "") || ($ticket->field3_description != "") || ($ticket->field4_description != "") || ($ticket->field5_description != "")){ ?>
							
							<div class="progress progress-striped active">
								<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?=$percentage?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$percentage?>%"></div>
							</div>
							
							<ul class="list-group">
								<? if ($ticket->field1_description != ""){ ?>
								<li class="list-group-item">
									<?=$ticket->field1_description?>
									<? if ($ticket->field1 == 1){ ?> 
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/1/0") ?>" class="badge badge-success">completed</a>
									<? }else{ ?>
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/1/1") ?>" class="badge badge-warning">pending</a>
									<? } ?>
								</li>
								<? } ?>
								<? if ($ticket->field2_description != ""){ ?>
								<li class="list-group-item">
									<?=$ticket->field2_description?>
									<? if ($ticket->field2 == 1){ ?> 
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/2/0") ?>" class="badge badge-success">completed</a>
									<? }else{ ?>
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/2/1") ?>" class="badge badge-warning">pending</a>
									<? } ?>
								</li>
								<? } ?>
								<? if ($ticket->field3_description != ""){ ?>
								<li class="list-group-item">
									<?=$ticket->field3_description?>
									<? if ($ticket->field3 == 1){ ?> 
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/3/0") ?>" class="badge badge-success">completed</a>
									<? }else{ ?>
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/3/1") ?>" class="badge badge-warning">pending</a>
									<? } ?>
								</li>
								<? } ?>
								<? if ($ticket->field4_description != ""){ ?>
								<li class="list-group-item">
									<?=$ticket->field4_description?>
									<? if ($ticket->field4 == 1){ ?> 
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/4/0") ?>" class="badge badge-success">completed</a>
									<? }else{ ?>
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/4/1") ?>" class="badge badge-warning">pending</a>
									<? } ?>
								</li>
								<? } ?>
								<? if ($ticket->field5_description != ""){ ?>
								<li class="list-group-item">
									<?=$ticket->field5_description?>
									<? if ($ticket->field5 == 1){ ?> 
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/5/0") ?>" class="badge badge-success">completed</a>
									<? }else{ ?>
										<a href="<?=site_url("entities/tickets_update/".$ticket->eid."/".$ticket->etid."/5/1") ?>" class="badge badge-warning">pending</a>
									<? } ?>
								</li>
								<? } ?>
							</ul>
							
							
							<? }else{ ?>
							
							<p class="text-warning">
								 No workflow specified.
							</p>
							
							<? } ?>
							
						</div>
					</div>
				
					<!-- PRODUCT OVERVIEW BEGIN-->
					<? if ($product != null) { ?>
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Related Product
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<div class="form-body">
								<div class="form-group">
									<label class="control-label">Product Category</label>
									<input type="text" class="form-control" placeholder="" value="<?=$product->productcategorydescription?>">
								</div>
								<div class="form-group">
									<label class="control-label">Product Type</label>
									<input type="text" class="form-control" placeholder="" value="<?=$product->producttypedescription?>">
								</div>
								<div class="form-group">
									<label class="control-label">Product Provider</label>
									<input type="text" class="form-control" placeholder="" value="<?=$product->productproviderdescription?>">
								</div>
								<div class="form-group">
									<label class="control-label">Product Identifier</label>
									<input type="text" class="form-control" placeholder="" value="<?=$product->identifier?>">
								</div>
								<div class="form-group">
									<label class="control-label">Product Issue Date</label>
									<input type="text" class="form-control" placeholder="" value="<?=$product->issuedate?>">
								</div>
							</div>
						</div>
					</div>
					<? } ?>
					<!-- PRODUCT OVERVIEW END-->
					
					<!-- SERVICE OVERVIEW BEGIN-->
					<? if ($service != null) { ?>
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Related Service
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<ul class="list-group">
								<li class="list-group-item">
									Service Type
									<span class="badge badge-primary">
										 <?=$service->servicetypedescription?>
									</span>
								</li>
								<li class="list-group-item">
									Service Identifier
									<span class="badge badge-primary">
										 <?=$service->identifier?>
									</span>
								</li>
								<li class="list-group-item">
									Service Issue Date
									<span class="badge badge-primary">
										 <?=$service->issuedate?>
									</span>
								</li>
							</ul>
						</div>
					</div>
					<? } ?>
					<!-- SERVICE OVERVIEW end-->
					
					<!-- DOCUMENTS BEGIN-->
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Related Documents
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<p class="text-warning">
								 No documents available
							</p>
						</div>
					</div>
					<!-- DOCUMENTS END-->
				
				</div>
			</div>
			
			<!-- TICKET NOTES -->
			<div class="row profile">
				<div class="col-md-12 col-sm-4">
					<div class="portlet box light-grey">
					
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Notes
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<form method="post" action="<?=site_url("entities/tickets_notes_insert") ?>">
							<input type="hidden" name="eid" value="<?=$ticket->eid?>">
							<input type="hidden" name="etid" value="<?=$ticket->etid?>">
							<input type="hidden" name="updated" value="<?=date('Y-m-d')?>">
							<div class="form-body">
								<? FOREACH($entityticketnotes->result() as $note): ?>
								<div class="note note-info">
									<div class=""><?=$note->description?></div>
									<div class="right"><?=$note->updated?></div>
								</div>
								<? ENDFOREACH; ?>
								<div class="form-group">
									<textarea class="form-control" placeholder="" name="description"></textarea>
								</div>
							</div>
						<div class="form-actions">
							<button type="submit" class="btn blue">Add Note</button>
						</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->