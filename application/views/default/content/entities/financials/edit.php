	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($contact->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_6")?>">
								Financials
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Financial
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form role="form" method="post" action="<?=site_url("entities/financials_update") ?>">
								<input type="hidden" name="efid" value="<?=$financial->efid?>">
								<input type="hidden" name="eid" value="<?=$financial->eid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<div class="form-body">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Property 1</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="property1" value="<?=$financial->property1?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Property 2</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="property2" value="<?=$financial->property2?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Property 3</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="property3" value="<?=$financial->property3?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Business Interest 1</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="business1" value="<?=$financial->business1?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Business Interest 2</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="business2" value="<?=$financial->business2?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Business Interest 3</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="business3" value="<?=$financial->business3?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Investment 1</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="investment1" value="<?=$financial->investment1?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Investment 2</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="investment2" value="<?=$financial->investment2?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Investment 3</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="investment3" value="<?=$financial->investment3?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Insurance 1</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="insurance1" value="<?=$financial->insurance1?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Insurance 2</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="insurance2" value="<?=$financial->insurance2?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Insurance 3</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="insurance3" value="<?=$financial->insurance3?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Other 1</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="other1" value="<?=$financial->other1?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Other 2</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="other2" value="<?=$financial->other2?>">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label class="control-label">Other 3</label>
												<input type="text" class="form-control" placeholder="Enter amount in Rands" name="other3" value="<?=$financial->other3?>">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->