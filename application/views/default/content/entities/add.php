	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN MESSAGE SECTION-->
			<? if ($this->session->flashdata('message') != ""){ ?>
			<div class="alert alert-danger display">
				<button class="close" data-close="alert"></button>
				<span>
					<?=$this->session->flashdata('message');?>
				</span>
			</div>
			<? } ?>
			<!-- END MESSAGE SECTION-->
			
			<!-- BEGIN PAGE CONTENT-->
      <!--Can create specific jquery for this - http://jqueryvalidation.org/validate/ -->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Add Entity
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/insert") ?>" id="entity">
								<div class="form-body">		
									<div class="alert alert-danger display-hide">
										<button class="close" data-close="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success display-hide">
										<button class="close" data-close="alert"></button>
										Your form validation is successful!
									</div>
                  
									<input type="hidden" name="updated" value="<?=$today?>">
									<input type="hidden" name="profile" value="<?=$this->session->userdata('profile')?>">
									<input type="hidden" name="mode" value="1">
									<input type="hidden" name="pid" id="pid" value="">
									<div class="form-group">
										<label class="control-label">Full Name <span class="required">*</span></label>
										<input type="text" class="form-control" placeholder="Enter the full name of the entity" name="name" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Registration Number</label>
										<input type="text" class="form-control" placeholder="Enter ID/IT/CIPC number" name="registration" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Status</label>
										<select name="status" class="form-control">
										<? if ($entitystatus->num_rows() != 0) { ?>
											<option value="" disabled selected>-- specify the entity status --</option>
											<? FOREACH($entitystatus->result() as $row): ?>
												<option value="<?=$row->oesid?>" <? if ($status == $row->oesid){ echo "selected"; }?>><?=$row->description?></option>
											<? ENDFOREACH; ?>
										<? } else { ?>
											<option value="">-- no entity status available, please specify these under profile options --</option>
										<? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Owner</label>
										<select name="createdby" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $this->session->userdata('uid')){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Type <span class="required">*</span></label>
										<select name="type" class="form-control">
										<? if ($entitytypes->num_rows() != 0) { ?>
											<option value="" disabled selected>-- specify the entity type --</option>
											<? FOREACH($entitytypes->result() as $row): ?>
												<option value="<?=$row->oetid?>"><?=$row->description?></option>
											<? ENDFOREACH; ?>
										<? } else { ?>
											<option value="">-- no entity types available, please specify these under profile options --</option>
										<? } ?>
										</select>
									</div>
									<h4 class="form-section">Primary Contact Info</h4>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Existing Person</label>
												<select class="form-control select2me" data-placeholder="Enter person name..." id="person" name="person">
													<option value=""></option>
													<? FOREACH($persons->result() as $row): ?>
													<option value="<?=$row->pid?>"><?=$row->firstname?> <?=$row->lastname?></option>
													<? ENDFOREACH; ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">First Name <span class="required">*</span></label>
												<input type="text" class="form-control" placeholder="Enter the firstname..." name="firstname" id="firstname" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Last Name <span class="required">*</span></label>
												<input type="text" class="form-control" placeholder="Enter the lastname..." name="lastname" id="lastname" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Preferred Name</label>
												<input type="text" class="form-control" placeholder="Enter preferred name" name="preferredname" id="preferredname" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">ID Number</label>
												<input type="text" class="form-control" placeholder="Enter ID number" name="idnumber" id="idnumber" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Date of Birth</label>
												<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="dateofbirth" id="dateofbirth" placeholder="Click here to enable date picker"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Physical Address</label>
												<input type="text" class="form-control" placeholder="Enter physical address" name="physicaladdress" id="physicaladdress" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Postal Address</label>
												<input type="text" class="form-control" placeholder="Enter postal address" name="postaladdress" id="postaladdress" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Home Phone</label>
												<input type="text" class="form-control" placeholder="Enter home phone" name="homephone" id="homephone" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Office Phone</label>
												<input type="text" class="form-control" placeholder="Enter office phone" name="officephone" id="officephone" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Mobile Number</label>
												<input type="text" class="form-control" placeholder="Enter mobile number" name="mobile" id="mobile" value="">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Email Address</label>
												<input type="text" class="form-control" placeholder="Enter email address" name="email" id="email" value="">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	
	<script>
		$( document ).ready(function() {
			$("#person").change(function(){
				var package = $(this).val();
				$.ajax({
				   type:'POST',
				   data:{package:package},
				   url: '<?=site_url("entities/get_person/")?>'+'/'+package,
				   dataType:"json",
				   success:function(data){
				   	   $('#pid').val(data['pid']);
				   	   $('#firstname').val(data['firstname']);
				       $('#lastname').val(data['lastname']);
				       $('#preferredname').val(data['preferredname']);
				       $('#idnumber').val(data['idnumber']);
				       $('#dateofbirth').val(data['dateofbirth']);
				       $('#physicaladdress').val(data['physicaladdress']);
				       $('#postaladdress').val(data['postaladdress']);
				       $('#homephone').val(data['homephone']);
				       $('#officephone').val(data['officephone']);
				       $('#mobile').val(data['mobile']);
				       $('#email').val(data['email']);
				   }
				})
			});
		});
	</script>