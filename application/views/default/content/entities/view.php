	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
      <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						<?=$title?> <small><?//=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
              <? if ($entity->status == "1"){ ?>
							<a href="<?=site_url("entities/leads")?>">
								Leads
							</a>
              <? } ?>
              <? if ($entity->status == "2"){ ?>
							<a href="<?=site_url("entities/prospects")?>">
								Prospects
							</a>
              <? } ?>
              <? if ($entity->status == "3"){ ?>
							<a href="<?=site_url("entities/clients")?>">
								Clients
							</a>
              <? } ?>
						</li>
						<? if ($entity->status == 3){ ?>
						<li class="btn-group2">
							<!-- filters button -->
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Export
								</span>
								<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="<?=site_url("entities/exports/".$eid."/1")?>" target="new">
										Product Statement
									</a>
								</li>
							</ul>
						</li>
						<? } ?>
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Actions
								</span>
								<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Profile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/contacts_add/".$eid)?>">
												Add Contact
											</a>
										</li>
										<? if ($entity->status != "1"){ ?>
										<li>
											<a href="<?=site_url("entities/products_add/".$eid)?>">
												Add Product
											</a>
										</li>
										<? } ?>
										<? if (($entity->status != "1")){ ?>
										<li>
											<a href="<?=site_url("entities/services_add/".$eid)?>">
												Add Service
											</a>
										</li>
										<? } ?>
										<? if (($entity->type == 1) && ($entity->status != "1")){ ?>
										<li>
											<a href="<?=site_url("entities/medicals_add/".$eid)?>">
												Add Medical
											</a>
										</li>
										<? } ?>
										<? if (($entity->status != "1")){ ?>
										<li>
											<a href="<?=site_url("entities/financials_add/".$eid)?>">
												Add Financials
											</a>
										</li>
										<? } ?>										
									</ul>
								</li>
								<? if ($profile->correspondence == 1){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Correspondence&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/comments_add/".$eid)?>">
												Add Comment
											</a>
										</li>
                    <? if ($this->session->userdata('sms') == 1){ ?>
										<li>
											<a href="<?=site_url("entities/sms_add/".$eid)?>">
												Send SMS
											</a>
										</li>
                    <? } ?>
                    <? if ($this->session->userdata('email') == 1){ ?>
										<li>
											<a href="<?=site_url("entities/email_add/".$eid)?>">
												Send Email
											</a>
										</li>
                    <? } ?>
									</ul>
								</li>
								<? } ?>
								<? if (($profile->support == 1) && ($entity->status == 3)){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Support&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/tickets_add/".$eid)?>">
												Add Ticket
											</a>
										</li>
									</ul>
								</li>
								<? } ?>
								<? if (($profile->governance == 1) && ($entity->status == 3)){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Governance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/fais_licenseproduct_add/".$eid)?>">
												New License Product
											</a>
										</li>
										<li>
											<a href="<?=site_url("entities/fais_keyindividual_add/".$eid)?>">
												New Key Individual
											</a>
										</li>
										<li>
											<a href="<?=site_url("entities/fais_representatives_add/".$eid)?>">
												New Representative
											</a>
										</li>
									</ul>
								</li>
								<? } ?>
                <? if (($profile->files == 1) && ($entity->status == 3)){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Files &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<a href="<?=site_url("entities/files_add/".$eid)?>">
												Add Folder
											</a>
										</li>
									</ul>
								</li>
								<? } ?>
								<? if ($entity->status == "1"){ ?>
								<li>
									<a href="<?=site_url("entities/edit/".$eid)?>">
										Edit Lead
									</a>
								</li>
								<? } ?>
								<? if ($entity->status == "2"){ ?>
								<li>
									<a href="<?=site_url("entities/edit/".$eid)?>">
										Edit Prospect
									</a>
								</li>
								<? } ?>
								<? if ($entity->status == "3"){ ?>
								<li>
									<a href="<?=site_url("entities/edit/".$eid)?>">
										Edit Client
									</a>
								</li>
								<? if ($entity->access == "1"){ ?>
								<li>
									<a data-toggle="modal" href="#openentitymodal">
										Open Client
									</a>
								</li>
								<? } ?>
								<? } ?>
								<li class="divider"></li>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Convert To &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<? FOREACH($entitystatus->result() as $status): ?>
											<? if ($status->oesid != $entity->status){ ?>
											<a href="<?=site_url("entities/status/".$eid."/".$status->oesid)?>">
												 <?=$status->description?>
											</a>
											<? } ?>
											<? ENDFOREACH; ?>
										</li>
									</ul>
								</li>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Change Mode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<? FOREACH($entitymodes->result() as $mode): ?>
											<? if ($mode->oemid != $entity->mode){ ?>
											<a href="<?=site_url("entities/mode/".$eid."/".$mode->oemid)?>">
												 <?=$mode->description?>
											</a>
											<? } ?>
											<? ENDFOREACH; ?>
										</li>
									</ul>
								</li>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Set Primary Contact &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<li>
											<? FOREACH($contacts->result() as $row): ?>
											<a href="<?=site_url("entities/primary/".$row->eid."/".$row->ecid)?>">
												 <?=$row->firstname?> <?=$row->lastname?>
											</a>
											<? ENDFOREACH; ?>
											<a href="<?=site_url("entities/contacts_add/".$eid)?>">
												 New Contact
											</a>
										</li>
									</ul>
								</li>
                
                <!-- DISABLED UNTILL REVIEWED -->
								<?  if (1 == 2){//if (($this->session->userdata('accesslevel') <= 2) && ($entity->status == 3)){ ?>
								<li class="dropdown-submenu pull-left">
									<a href="javascript:;">
										Set Online Access &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</a>
									<ul class="dropdown-menu pull-left" style="">
										<? if ($entity->access == 0){ ?>
										<? $onlineaccessurl = site_url("entities/access/1/".$eid); ?>
										<li>
											<a data-toggle="modal" href="#onlineaccessmodal">
												 Active
											</a>
										</li>
										<? }else{ ?>
										<? $onlineaccessurl = site_url("entities/access/0/".$entity->eid); ?>
										<li>
											<a data-toggle="modal" href="#onlineaccessmodal">
												 Disabled
											</a>
										</li>
										<? } ?>
									</ul>
								</li>
								<? } ?>
                
							</ul>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
      
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet-body">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom tabbable-full-width">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#tab_1" data-toggle="tab">
										 Profile
									</a>
								</li>
								<? if ($profile->correspondence == 1){ ?>
								<li>
									<a href="#tab_2" data-toggle="tab">
										 Correspondence
									</a>
								</li>						
								<? } ?>
								<? if (($profile->support == 1) && ($entity->status == 3)){ ?>
								<li>
									<a href="#tab_3" data-toggle="tab">
										 Support
									</a>
								</li>
								<? } ?>
								<? if (($profile->files == 1) && ($entity->status == 3)){ ?>
								<li>
									<a href="#tab_4" data-toggle="tab">
										 Files
									</a>
								</li>
								<? } ?>
							</ul>
							<div class="tab-content">
								<!--begin tab_1-->
								<div class="tab-pane active" id="tab_1">
									<div class="row profile-account">
										<div class="col-md-2">
											<ul class="ver-inline-menu tabbable margin-bottom-10">
												<li class="active">
													<a data-toggle="tab" href="#tab_1_1">
														<i class="fa fa-eye"></i> Overview
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_1_2">
														<i class="fa fa-map-marker"></i> Contacts
													</a>
												</li>
												<? if (($entity->status != "1")){ ?>
												<li>
													<a data-toggle="tab" href="#tab_1_3">
														<i class="fa fa-shopping-cart"></i> Products
													</a>
												</li>
												<? } ?>
												<? if (($entity->status != "1")){ ?>
												<li>
													<a data-toggle="tab" href="#tab_1_4">
														<i class="fa fa-shopping-basket"></i> Services
													</a>
												</li>
												<? } ?>
												<? if (($entity->type == 1) && ($entity->status != "1")){ ?>
												<li>
													<a data-toggle="tab" href="#tab_1_5">
														<i class="fa fa-heartbeat"></i> Medicals
													</a>
												</li>
												<? } ?>
												<? if (($entity->status != "1")){ ?>
												<li>
													<a data-toggle="tab" href="#tab_1_6">
														<i class="fa fa-money"></i> Financials
													</a>
												</li>
												<? } ?>
											</ul>
										</div>
										<div class="col-md-10">
											<div class="tab-content">
												<!-- begin tab_1_1 -->
												<div id="tab_1_1" class="tab-pane active">
													<div class="col-md-6">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th colspan="2">
																		Profile Overview
																	</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>Mode</td>
																	<td>
																		<? 
																		  if($entity->mode == 0){
																		     echo "<span class='badge badge-warning'>".$entity->entitymode."</span>";
																		   }else{
																		     echo "<span class='badge badge-success'>".$entity->entitymode."</span>";
																		   }
																		?>
																	</td>
																</tr>
																<tr>
																	<td>Type</td>
																	<td>
																		<span class="badge badge-default"><?=$entity->entitytype?></span>
																	</td>
																</tr>
																<tr>
																	<td>Registration</td>
																	<td>
																		<span class="badge badge-default"><?=$entity->registration?></span>
																	</td>
																</tr>
																<tr>
																	<td>Status</td>
																	<td>
																		<span class="badge badge-default"><?=$entity->entitystatus?></span>
																	</td>
																</tr>
																<tr>
																	<td>Group Subscriptions</td>
																	<td>
																		<? 
																			if ($entity->groups != ""){
																				$groups = explode(",",$entity->groups);
																				FOR ($x=0; $x < count($groups); $x++){
					 																FOREACH($entitygroups->result() as $group):
																						if ($group->egid == $groups[$x]){
																							echo "<span class='badge badge-default'>".$group->name."</span>&nbsp;";
																							//echo $group->name;
																						}
																					ENDFOREACH;
																				}
																			}else{
																				echo "<span class='badge badge-warning'>none</span>";
																			}
																		?>
																	</td>
																</tr>
																<tr>
																	<td>Products</td>
																	<td>
																		<? 
																		  if($products->num_rows() == 0){
																		     echo "<span class='badge badge-warning'>none</span>";
																		   }else{
																		     echo "<span class='badge badge-default'>".$products->num_rows()."</span>";
																		   }
																		?>
																	</td>
																</tr>
																<tr>
																	<td>Services</td>
																	<td>
																		<? 
																		  if($services->num_rows() == 0){
																		     echo "<span class='badge badge-warning'>none</span>";
																		   }else{
																		     echo "<span class='badge badge-default'>".$services->num_rows()."</span>";
																		   }
																		?>
																	</td>
																</tr>
																<tr>
																	<td>Owned By</td>
																	<td>
																		<span class="badge badge-default"><?=$entity->entityowner?></span>
																	</td>
																</tr>
																<tr>
																	<td>Online Access</td>
																	<td>
																		 <? if ($entity->access == 1){?>
																			<span class="badge badge-success">Active</span>
																		<?}else{?>
																			<span class="badge badge-waning">Inactive</span>
																		<? } ?>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-6">
														<? if ($entity->primarycontact != NULL){ ?>
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th colspan="2">
																		Primary Contact
																	</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>Full Name</td>
																	<td>
																		<span class="badge badge-success"><?=$entity->firstname?> <?=$entity->lastname?></span>
																	</td>
																</tr>
																<tr>
																	<td>Email</td>
																	<td>
																		<span class="badge badge-success"><?=$entity->email?></span>
																	</td>
																</tr>
																<tr>
																	<td>Office Phone</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->officephone?></span>
																	</td>
																</tr>
																<tr>
																	<td>Home Phone</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->homephone?></span>
																	</td>
																</tr>
																<tr>
																	<td>Mobile Phone</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->mobile?></span>
																	</td>
																</tr>
																<tr>
																	<td>Physical Address</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->physicaladdress?></span>
																	</td>
																</tr>
																<tr>
																	<td>Postal Address</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->postaladdress?></span>
																	</td>
																</tr>
																<tr>
																	<td>Date of Birth</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->dateofbirth?></span>
																	</td>
																</tr>
																<tr>
																	<td>ID Number</td>
																	<td>
																		<span class="badge badge-info"><?=$entity->idnumber?></span>
																	</td>
																</tr>
															</tbody>
														</table>
														<? }else{ ?>
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	Primary Contact
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There is no primary contact specified.</td>
															</tr>
														</tbody>
														</table>
														<? } ?>
													</div>
												</div>
												<!-- begin tab_1_2 -->
												<div id="tab_1_2" class="tab-pane">
												<? if ($contacts->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Contacts
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are currently no contacts found for this entity, please <a href="<?=site_url("entities/contacts_add/".$eid)?>">click here</a> to add a new contact.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Firstname
														</th>
														<th>
															Lastname
														</th>
														<th>
															Email
														</th>
														<th>
															Mobile
														</th>
														<th>
															Relationship
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($contacts->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/contacts_edit/".$row->eid."/".$row->ecid)?>"><? if ($row->firstname == ""){ echo "&lt;empty&gt;"; } else{  echo $row->firstname; } ?></a>
														</td>
														<td>
															<?=$row->lastname?>
														</td>
														<td>
															<?=$row->email?>
														</td>
														<td>
															<?=$row->mobile?>
														</td>
														<td>
															<?=$row->relationshipdescription?>
														</td>
														<td>
															<a href="<?=site_url("entities/contacts_edit/".$row->eid."/".$row->ecid)?>" class="btn default btn-xs blue-stripe">Edit</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- begin tab_1_3 -->
												<div id="tab_1_3" class="tab-pane">
												<? if ($products->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Products
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are currently no products found for this entity, please <a href="<?=site_url("entities/products_add/".$eid)?>">click here</a> to add a new product.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Type
														</th>
														<th>
															Provider
														</th>
														<th>
															Category
														</th>
														<th>
															Identifier
														</th>
														<th>
															Status
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($products->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/products_edit/".$row->eid."/".$row->epid)?>"><?=$row->producttypedescription?></a>
														</td>
														<td>
															<?=$row->productproviderdescription?>
														</td>
														<td>
															<?=$row->productcategorydescription?>
														</td>
														<td>
															<?=$row->identifier?>
														</td>
														<td>
															<?=$row->productstatusdescription?>
														</td>
														<td>
															<a href="<?=site_url("entities/products_edit/".$row->eid."/".$row->epid)?>" class="btn default btn-xs green-stripe">
																 Edit
															</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- begin tab_1_4 -->
												<div id="tab_1_4" class="tab-pane">
												<? if ($services->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Services
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are currently no services found for this entity, please <a href="<?=site_url("entities/services_add/".$eid)?>">click here</a> to add a new service.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Type
														</th>
														<th>
															Identifier
														</th>
														<th>
															Date Issued
														</th>
														<th>
															Status
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($services->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/services_edit/".$row->eid."/".$row->esid)?>"><?=$row->servicetypedescription?></a>
														</td>
														<td>
															<?=$row->identifier?>
														</td>
														<td>
															<?=$row->issuedate?>
														</td>
														<td>
															<?=$row->servicestatusdescription?>
														</td>
														<td>
															<a href="<?=site_url("entities/services_edit/".$row->eid."/".$row->esid)?>" class="btn default btn-xs blue-stripe">Edit</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- begin tab_1_5 -->
												<div id="tab_1_5" class="tab-pane">
												<? if ($medicals->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Medicals
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are currently no medicals found for this entity, please <a href="<?=site_url("entities/medicals_add/".$eid)?>">click here</a> to add a new medical.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Date Captured
														</th>
														<th>
															Gender
														</th>
														<th>
															Weight
														</th>
														<th>
															Height
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($medicals->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/medicals_edit/".$row->eid."/".$row->emid)?>"><?=$row->updated?></a>
														</td>
														<td>
															<? if ($row->gender == 1){ echo "Male";}elseif($row->gender == 2){echo "Female"; }?>
														</td>
														<td>
															<?=$row->weight?>
														</td>
														<td>
															<?=$row->height?>
														</td>
														<td>
															<a href="<?=site_url("entities/medicals_edit/".$row->eid."/".$row->emid)?>" class="btn default btn-xs blue-stripe">Edit</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- begin tab_1_6 -->
												<div id="tab_1_6" class="tab-pane">
												<? if ($financials->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Financials
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are currently no financial reviews found for this entity, please <a href="<?=site_url("entities/financials_add/".$eid)?>">click here</a> to add a new financial review.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Date Captured
														</th>
														<th>
															Total Assets and Liabilities
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($financials->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/financials_edit/".$row->eid."/".$row->efid)?>"><?=$row->updated?></a>
														</td>
														<td></td>
														<td>
															<a href="<?=site_url("entities/financials_edit/".$row->eid."/".$row->efid)?>" class="btn default btn-xs blue-stripe">Edit</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--begin tab_2-->
								<div class="tab-pane" id="tab_2">
									<div class="row profile-account">
										<div class="col-md-2">
											<ul class="ver-inline-menu tabbable margin-bottom-10">
												<li class="active">
													<a data-toggle="tab" href="#tab_2_1">
														<i class="fa fa-eye"></i> Overview
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_2_2">
														<i class="fa fa-envelope"></i> Email Messages
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_2_3">
														<i class="fa fa-phone"></i> SMS Messages
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_2_4">
														<i class="fa fa-sticky-note"></i> Notes
													</a>
												</li>
											</ul>
										</div>
										<div class="col-md-10">
											<div class="tab-content">
												<!-- begin tab_2_1 -->
												<div id="tab_2_1" class="tab-pane active">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th colspan="2">
															Correspondence Overview
														</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td>Email Messages</td>
														<td><span class="badge badge-info"><?=$emails->num_rows()?></span></td>
													</tr>
													<tr>
														<td>SMS Messages</td>
														<td><span class="badge badge-info"><?=$sms->num_rows()?></span></td>
													</tr>
													<tr>
														<td>Comments</td>
														<td><span class="badge badge-info"><?=$comments->num_rows()?></span></td>
													</tr>
													</tbody>
													</table>
												</div>
												<!-- end tab_2_1 -->
												<!-- begin tab_2_2 -->
												<div id="tab_2_2" class="tab-pane">
												<? if ($emails->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Email Messages
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no email messages found for this entity.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Description
														</th>
														<th>
															Created By
														</th>
														<th>
															Updated
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($emails->result() as $row): ?>
													<tr>
														<td>
															<?=$row->description?>
														</td>
														<td>
															<?=$row->entityowner?>
														</td>
														<td>
															<?=$row->updated?>
														</td>
														<td>
															<a href="<?=site_url("entities/corre_edit/".$row->eid."/".$row->ecid)?>" class="btn default btn-xs blue-stripe">
	                                        					 Edit
	                                    					</a>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_2_2 -->
												<!-- begin tab_2_3 -->
												<div id="tab_2_3" class="tab-pane">
												<? if ($sms->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No SMS Messages
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no sms messages found for this entity.</td>
															</tr>
														</tbody>
													</table>		
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Description
														</th>
														<th>
															Created By
														</th>
														<th>
															Updated
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($sms->result() as $row): ?>
													<tr>
														<td>
															<?=$row->description?>
														</td>
														<td>
															<?=$row->entityowner?>
														</td>
														<td>
															<?=$row->updated?>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_2_3 -->
												<!-- begin tab_2_4 -->
												<div id="tab_2_4" class="tab-pane">
												<? if ($comments->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Comments
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no comments found for this entity.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Description
														</th>
														<th>
															Created By
														</th>
														<th>
															Updated
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($comments->result() as $row): ?>
													<tr>
														<td>
															<?=$row->description?>
														</td>
														<td>
															<?=$row->entityowner?>
														</td>
														<td>
															<?=$row->updated?>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_2_4 -->
											</div>
										</div>
									</div>
								</div>
								<!--begin tab_3-->
								<div class="tab-pane" id="tab_3">
									<div class="row profile-account">
										<div class="col-md-2">
											<ul class="ver-inline-menu tabbable margin-bottom-10">
												<li class="active">
													<a data-toggle="tab" href="#tab_3_1">
														<i class="fa fa-eye"></i> Overview
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_3_2">
														<i class="fa fa-ticket"></i> Pending Tickets
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_3_3">
														<i class="fa fa-ticket"></i> Completed Tickets
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#tab_3_4">
														<i class="fa fa-ticket"></i> Unresolved Tickets
													</a>
												</li>
											</ul>
										</div>
										<div class="col-md-10">
											<div class="tab-content">
												<!-- begin tab_3_1 -->
												<div id="tab_3_1" class="tab-pane active">
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th colspan="2">
															Tickets Overview
														</th>
													</tr>
													</thead>
													<tbody>
													<tr>
														<td>Pending Tickets</td>
														<td><span class="badge badge-success"><?=$pendingtickets->num_rows()?></span></td>
													</tr>
													<tr>
														<td>Completed Tickets</td>
														<td><span class="badge badge-success"><?=$completedtickets->num_rows()?></span></td>
													</tr>
													<tr>
														<td>Unresolved Tickets</td>
														<td><span class="badge badge-success"><?=$unresolvedtickets->num_rows()?></span></td>
													</tr>
													</tbody>
													</table>
												</div>
												<!-- end tab_3_1 -->
												<!-- begin tab_3_2 -->
												<div id="tab_3_2" class="tab-pane">
												<? if ($pendingtickets->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Pending Tickets.
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no pending tickets found for this entity.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Type
														</th>
														<th>
															Title
														</th>
														<th>
															Due Date
														</th>
														<th>
															Status
														</th>
														<th>
															Owner
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($pendingtickets->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>"><?=$row->tickettype?></a>
														</td>
														<td>
															<?=$row->title?>
														</td>
														<td>
															<?=$row->duedate?>
														</td>
														<td>
															<?=$row->ticketstatus?>
														</td>
														<td>
															<?=$row->ticketowner?>
														</td>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>" class="btn default btn-xs blue-stripe">
																 Edit
															</a>
															<? if ($this->session->userdata('accesslevel') == 1){ ?>
															<a href="<?=site_url("entities/tickets_delete/".$row->eid."/".$row->etid)?>" class="btn default btn-xs red-stripe">
																 Delete
															</a>
															<? } ?>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_3_2 -->
												<!-- begin tab_3_3 -->
												<div id="tab_3_3" class="tab-pane">
												<? if ($completedtickets->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Completed Tickets.
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no completed tickets found for this entity.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Type
														</th>
														<th>
															Title
														</th>
														<th>
															Due Date
														</th>
														<th>
															Status
														</th>
														<th>
															Owner
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($completedtickets->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>"><?=$row->tickettype?></a>
														</td>
														<td>
															<?=$row->title?>
														</td>
														<td>
															<?=$row->duedate?>
														</td>
														<td>
															<?=$row->ticketstatus?>
														</td>
														<td>
															<?=$row->ticketowner?>
														</td>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>" class="btn default btn-xs blue-stripe">
																 Edit
															</a>
															<? if ($this->session->userdata('accesslevel') == 1){ ?>
															<a href="<?=site_url("entities/tickets_delete/".$row->eid."/".$row->etid)?>" class="btn default btn-xs red-stripe">
																 Delete
															</a>
															<? } ?>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_3_3 -->
												<!-- begin tab_3_4 -->
												<div id="tab_3_4" class="tab-pane">
												<? if ($unresolvedtickets->num_rows() == 0){ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	No Unresolved Tickets.
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>There are no unresolved tickets found for this entity.</td>
															</tr>
														</tbody>
													</table>
												<? }else{ ?>
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr>
														<th>
															Type
														</th>
														<th>
															Title
														</th>
														<th>
															Due Date
														</th>
														<th>
															Status
														</th>
														<th>
															Owner
														</th>
														<th>
															Actions
														</th>
													</tr>
													</thead>
													<tbody>
													<? FOREACH($unresolvedtickets->result() as $row): ?>
													<tr>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>"><?=$row->tickettype?></a>
														</td>
														<td>
															<?=$row->title?>
														</td>
														<td>
															<?=$row->duedate?>
														</td>
														<td>
															<?=$row->ticketstatus?>
														</td>
														<td>
															<?=$row->ticketowner?>
														</td>
														<td>
															<a href="<?=site_url("entities/tickets_edit/".$row->eid."/".$row->etid)?>" class="btn default btn-xs blue-stripe">
																 Edit
															</a>
															<? if ($this->session->userdata('accesslevel') == 1){ ?>
															<a href="<?=site_url("entities/tickets_delete/".$row->eid."/".$row->etid)?>" class="btn default btn-xs red-stripe">
																 Delete
															</a>
															<? } ?>
														</td>
													</tr>
													<? ENDFOREACH; ?>
													</tbody>
													</table>
												<? } ?>
												</div>
												<!-- end tab_3_4 -->
											</div>
										</div>
									</div>
								</div>
								<!--begin tab_4-->
								<div class="tab-pane" id="tab_4">
									<div class="row profile-account">
										<div class="col-md-2">
											<ul class="ver-inline-menu tabbable margin-bottom-10">
												<li class="active">
													<a data-toggle="tab" href="#tab_5_1">
														<i class="fa fa-eye"></i> Overview
													</a>
												</li>
                        <li>
													<a data-toggle="tab" href="#tab_5_2">
														<i class="fa fa-eye"></i> Seafile Folder
													</a>
												</li>
											</ul>
										</div>
										<div class="col-md-10">
											<div class="tab-content">
												<!-- begin tab_5_1 -->
												<div id="tab_5_1" class="tab-pane active">
													<div class="col-md-12">
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th colspan="2">
																	Files Overview
																</th>
															</tr>
														</thead>
                            <tbody>
                            <? if ($all_files->num_rows() > 0){ ?>  
                              <? FOREACH($all_files->result() as $row): ?>  
                              <tr>
                                <td>Type</td>
                                <td><a href="<?=$profile->seafile_library?>" target="new"><span class="badge badge-success"><? if($row->type == 1){ echo "Seafile"; } ?></span></a></td>
                              </tr>
                              <? ENDFOREACH; ?>
                            <? }else{ ?>
                              <tr>
                                <td colspan="2">No Folders Linked to this entity.</td>
                              </tr>
                            <? } ?>
                            </tbody>
														</table>
													</div>
												</div>
  											<!-- begin tab_5_2 -->
												<div id="tab_5_2" class="tab-pane">
													<div class="col-md-12">
														<table class="table table-striped table-bordered table-advance table-hover">
														<thead>
															<tr>
																<th>
																	Seafile Folder
																</th>
                                <th>Action</th>
															</tr>
														</thead>
                            <tbody>
                            <? if ($seafile_files->num_rows() > 0){ ?>  
                              <? FOREACH($seafile_files->result() as $row): ?>  
                              <tr>
                                <td><a href="<?=$row->folder?>" target="new"><?=$row->folder?></a></td>
                                <td><a href="<?=site_url("entities/files_edit/".$row->eid."/".$row->efid)?>" class="btn default btn-xs blue-stripe">Edit</a></td>
                              </tr>
                              <? ENDFOREACH; ?>
                            <? }else{ ?>
                              <tr>
                                <td colspan="2">No seafile files found</td> 
                              </tr>
                            <? } ?>
                            </tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
			<!-- END PAGE CONTENT-->
			
			<!--BEGIN MODAL FOR ONLINE ACCESS -->
				<div class="modal fade" id="onlineaccessmodal" tabindex="-1" role="onlineaccessmodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Online Access to Entity</h4>
							</div>
							<? if ($entity->primarycontact == NULL){ ?>
							<div class="modal-body">
								 You need to specify a primary contact before you can continue.
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">OK</button>
							</div>
							<? }elseif ($entity->email == NULL){ ?>
							<div class="modal-body">
								 You need to specify an email for the primary contact before you can continue.
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">OK</button>
							</div>
							<? }else{ ?>
							<div class="modal-body">
								 You are about to give online access to this entity, are you sure you wish to do this. 
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=$onlineaccessurl?>')">Yes</button>
							</div>
							<? } ?>
						</div>
					</div>
				</div>
			<!--END MODAL FOR ONLINE ACCESS -->
			
			<!--BEGIN MODAL FOR OPEN ENTITY -->
				<div class="modal fade" id="openentitymodal" tabindex="-1" role="openentitymodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Open and Manage Entity</h4>
							</div>
							<div class="modal-body">
								 You are about to change over to and operate under <strong><?=$title?></strong>. All changes will be then be saved under this entity. Are you sure you want to continue?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("entities/open/".$eid)?>')">Yes</button>
							</div>
						</div>
					</div>
				</div>
			<!--END MODAL FOR OPEN ENTITY -->
			
			</div>
		</div>
	</div>
	<!-- END CONTENT -->