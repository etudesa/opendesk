<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						<?=$title?>&nbsp;<small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<? if ($status == 3){ ?>
						<li class="btn-group2">
							<!-- filters button -->
              <? if ($filters->num_rows() > 0){ ?>
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Filters
								</span>
								<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
                <? FOREACH($filters->result() as $filter): ?>
                <li>
                  <a href="<?=site_url("entities/filter/".$filter->oefid)?>">
                    <?=$filter->title?>&nbsp;
                  </a>
                </li>
                <? ENDFOREACH; ?>
							</ul>
						</li>
            <? }?>
						<? } ?>
						<li class="btn-group">
							<!-- actions button -->
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span>
									Actions
								</span>
								<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="<?=site_url("entities/add/".$status)?>">
										New Entity
									</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-list"></i>
							<a href="">
								Listing
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<?=$title?>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="entities">
							<thead>
							<tr>
								<th>
									 Name
								</th>
								<th>
									 Identification
								</th>
								<th>
									 Type
								</th>
								<th>
									 Status
								</th>
								<th>
									 Mode
								</th>
							</tr>
							</thead>
							<tbody>
              <? FOREACH($query->result() as $row): ?>
							<tr class="odd gradeX">
								<td width="100%">
									 <a href="<?=site_url("entities/view/".$row->eid)?>"><?=$row->name?></a>
								</td>
								<td nowrap>
									 <?=$row->registration?>
								</td>
								<td nowrap>
									 <?=$row->entitytype?>
								</td>
								<td nowrap>
									 <?=$row->entitystatus?>
								</td>
								<td nowrap>
									 <?=$row->entitymode?>
								</td>
							</tr>
              <? ENDFOREACH; ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->