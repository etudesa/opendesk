	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($contact->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_4")?>">
								Services
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Service
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/services_update") ?>">
								<input type="hidden" name="eid" value="<?=$service->eid?>">
								<input type="hidden" name="esid" value="<?=$service->esid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Type</label>
												<select name="type" class="form-control" >
												<? FOREACH($servicetypes->result() as $row): ?>
													<option value="<?=$row->oestid?>" <? if ($service->type == $row->oestid){ echo "selected"; }?> ><?=$row->description?></option>
												<? ENDFOREACH; ?>
												</select>
											</div>
											<div class="form-group">
												<label class="control-label">Unique Number</label>
												<input type="text" class="form-control" placeholder="Enter unique number" name="identifier" value="<?=$service->identifier?>">
											</div>
											<div class="form-group">
												<label class="control-label">Date Issued</label>
												<input class="form-control input-long date-picker" placeholder="Enter date issue" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?=$service->issuedate?>" name="issuedate"/>
											</div>
											<div class="form-group">
												<label class="control-label">Status</label>
												<select name="status" class="form-control">
													<option value="2" <? if ($service->status == 2){ echo "selected"; }?>>Inactive</option>
													<option value="1" <? if ($service->status == 1){ echo "selected"; }?>>Active</option>
												</select>
											</div>
										</div>
									</div>

								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
									<a class="btn red" data-toggle="modal" href="#deleteservicemodal">Delete Service</a>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			<!--BEGIN MODAL FOR SERVICE DELETE -->
				<div class="modal fade" id="deleteservicemodal" tabindex="-1" role="deleteservicemodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Delete Service</h4>
							</div>
							<div class="modal-body">
								 You are about to delete <strong><?=$service->identifier?></strong>. Are you sure you want to do this?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("entities/services_delete/".$service->eid."/".$service->esid) ?>')">Yes</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			<!-- END MODAL -->
			
		</div>
	</div>
	<!-- END CONTENT -->