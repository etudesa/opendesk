	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($contact->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_5")?>">
								Medicals
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Medical
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/medicals_update") ?>">
								<input type="hidden" name="eid" value="<?=$medical->eid?>">
								<input type="hidden" name="emid" value="<?=$medical->emid?>">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Gender</label>
												<select name="gender" class="form-control" >
													<option value="" disabled selected>--Specify Gender--</option>
													<option value="1" <?if ($medical->gender == 1){ echo "selected";}?> >Male</option>
													<option value="2" <?if ($medical->gender == 2){ echo "selected";}?> >Female</option>
												</select>
											</div>
											<div class="form-group">
												<label class="control-label">Weight (Kg)</label>
												<input type="text" class="form-control" placeholder="Enter weight in kilograms" name="weight" value="<?=$medical->weight?>">
											</div>
											<div class="form-group">
												<label class="control-label">Height (M)</label>
												<input type="text" class="form-control" placeholder="Enter height in meters" name="height" value="<?=$medical->height?>">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->