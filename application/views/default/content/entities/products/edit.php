	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($contact->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_3")?>">
								Products
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit <?=$product->productcategorydescription?> Product
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/products_update") ?>"  id="product">
								<input type="hidden" name="eid" value="<?=$product->eid?>">
								<input type="hidden" name="epid" value="<?=$product->epid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label">Provider</label>
												<select name="provider" class="form-control">
												<? FOREACH($productproviders->result() as $row): ?>
													<option value="<?=$row->oeppid?>" <? if ($product->provider == $row->oeppid){ echo "selected"; }?> ><?=$row->description?></option>
												<? ENDFOREACH; ?>
												</select>
											</div>
											<div class="form-group">
												<label class="control-label">Type</label>
												<select name="type" class="form-control" >
												<? FOREACH($producttypes->result() as $row): ?>
													<option value="<?=$row->oeptid?>" <? if ($product->type == $row->oeptid){ echo "selected"; }?> ><?=$row->description?></option>
												<? ENDFOREACH; ?>
												</select>
											</div>
											<div class="form-group">
												<label class="control-label">Unique Number</label>
												<input type="text" class="form-control" placeholder="Enter unique number" name="identifier" value="<?=$product->identifier?>">
											</div>
											<div class="form-group">
												<label class="control-label">Date Issued</label>
												<input class="form-control input-long date-picker" placeholder="Enter date issue" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?=$product->issuedate?>" name="issuedate"/>
											</div>
											<div class="form-group">
												<label class="control-label">Status</label>
												<select name="status" class="form-control">
													<option value="2" <? if ($product->status == 2){ echo "selected"; }?>>Inactive</option>
													<option value="1" <? if ($product->status == 1){ echo "selected"; }?>>Active</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<? if ($product->field1_description != NULL){?>
											<div class="form-group">
												<label class="control-label"><?=$product->field1_description?></label>
												<select name="field1" class="form-control">
													<option value="0" <? if ($product->field1 == 0){ echo "selected"; }?>>No</option>
													<option value="1" <? if ($product->field1 == 1){ echo "selected"; }?>>Yes</option>
												</select>
											</div>
											<? } ?>
											<? if ($product->field2_description != NULL){?>
											<div class="form-group">
												<label class="control-label"><?=$product->field2_description?></label>
												<select name="field2" class="form-control">
													<option value="0" <? if ($product->field2 == 0){ echo "selected"; }?>>No</option>
													<option value="1" <? if ($product->field2 == 1){ echo "selected"; }?>>Yes</option>
												</select>
											</div>
											<? } ?>
											<? if ($product->field3_description != NULL){?>
											<div class="form-group">
												<label class="control-label"><?=$product->field3_description?></label>
												<select name="field3" class="form-control">
													<option value="0" <? if ($product->field3 == 0){ echo "selected"; }?>>No</option>
													<option value="1" <? if ($product->field3 == 1){ echo "selected"; }?>>Yes</option>
												</select>
											</div>
											<? } ?>
											<? if ($product->field4_description != NULL){?>
											<div class="form-group">
												<label class="control-label"><?=$product->field4_description?></label>
												<select name="field4" class="form-control">
													<option value="0" <? if ($product->field4 == 0){ echo "selected"; }?>>No</option>
													<option value="1" <? if ($product->field4 == 1){ echo "selected"; }?>>Yes</option>
												</select>
											</div>
											<? } ?>
											<? if ($product->field5_description != NULL){?>
											<div class="form-group">
												<label class="control-label"><?=$product->field5_description?></label>
												<select name="field5" class="form-control">
													<option value="0" <? if ($product->field5 == 0){ echo "selected"; }?>>No</option>
													<option value="1" <? if ($product->field5 == 1){ echo "selected"; }?>>Yes</option>
												</select>
											</div>
											<? } ?>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->