	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($entity->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($entity->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($entity->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$entity->eid."#tab_1_3")?>">
								Products
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Add Product
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form role="form" method="post" action="<?=site_url("entities/products_insert") ?>" id="product">
								<input type="hidden" name="eid" value="<?=$entity->eid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<input type="hidden" name="status" value="1">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Category</label>
										<select name="category" class="form-control" >
                    <? if ($productcategories->num_rows() != 0) { ?>
										<option value="" disabled selected>--Select Product Category--</option>
										<? FOREACH($productcategories->result() as $row): ?>
											<option value="<?=$row->oepcid?>"><?=$row->description?></option>
										<? ENDFOREACH; ?>
                    <? } else { ?>
                      <option value="">-- no product categories available, please specify these under your organization options --</option>
                    <? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Provider</label>
										<select name="provider" class="form-control" >
										<? if ($productproviders->num_rows() != 0) { ?>
                      <option value="" disabled selected>--Select Product Provider--</option>
                      <? FOREACH($productproviders->result_array() as $row): ?>
                        <option value="<?=$row['oeppid']?>"><?=$row['description']?></option>
                      <? ENDFOREACH; ?>
                    <? } else { ?>
                      <option value="">-- no product providers available, please specify these under your organization options --</option>
                    <? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Type</label>
										<select name="type" class="form-control" >
                    <? if ($producttypes->num_rows() != 0) { ?>
										<option value="" disabled selected>--Select Product Type--</option>
                      <? FOREACH($producttypes->result_array() as $row): ?>
                        <option value="<?=$row['oeptid']?>"><?=$row['description']?></option>
                      <? ENDFOREACH; ?>
                    <? } else { ?>
                      <option value="">-- no product types available, please specify these under your organization options --</option>
                    <? } ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Unique Identifier</label>
										<input type="text" class="form-control" placeholder="Enter unique number" name="identifier" value="">
									</div>
									<div class="form-group">
										<label class="control-label">Date Issued</label>
										<input class="form-control input-long date-picker" placeholder="Enter date issue" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="issuedate"/>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->