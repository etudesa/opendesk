	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($contact->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Leads
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Prospects
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
							<? if ($contact->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$contact->eid)?>">
								Clients
							</a>
							<i class="fa fa-angle-right"></i>
							<? } ?>
						</li>
						<li>
							<a href="<?=site_url("entities/view/".$contact->eid."#tab_1_2")?>">
								Contacts
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Contact
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/contacts_update") ?>"  id="contact">
								<input type="hidden" name="eid" value="<?=$contact->eid?>">
								<input type="hidden" name="ecid" value="<?=$contact->ecid?>">
								<input type="hidden" name="pid" value="<?=$contact->pid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<div class="form-body">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Relationship</label>
												<select class="form-control" data-placeholder="Specify relationship" id="relationship" name="relationship">
													<? FOREACH($relationships->result() as $row): ?>
													<option value="<?=$row->oecrid?>" <? if ($row->oecrid == $contact->relationship){ echo "selected";} ?>><?=$row->description?></option>
													<? ENDFOREACH; ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">First Name</label>
												<input type="text" class="form-control" placeholder="Enter the firstname..." name="firstname" id="firstname" value="<?=$contact->firstname?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Last Name</label>
												<input type="text" class="form-control" placeholder="Enter the lastname..." name="lastname" id="lastname" value="<?=$contact->lastname?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Preferred Name</label>
												<input type="text" class="form-control" placeholder="Enter preferred name" name="preferredname" id="preferredname" value="<?=$contact->preferredname?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">ID Number</label>
												<input type="text" class="form-control" placeholder="Enter ID number" name="idnumber" id="idnumber" value="<?=$contact->idnumber?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Date of Birth</label>
												<input class="form-control input-long date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="<?=$contact->dateofbirth?>" name="dateofbirth" id="dateofbirth" placeholder="Click here to enable date picker"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Physical Address</label>
												<input type="text" class="form-control" placeholder="Enter physical address" name="physicaladdress" id="physicaladdress" value="<?=$contact->physicaladdress?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Postal Address</label>
												<input type="text" class="form-control" placeholder="Enter postal address" name="postaladdress" id="postaladdress" value="<?=$contact->postaladdress?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Home Phone</label>
												<input type="text" class="form-control" placeholder="Enter home phone" name="homephone" id="homephone" value="<?=$contact->homephone?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Office Phone</label>
												<input type="text" class="form-control" placeholder="Enter office phone" name="officephone" id="officephone" value="<?=$contact->officephone?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Mobile Number</label>
												<input type="text" class="form-control" placeholder="Enter mobile number" name="mobile" id="mobile" value="<?=$contact->mobile?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Email Address</label>
												<input type="text" class="form-control" placeholder="Enter email address" name="email" id="email" value="<?=$contact->email?>">
											</div>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
									<a class="btn red" data-toggle="modal" href="#deletecontactmodal">Delete Contact</a>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			<!--BEGIN MODAL FOR CONTACT DELETE -->
				<div class="modal fade" id="deletecontactmodal" tabindex="-1" role="deletecontactmodal" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Delete Contact</h4>
							</div>
							<div class="modal-body">
								 You are about to delete <strong><?=$contact->firstname?> <?=$contact->lastname?></strong>. Are you sure you want to do this?
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">No</button>
								<button type="button" class="btn blue" onclick="window.location.assign('<?=site_url("entities/contacts_delete/".$contact->eid."/".$contact->ecid) ?>')">Yes</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			<!-- END MODAL -->
			
		</div>
	</div>
	<!-- END CONTENT -->