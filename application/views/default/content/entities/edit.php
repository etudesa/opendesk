	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?=$title?> <small><?=$subtitle?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="<?=site_url("dashboard")?>">
								<i class="fa fa-home"></i>
							</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<? if ($entity->status == "1"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Leads
							</a>
							<? } ?>
							<? if ($entity->status == "2"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Prospects
							</a>
							<? } ?>
							<? if ($entity->status == "3"){ ?>
							<a href="<?=site_url("entities/view/".$entity->eid)?>">
								Clients
							</a>
							<? } ?>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<? if ($entity->status == "1"){ ?>
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Lead
							</div>
							<? } ?>
							<? if ($entity->status == "2"){ ?>
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Prospect
							</div>
							<? } ?>
							<? if ($entity->status == "3"){ ?>
							<div class="caption">
								<i class="fa fa-reorder"></i>Edit Client
							</div>
							<? } ?>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form method="post" action="<?=site_url("entities/update") ?>">
								<input type="hidden" name="eid" value="<?=$entity->eid?>">
								<input type="hidden" name="updated" value="<?=$today?>">
								<input type="hidden" name="organization" value="<?=$this->session->userdata('organization')?>">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label">Full Name</label>
										<input type="text" class="form-control" placeholder="Enter full name" name="name" value="<?=$entity->name?>">
									</div>
									<div class="form-group">
										<label class="control-label">Registration Number</label>
										<input type="text" class="form-control" placeholder="Enter ID/IT/CIPC number" name="registration" value="<?=$entity->registration?>">
									</div>
									<div class="form-group">
										<label class="control-label">Owner</label>
										<select name="createdby" class="form-control">
										<? FOREACH($users->result() as $row): ?>
											<option value="<?=$row->uid?>" <? if ($row->uid == $entity->createdby){ echo "selected"; }?>><?=$row->fullname?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Type</label>
										<select name="type" class="form-control">
										<? FOREACH($entitytypes->result() as $row): ?>
											<option value="<?=$row->oetid?>" <? if ($row->oetid == $entity->type){ echo "selected";}?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Status</label>
										<select name="status" class="form-control">
										<? FOREACH($entitystatus->result() as $row): ?>
											<option value="<?=$row->oesid?>" <? if ($row->oesid == $entity->status){ echo "selected";}?>><?=$row->description?></option>
										<? ENDFOREACH; ?>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label">Groups</label>
										<input type="hidden" id="groups" class="form-control select2" name="groups" value="<?=$entity->groups?>">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="btn green">Submit</button>
									<button type="button" class="btn default" onclick="window.history.back()">Cancel</button>
								</div>
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	
	<script>
		$( document ).ready(function() {
	        $("#groups").select2({
	            tags: [<? foreach ($groups->result() as $row){ echo "{id:\"".$row->egid."\", text:\"".$row->name."\"},"; } ?>]
	        });
		});
	</script>