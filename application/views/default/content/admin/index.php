	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
						Administration Dashboard <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a data-toggle="modal" href="#neworganizationmodal">New Organization</a>
								</li>
								<li>
									<a data-toggle="modal" href="#newusermodal">New User</a>
								</li>
							</ul>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="<?=site_url("dashboard")?>">
								Dashboard
							</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN DASHBOARD FOR ADMINISTRATION-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box light-grey">
						<div class="portlet-title">
							<div class="caption">
								Users
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="javascript:;" class="reload">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="entities">
							<thead>
							<tr>
								<th>
									 Fullname
								</th>
								<th>
									 Username
								</th>
								<th>
									 Access Level
								</th>
								<th>
									 Organization
								</th>
								<th>
									 Database
								</th>
							</tr>
							</thead>
							<tbody>
              <? FOREACH($query->result() as $row): ?>
							<tr class="odd gradeX">
								<td nowrap>
									 <?=$row->fullname?>
								</td>
								<td nowrap>
									 <?=$row->username?>
								</td>
								<td nowrap>
									 <?=$row->accesslevel?>
								</td>
								<td nowrap>
									 <?=$row->organization?>
								</td>
								<td nowrap>
									 <?=$row->database?>
								</td>
							</tr>
              <? ENDFOREACH; ?>
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END DASHBOARD FOR ADMINISTRATION-->
			
			<!--BEGIN MODAL FOR NEW ORGANIZATION -->
			<div id="neworganizationmodal" class="modal fade" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Create New Organization</h4>
							</div>
							<div class="modal-body">
								<form method="post" action="<?=site_url("admin/organization_insert") ?>" name="organizationform" id="organizationform">
									<input type="hidden" name="status" value="1">
									<div class="form-group">
										<label class="control-label">What is the name of the new organization?</label>
										<input type="text" class="form-control" placeholder="Enter organization name" name="name" value="">
									</div>
									<div class="form-group">
										<label class="control-label">What is the database name of the organization.</label>
										<input type="text" class="form-control" placeholder="Enter the database name" name="database" value="">
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn green" onclick="document.organizationform.submit()">Submit</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL FOR NEW ORGANIZATION-->
      
			<!--BEGIN MODAL FOR NEW USER -->
			<div id="newusermodal" class="modal fade" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Create New User</h4>
							</div>
							<div class="modal-body">
								<form method="post" action="<?=site_url("admin/user_insert") ?>" name="userform" id="userform">
									<input type="hidden" name="status" value="1">
									<div class="form-group">
										<label class="control-label">What is the full name of the new user?</label>
										<input type="text" class="form-control" placeholder="Enter full name" name="fullname" value="">
									</div>
									<div class="form-group">
										<label class="control-label">What will the username be?</label>
										<input type="text" class="form-control" placeholder="Enter username" name="username" value="">
									</div>
                  <div class="form-group">
                    <label class="control-label">What access level will this user have?</label>
                    <select name="accesslevel" class="form-control">
                      <option value="2">Level 2</option>
                      <option value="3">Level 3</option>
                      <option value="4">Level 4</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="control-label">For which organization is this?</label>
                    <select name="organization" class="form-control">
                      <? FOREACH($organizations->result() as $row): ?>
                        <option value="<?=$row->oid?>"><?=$row->name?></option>                      
                      <? ENDFOREACH; ?>
                    </select>
                  </div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn default" data-dismiss="modal">Cancel</button>
								<button type="button" class="btn green" onclick="document.userform.submit()">Submit</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL FOR NEW USER-->
			
		</div>
	</div>
	<!-- END CONTENT -->