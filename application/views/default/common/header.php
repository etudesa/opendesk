<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title>OpenDesk</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

	<? if (!isset($pageheader)){ ?>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

	<link href="<?=base_url('assets/default/plugins/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/uniform/css/uniform.default.css')?>" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link href="<?=base_url('assets/default/plugins/select2/select2.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/default/plugins/select2/select2-metronic.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/bootstrap-select/bootstrap-select.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/default/plugins/jquery-multi-select/css/multi-select.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/gritter/css/jquery.gritter.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/fullcalendar/fullcalendar/fullcalendar.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/data-tables/DT_bootstrap.css')?>"  rel="stylesheet" />
	<link href="<?=base_url('assets/default/plugins/bootstrap-datepicker/css/datepicker.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/bootstrap-datetimepicker/css/datetimepicker.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/nouislider/jquery.nouislider.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/plugins/bootstrap-switch/css/bootstrap-switch.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/default/plugins/jstree/dist/themes/default/style.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('assets/default/plugins/bootstrap-fileinput/bootstrap-fileinput.css')?>" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?=base_url('assets/default/css/style-metronic.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/style.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/style-responsive.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/plugins.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/pages/tasks.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/pages/login.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/themes/default.css')?>" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?=base_url('assets/default/css/print.css')?>" rel="stylesheet" type="text/css" media="print"/>
	<link href="<?=base_url('assets/default/css/custom.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/pages/profile.css')?>" rel="stylesheet" type="text/css"/>
	<link href="<?=base_url('assets/default/css/pages/inbox.css')?>" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->

	<link rel="shortcut icon" href="<?=base_url('favicon.ico')?>"/>

	<script src="<?=base_url('assets/default/plugins/jquery-1.10.2.min.js')?>" type="text/javascript"></script>

	<? } ?>

</head>
<!-- END HEAD -->