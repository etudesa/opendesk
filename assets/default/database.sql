--
-- Table structure for table `entities`
--

CREATE TABLE IF NOT EXISTS `entities` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `organization` int(11) DEFAULT '0',
  `mode` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `registration` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `primarycontact` int(11) DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `groups` varchar(255) NOT NULL,
  `access` int(10) DEFAULT '0',
  `updated` date NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `profile` (`organization`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `organization` (`organization`),
  KEY `createdby` (`createdby`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_activities`
--

CREATE TABLE IF NOT EXISTS `entities_activities` (
  `eaid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `createdby` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`eaid`),
  KEY `type` (`type`),
  KEY `createdby` (`createdby`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_contacts`
--

CREATE TABLE IF NOT EXISTS `entities_contacts` (
  `ecid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `relationship` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ecid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_correspondence`
--

CREATE TABLE IF NOT EXISTS `entities_correspondence` (
  `ecid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `createdby` int(11) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`ecid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_files`
--

CREATE TABLE IF NOT EXISTS `entities_files` (
  `efid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`efid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_financials`
--

CREATE TABLE IF NOT EXISTS `entities_financials` (
  `efid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `property1` float DEFAULT NULL,
  `property2` float DEFAULT NULL,
  `property3` float DEFAULT NULL,
  `business1` float DEFAULT NULL,
  `business2` float DEFAULT NULL,
  `business3` float DEFAULT NULL,
  `investment1` float DEFAULT NULL,
  `investment2` float DEFAULT NULL,
  `investment3` float DEFAULT NULL,
  `insurance1` float DEFAULT NULL,
  `insurance2` float DEFAULT NULL,
  `insurance3` float DEFAULT NULL,
  `other1` float DEFAULT NULL,
  `other2` float DEFAULT NULL,
  `other3` float DEFAULT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) DEFAULT NULL,
  `field5` int(11) DEFAULT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`efid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_medicals`
--

CREATE TABLE IF NOT EXISTS `entities_medicals` (
  `emid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `diet` int(11) DEFAULT NULL,
  `smoker` int(11) DEFAULT NULL,
  `allergy` int(11) DEFAULT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) DEFAULT NULL,
  `field5` int(11) DEFAULT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`emid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_products`
--

CREATE TABLE IF NOT EXISTS `entities_products` (
  `epid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `provider` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `issuedate` date DEFAULT NULL,
  `field1` int(11) NOT NULL,
  `field2` int(11) NOT NULL,
  `field3` int(11) NOT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`epid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_services`
--

CREATE TABLE IF NOT EXISTS `entities_services` (
  `esid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `issuedate` date DEFAULT NULL,
  `field1` int(11) NOT NULL,
  `field2` int(11) NOT NULL,
  `field3` int(11) NOT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`esid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_tickets`
--

CREATE TABLE IF NOT EXISTS `entities_tickets` (
  `etid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `epid` int(11) NOT NULL,
  `esid` int(11) NOT NULL,
  `ownedby` int(100) NOT NULL,
  `assignedto` int(100) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `reminderdate` date NOT NULL,
  `duedate` date NOT NULL,
  `field1` int(11) DEFAULT NULL,
  `field2` int(11) DEFAULT NULL,
  `field3` int(11) DEFAULT NULL,
  `field4` int(11) NOT NULL,
  `field5` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`etid`),
  KEY `eid` (`eid`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entities_tickets_notes`
--

CREATE TABLE IF NOT EXISTS `entities_tickets_notes` (
  `etnid` int(11) NOT NULL AUTO_INCREMENT,
  `etid` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`etnid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options_activitytypes`
--

CREATE TABLE IF NOT EXISTS `options_activitytypes` (
  `oeatid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oeatid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_activitytypes`
--

INSERT INTO `options_activitytypes` (`oeatid`, `description`) VALUES
(1, 'comment'),
(2, 'phone-square'),
(3, 'briefcase'),
(4, 'male'),
(5, 'ticket'),
(6, 'email'),
(7, 'building-o');

-- --------------------------------------------------------

--
-- Table structure for table `options_communicationmodes`
--

CREATE TABLE IF NOT EXISTS `options_communicationmodes` (
  `ocmid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ocmid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_communicationmodes`
--

INSERT INTO `options_communicationmodes` (`ocmid`, `description`) VALUES
(1, 'Qeued'),
(2, 'Sent');

-- --------------------------------------------------------

--
-- Table structure for table `options_communicationstatus`
--

CREATE TABLE IF NOT EXISTS `options_communicationstatus` (
  `ocsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ocsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_communicationstatus`
--

INSERT INTO `options_communicationstatus` (`ocsid`, `description`) VALUES
(1, 'Draft'),
(2, 'Finalized');

-- --------------------------------------------------------

--
-- Table structure for table `options_communicationtypes`
--

CREATE TABLE IF NOT EXISTS `options_communicationtypes` (
  `octid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`octid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_communicationtypes`
--

INSERT INTO `options_communicationtypes` (`octid`, `description`) VALUES
(1, 'Email'),
(2, 'SMS');

-- --------------------------------------------------------

--
-- Table structure for table `options_contactrelationships`
--

CREATE TABLE IF NOT EXISTS `options_contactrelationships` (
  `oecrid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oecrid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `options_contactrelationships`
--

INSERT INTO `options_contactrelationships` (`oecrid`, `description`) VALUES
(1, 'Director'),
(2, 'Member'),
(3, 'Trustee'),
(4, 'Beneficiary'),
(5, 'Spouse'),
(6, 'Dependant'),
(7, 'Proprietor'),
(8, 'Executor'),
(9, 'Founder'),
(10, 'Financial Officer'),
(11, 'Staff Member'),
(12, 'Donor');

-- --------------------------------------------------------

--
-- Table structure for table `options_correspondencetypes`
--

CREATE TABLE IF NOT EXISTS `options_correspondencetypes` (
  `oectid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `fa_icon` varchar(255) NOT NULL,
  PRIMARY KEY (`oectid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `options_correspondencetypes`
--

INSERT INTO `options_correspondencetypes` (`oectid`, `description`, `fa_icon`) VALUES
(1, 'Phone', 'fa-phone'),
(2, 'SMS', 'fa-mobile'),
(3, 'Email', 'fa-envelope'),
(4, 'Note', 'fa-comment');

-- --------------------------------------------------------

--
-- Table structure for table `options_entityfilters`
--

CREATE TABLE IF NOT EXISTS `options_entityfilters` (
  `oefid` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`oefid`),
  KEY `eid` (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options_entitymedicaldiets`
--

CREATE TABLE IF NOT EXISTS `options_entitymedicaldiets` (
  `oemdid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oemdid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitymedicaldiets`
--

INSERT INTO `options_entitymedicaldiets` (`oemdid`, `description`) VALUES
(1, 'Vegeterian'),
(2, 'Vegan'),
(3, 'Raw Food');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitymodes`
--

CREATE TABLE IF NOT EXISTS `options_entitymodes` (
  `oemid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oemid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitymodes`
--

INSERT INTO `options_entitymodes` (`oemid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitystatus`
--

CREATE TABLE IF NOT EXISTS `options_entitystatus` (
  `oesid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oesid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_entitystatus`
--

INSERT INTO `options_entitystatus` (`oesid`, `description`) VALUES
(1, 'Lead'),
(2, 'Prospect'),
(3, 'Client');

-- --------------------------------------------------------

--
-- Table structure for table `options_entitytypes`
--

CREATE TABLE IF NOT EXISTS `options_entitytypes` (
  `oetid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oetid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_entitytypes`
--

INSERT INTO `options_entitytypes` (`oetid`, `description`) VALUES
(1, 'Individual'),
(2, 'Partnership'),
(3, 'Trust'),
(4, 'Close Corporation'),
(5, 'Private Company'),
(6, 'Public Company'),
(7, 'Incorporation');

-- --------------------------------------------------------

--
-- Table structure for table `options_organizationstatus`
--

CREATE TABLE IF NOT EXISTS `options_organizationstatus` (
  `oosid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oosid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_organizationstatus`
--

INSERT INTO `options_organizationstatus` (`oosid`, `description`) VALUES
(1, 'Lead'),
(2, 'Prospect'),
(3, 'Client');

-- --------------------------------------------------------

--
-- Table structure for table `options_organizationtypes`
--

CREATE TABLE IF NOT EXISTS `options_organizationtypes` (
  `ootid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ootid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_organizationtypes`
--

INSERT INTO `options_organizationtypes` (`ootid`, `description`) VALUES
(1, 'Individual'),
(2, 'Partnership'),
(3, 'Trust'),
(4, 'Close Corporation'),
(5, 'Private Company'),
(6, 'Public Company'),
(7, 'Incorporation');

-- --------------------------------------------------------

--
-- Table structure for table `options_productcategories`
--

CREATE TABLE IF NOT EXISTS `options_productcategories` (
  `oepcid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oepcid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `options_productcategories`
--

INSERT INTO `options_productcategories` (`oepcid`, `description`) VALUES
(1, 'Short-Term Insurance'),
(2, 'Long-Term Insurance'),
(3, 'Health Service Benefits'),
(4, 'Collective Investment Scheme'),
(5, 'Foreign Exchange'),
(6, 'Securities and Shares'),
(7, 'Short-Term Deposits'),
(8, 'Debentures and Securitized Debt'),
(9, 'Retail Pension Benefits'),
(10, 'Funeral Policy'),
(11, 'Long-Term Deposits');

-- --------------------------------------------------------

--
-- Table structure for table `options_productproviders`
--

CREATE TABLE IF NOT EXISTS `options_productproviders` (
  `oeppid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oeppid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `options_productproviders`
--

INSERT INTO `options_productproviders` (`oeppid`, `description`) VALUES
(1, 'Mutual and Federal'),
(2, 'Santam'),
(3, 'Momentum Life'),
(4, 'Discovery Life'),
(5, 'Momentum Health'),
(6, 'Discovery Health'),
(7, 'Allan Gray'),
(8, 'Investec'),
(9, 'ONE Insurance'),
(10, 'New Wheels Insurance'),
(11, 'Zurich Insurance'),
(12, 'Old Mutual Life'),
(13, 'Old Mutual Wealth'),
(14, 'Efficient Wealth'),
(15, 'Momentum Wealth'),
(16, 'Discovery Invest'),
(17, 'Discovery Insure'),
(18, 'Brightrock Life'),
(19, 'Liberty Life'),
(20, 'Liberty Health'),
(21, 'Liberty Invest'),
(22, 'Resolution Health'),
(23, 'Medscheme'),
(24, 'Bonitas Medical Scheme'),
(25, 'Profmed Medical Scheme'),
(26, 'Sanlam Glacier'),
(27, 'Genric Insurance'),
(28, 'Sanlam Life'),
(29, 'Hollard'),
(30, 'Assupol');

-- --------------------------------------------------------

--
-- Table structure for table `options_productstatus`
--

CREATE TABLE IF NOT EXISTS `options_productstatus` (
  `oepsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oepsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_productstatus`
--

INSERT INTO `options_productstatus` (`oepsid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `options_producttypes`
--

CREATE TABLE IF NOT EXISTS `options_producttypes` (
  `oeptid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) DEFAULT NULL,
  `field2_description` varchar(255) DEFAULT NULL,
  `field3_description` varchar(255) DEFAULT NULL,
  `field4_description` varchar(255) DEFAULT NULL,
  `field5_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oeptid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `options_producttypes`
--

INSERT INTO `options_producttypes` (`oeptid`, `description`, `field1_description`, `field2_description`, `field3_description`, `field4_description`, `field5_description`) VALUES
(2, 'Unit Trust - Tax Free Saving', 'sdfs', 'sdfs', '', '', ''),
(3, 'Unit Trust - Retirement Annuity', '', '', '', '', ''),
(4, 'Unit Trust - Living Annuity', '', '', '', '', ''),
(5, 'Life Insurance', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options_servicestatus`
--

CREATE TABLE IF NOT EXISTS `options_servicestatus` (
  `oessid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oessid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_servicestatus`
--

INSERT INTO `options_servicestatus` (`oessid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `options_servicetypes`
--

CREATE TABLE IF NOT EXISTS `options_servicetypes` (
  `oestid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) DEFAULT NULL,
  `field2_description` varchar(255) DEFAULT NULL,
  `field3_description` varchar(255) DEFAULT NULL,
  `field4_description` varchar(255) DEFAULT NULL,
  `field5_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oestid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `options_servicetypes`
--

INSERT INTO `options_servicetypes` (`oestid`, `description`, `field1_description`, `field2_description`, `field3_description`, `field4_description`, `field5_description`) VALUES
(4, 'Estate Planning', '', '', '', '', NULL),
(5, 'Executorships', '', '', '', '', NULL),
(6, 'Trust Planning', NULL, NULL, NULL, NULL, NULL),
(7, 'Financial Planning', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options_ticketstatus`
--

CREATE TABLE IF NOT EXISTS `options_ticketstatus` (
  `oetsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oetsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_ticketstatus`
--

INSERT INTO `options_ticketstatus` (`oetsid`, `description`) VALUES
(1, 'Pending'),
(2, 'Completed'),
(3, 'Unresolved');

-- --------------------------------------------------------

--
-- Table structure for table `options_tickettypes`
--

CREATE TABLE IF NOT EXISTS `options_tickettypes` (
  `oettid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `field1_description` varchar(255) NOT NULL,
  `field2_description` varchar(255) NOT NULL,
  `field3_description` varchar(255) NOT NULL,
  `field4_description` varchar(255) NOT NULL,
  `field5_description` varchar(255) NOT NULL,
  PRIMARY KEY (`oettid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `options_useraccesslevels`
--

CREATE TABLE IF NOT EXISTS `options_useraccesslevels` (
  `oualid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oualid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `options_useraccesslevels`
--

INSERT INTO `options_useraccesslevels` (`oualid`, `description`) VALUES
(1, 'Priviledged User'),
(2, 'Normal User');

-- --------------------------------------------------------

--
-- Table structure for table `options_usermode`
--

CREATE TABLE IF NOT EXISTS `options_usermode` (
  `oumid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`oumid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_usermode`
--

INSERT INTO `options_usermode` (`oumid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_userstatus`
--

CREATE TABLE IF NOT EXISTS `options_userstatus` (
  `ousid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`ousid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_userstatus`
--

INSERT INTO `options_userstatus` (`ousid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Suspended');

-- --------------------------------------------------------

--
-- Table structure for table `options_usertaskstatus`
--

CREATE TABLE IF NOT EXISTS `options_usertaskstatus` (
  `outsid` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`outsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `options_usertaskstatus`
--

INSERT INTO `options_usertaskstatus` (`outsid`, `description`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `orgid` int(11) NOT NULL AUTO_INCREMENT,
  `mode` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `registration` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(100) NOT NULL,
  `groups` varchar(255) NOT NULL,
  `createdby` int(100) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`orgid`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_communications`
--

CREATE TABLE IF NOT EXISTS `organizations_communications` (
  `ecid` int(100) NOT NULL AUTO_INCREMENT,
  `organization` int(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `_wysihtml5_mode` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `mode` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`ecid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_groups`
--

CREATE TABLE IF NOT EXISTS `organizations_groups` (
  `egid` int(100) NOT NULL AUTO_INCREMENT,
  `organization` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`egid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_notifications`
--

CREATE TABLE IF NOT EXISTS `organizations_notifications` (
  `unid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`unid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_options`
--

CREATE TABLE IF NOT EXISTS `organizations_options` (
  `eoid` int(100) NOT NULL AUTO_INCREMENT,
  `organization` int(100) NOT NULL,
  `productcategories` varchar(255) DEFAULT NULL,
  `productproviders` varchar(255) DEFAULT NULL,
  `producttypes` varchar(255) DEFAULT NULL,
  `servicetypes` varchar(255) DEFAULT NULL,
  `tickettypes` varchar(255) DEFAULT NULL,
  `entitytypes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`eoid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_settings`
--

CREATE TABLE IF NOT EXISTS `organizations_settings` (
  `esid` int(11) NOT NULL AUTO_INCREMENT,
  `organization` int(11) NOT NULL,
  `imap_from` varchar(255) DEFAULT NULL,
  `imap_smtp` varchar(255) DEFAULT NULL,
  `imap_folder` varchar(255) NOT NULL,
  `imap_port` varchar(255) NOT NULL,
  `imap_flags` varchar(255) NOT NULL,
  `sms_apiid` varchar(255) DEFAULT NULL,
  `sms_username` varchar(255) DEFAULT NULL,
  `sms_password` varchar(255) DEFAULT NULL,
  `seafile_token` varchar(255) DEFAULT NULL,
  `seafile_library` varchar(255) DEFAULT NULL,
  `correspondence` tinyint(1) NOT NULL DEFAULT '0',
  `support` tinyint(1) NOT NULL DEFAULT '0',
  `governance` tinyint(1) NOT NULL DEFAULT '0',
  `fiduciary` tinyint(1) NOT NULL DEFAULT '0',
  `files` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`esid`),
  UNIQUE KEY `esid` (`esid`),
  KEY `eid` (`organization`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_users`
--

CREATE TABLE IF NOT EXISTS `organizations_users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `organization` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `accesslevel` int(100) NOT NULL,
  `status` int(11) NOT NULL,
  `imap_username` varchar(255) NOT NULL,
  `imap_password` varchar(255) NOT NULL,
  `seafile_username` varchar(255) NOT NULL,
  `seafile_password` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organizations_users_tasks`
--

CREATE TABLE IF NOT EXISTS `organizations_users_tasks` (
  `utid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `reminderdate` date NOT NULL,
  `duedate` date NOT NULL,
  `progress` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`utid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE IF NOT EXISTS `persons` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `organization` int(11) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `preferredname` varchar(255) NOT NULL,
  `idnumber` varchar(255) NOT NULL,
  `dateofbirth` date NOT NULL,
  `physicaladdress` varchar(255) NOT NULL,
  `postaladdress` varchar(255) NOT NULL,
  `homephone` varchar(255) NOT NULL,
  `officephone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `field1` varchar(255) NOT NULL,
  `field2` varchar(11255) NOT NULL,
  `field3` varchar(255) NOT NULL,
  `field4` varchar(255) NOT NULL,
  `field5` varchar(255) NOT NULL,
  `updated` date NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
